<?php

/*
 *	File for registration and adding custom actions/filters to the theme
-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Adding Custom actions
- Tokyo After Post actions
- Additional functions for displaying links
-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Adding Custom actions */
/*-----------------------------------------------------------------------------------*/
/*
 * Sets up the new tokyofashion action which will run before the_content function
 * Will works in single.php file
 * 
 */
if (!function_exists('th_before_content')) {

    function th_before_content()
    {
        do_action('th_before_content');
    }

}


/*
 * Sets up the new tokyofashion action which run after the_content function and compare all tag-style information
 * Will works in single.php file
 * 
 **/
if (!function_exists('tokyo_after_post')) {
    function tokyo_after_post()
    {
        do_action('tokyo_after_post');
    }
}

/*-----------------------------------------------------------------------------------*/
/* Tokyo After Post actions */
/*-----------------------------------------------------------------------------------*/
/**
 * This function get all information from post_to_persons connection and displaying it in affordable to user view
 */
add_action('tokyo_after_post', 'getPersonInfromation', 2);
function getPersonInfromation()
{

    global $wp_query;

    $post_id = get_the_ID();


    $default_keys = array(
        'name',
        'photos',
        'gender',
        'job',
        'age',
        'facebook',
        'twitter',
        'instagram',
        'tumblr',
        'blog',
        'brands',
        'favorite_brands',
        'favorite_music'
    );

    //get persons p2p_meta for disconnect & connect
    if( $wp_query->is_preview()  )  {

        $p2p_meta = tf_custom_get_p2p_meta_preview_mode($post_id, 'post_to_persons', $default_keys);

    } else {

        $p2p_meta = tf_custom_get_p2p_meta($post_id, 'post_to_persons', $default_keys);

    }
    $html = '';

    $p2p_meta['layouts'] = array_reverse($p2p_meta['layouts']);

    foreach ($p2p_meta['layouts'] as $layout)

        $html .= person_snap_data_print($layout);

    echo $html;

}


/**
 * This function get location information from post`s meta and displaying link
 */
add_action('tokyo_after_post', 'getAdditionalSnapInformation', 3);
function getAdditionalSnapInformation()
{

    $location = get_field('photo-location');

    if ($location) {
        $url = add_query_arg('location', $location, get_site_url() . '/photos/');
        $location_link = apply_filters('filter_create_link', $html, $url, $location);
        echo apply_filters('filter_person_print_data', $html, 'div', 'Location', $location_link);
    }
}

/**
 *  This function display post`s snap date information
 */
// add_action('tokyo_after_post', 'date_information', 5);
// function date_information()
// {
//     $originalDate = get_field('snap_date');
//     $date = new DateTime($originalDate);

//     echo apply_filters('filter_person_print_data', $html, 'p', 'Posted on', $date->format('m/d/Y'));
    
//     // echo apply_filters('filter_person_print_data', $html, 'p', 'Posted on', $date->format('d.m.Y'));

// }


/**
 *  This function display post`s contributor information
 */
add_action('tokyo_after_post', 'contributor_info', 6);
function contributor_info()
{

    // var_dump( get_field('contributors') );

    $contributors = get_field('contributors');

    if (!get_field('contributors'))
        return;

    $html = '';

    foreach ($contributors as $contributor) {

        $contributor_object = $contributor['contributor'];
        $short_name = get_field('contributor_shortname', $contributor_object->ID);
        $html .= apply_filters('filter_person_print_data', $html, 'p', ucfirst($contributor['contributor_type']), createContributorLink($short_name, $contributor['contributor_type']));
    }
    
    $html .= '</br>';

    echo $html;
}

// add_action('tokyo_after_post', 'get_social_counter', 7);
function get_social_counter()
{
    $total_social_count = 0;
    $post_id = get_the_ID();
    if(class_exists('Social_Count')){
        $instance = Social_Count::get_instance();
        $url = get_page_link($post_id);
        $socials = array(
            'facebook' => $instance->getFacebook($url),
            'twitter' => $instance->getTwitter($url),
            'pinterest' => $instance->getPint($url),
            'googlePlus' => $instance->getGplus($url)
        );
        foreach ($socials as $social) {
            $total_social_count += $social;
        }
        // echo "Total socials likes " . $total_social_count;
        // echo 'div class="total-likes"' . $total_social_count . '</div>';
        echo apply_filters ('filter_person_print_data', $html, 'div class="total-likes"', '&nbsp;&nbsp;&nbsp;&nbsp;', $total_social_count);    
    }else{
        return false;
    }
    

    // print_r($post_id);
}

/**
 *  This function display before post content person information
 */
add_action('th_before_content', 'personInformation', 1);
function personInformation()
{

    if (get_field('before_content', get_the_ID())) {
        the_field('before_content', get_the_ID());
    }

}


/**
 *  This function display all information in link style for filtration.
 */
function person_snap_data_print($data)
{

    if (empty($data))
        return;
    //print_r($data['gender']);
    $html = '<ul class="nag">';
    $html .= '<li>'. createSimpleLink('name', ucfirst($data['name'])).'</li>';

    if(!empty($data['age'])){
        $html .= '<li>- ' . createSimpleLinkWithCustomTitle( 'age', $data['age'], '-years-old' )  . '</li>';
    }

    if(!empty($data['gender']) && $data['gender']!= 'unstated'){
    $html .= '<li>- '.createSimpleLink('gender', ucfirst($data['gender'])).'</li>';
    }
    $html .= '</ul>';
    $html .= apply_filters('filter_person_print_data', $html, 'p', 'Occupation', createSimpleLink('job', ucfirst($data['job'])));

    if ($data['facebook'] != 'Invalid URL')
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Facebook', $data['facebook']);

    if ($data['twitter'])
        $html .= apply_filters('filter_social_link', $html, $data['twitter'], 'twitter');

    if ($data['instagram'])
        $html .= apply_filters('filter_social_link', $html, $data['instagram'], 'instagram');

    if ($data['tumblr'])
        $html .= apply_filters('filter_social_link', $html, $data['tumblr'], 'tumblr');

    if ($data['blog'] != 'Invalid URL')
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Blog', $data['blog']);


    if ($data['brands']['brands_outer'] && !empty($data['brands']['brands_outer'])) {
        $links = array();
        foreach ($data['brands']['brands_outer'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Outer', implode(", ", $links));
    }

    if ($data['brands']['brands_tops'] && !empty($data['brands']['brands_tops'])) {
        $links = array();
        foreach ($data['brands']['brands_tops'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Tops', implode(", ", $links));
    }

    if ($data['brands']['brands_bottoms'] && !empty($data['brands']['brands_bottoms'])) {
        $links = array();
        foreach ($data['brands']['brands_bottoms'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Bottoms', implode(", ", $links));
    }

    if ($data['brands']['brands_bag'] && !empty($data['brands']['brands_bag'])) {
        $links = array();
        foreach ($data['brands']['brands_bag'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Bag', implode(", ", $links));
    }

    if ($data['brands']['brands_shoes'] && !empty($data['brands']['brands_shoes'])) {
        $links = array();
        foreach ($data['brands']['brands_shoes'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Shoes', implode(", ", $links));
    }

    if ($data['brands']['brands_accessories'] && !empty($data['brands']['brands_accessories'])) {
        $links = array();
        foreach ($data['brands']['brands_accessories'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Accessories', implode(", ", $links));
    }

    if ($data['favorite_brands'] && !empty($data['favorite_brands'])) {
        $links = array();
        foreach ($data['favorite_brands'] as $key => $value) {
            $links[] = create_link_for_favorite_brands('favorite_brands', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Favorite shops/brands', implode(", ", $links));
    }

    if ($data['favorite_music'] && !empty($data['favorite_music'])) {
        $links = array();
        foreach ($data['favorite_music'] as $key => $value) {
            $links[] = create_link_for_music_and_brands('music', (int)$value);
        }
        $html .= apply_filters('filter_person_print_data', $html, 'p', 'Favorite music', implode(", ", $links));
    }

    $html .= '</br>';

    return $html;
}


/*-----------------------------------------------------------------------------------*/
/* Additional functions for displaying links */
/*-----------------------------------------------------------------------------------*/
//function createRepeaterLink($clothes_object, $brand_object)
//{
//
//    $item = $clothes_object->post_name;
//    $brand = $brand_object->slug;
//    $brand_name = $brand_object->name;
//
//    $array = array(
//        get_site_url(),
//        'find',
//        'item',
//        strtolower($item),
//        'brand',
//        $brand
//    );
//
//    return apply_filters('filter_create_link', $html, implode('/', $array), $brand_name);
//}


function create_link_for_favorite_brands($field, $termId)
{

    $term = get_term_by('id', $termId, 'brands');

    if (!$term)
        return;

    $array = array(
        get_site_url(),
        'find',
        $field,
        $term->slug
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), $term->name);
}


function create_link_for_music_and_brands($field, $termId)
{

    if ($field == 'music')
        $taxonomy = 'music';
    else
        $taxonomy = 'brands';

    $term = get_term_by('id', $termId, $taxonomy);

    if (!$term)
        return;

    $array = array(
        get_site_url(),
        $field,
        $term->slug
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), $term->name);
}


function createSimpleLink($field, $value)
{

    $array = array(
        get_site_url(),
        'find',
        //'item'             ,
        $field,
        strtolower($value),
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), $value);
}

function createSimpleLinkWithCustomTitle( $field, $value, $title ){

    $array = array(
        get_site_url(),
        'find',
        //'item'             ,
        $field,
        strtolower($value),
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), $value . $title );

}

function createSocialLink($field, $value)
{

    return apply_filters('filter_create_link', $html, addhttp($field), $value);
}


function createContributorLink($name, $short_name)
{

    $array = array(
        get_site_url(),
        'find',
        strtolower($short_name),
        strtolower($name),
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), ucfirst($name));
}


add_filter('filter_create_link', 'create_diferent_link', 10, 4);
function create_diferent_link($html, $value, $name, $class = '')
{
    if (empty($name)) {
        return;
    } else {
        $html = '<a class="' . $class = '' . '" href="' . $value . '">' . $name . '</a>';
        return $html;
    }
}


// add_filter('filter_social_link', 'create_social_link', 10, 4);
// function create_social_link($html, $value, $field_name, $class = '')
// {
//     $html = '';

//     if ($field_name == 'twitter')
//         $html .= '<p>Twitter: <a class="' . $class . '" href="https://twitter.com/' . $value . '">' . $value . '</a></p>';
//     elseif ($field_name == 'instagram')
//         $html .= '<p>Instagram: <a class="' . $class . '" href="http://instagram.com/' . $value . '">' . $value . '</a></p>'; elseif ($field_name == 'tumblr')
//         $html .= '<p>Tumblr: <a class="' . $class . '" href="http://' . $value . '.tumblr.com">' . $value . '</a></p>';


//     return $html;

// }

add_filter('filter_social_link', 'create_social_link', 10, 4);
function create_social_link($html, $value, $field_name, $class = '')
{
    $html = '';

    if ($field_name == 'twitter')
        $html .= '<p>Twitter: <a class="' . $class . '" target="_blank" href="https://twitter.com/' . $value . '">' . $value . '</a></p>';
    elseif ($field_name == 'instagram')
        $html .= '<p>Instagram: <a class="' . $class . '" target="_blank" href="http://instagram.com/' . $value . '">' . $value . '</a></p>'; elseif ($field_name == 'tumblr')
        $html .= '<p>Tumblr: <a class="' . $class . '" target="_blank" href="http://' . $value . '.tumblr.com">' . $value . '</a></p>';


    return $html;

}

add_filter('filter_person_print_data', 'filter_person_print', 10, 5);
function filter_person_print($html, $tag, $field, $value, $colon = ': ')
{
    if (!empty($value))
        return '<' . $tag . '>' . $field . $colon . $value . '</' . $tag . '>';

    return;
}

add_filter('filter_get_term_name', 'get_term_name', 10, 2);
function get_term_name($name, $value)
{

    global $wpdb;

    if ($name == 'brands' || $name == 'favorite-brands' || $name == 'favorite_brands')

        $taxonomy = 'brands';

    elseif ($name == 'music')

        $taxonomy = 'music'; else

        return urldecode($value);

    $field = 't.slug';
    $value = htmlentities($value);

    $term = $wpdb->get_row($wpdb->prepare(
        "SELECT t.*, tt.*
                    FROM $wpdb->terms
                    AS t INNER JOIN $wpdb->term_taxonomy
                    AS tt ON t.term_id = tt.term_id
                    WHERE tt.taxonomy = %s AND $field = %s LIMIT 1",
        $taxonomy,
        $value
    ));

    if (isset($term->name))
        return $term->name;

    return false;


}


add_action('admin_head', 'replace_acf_select');
function replace_acf_select()
{
    ?>
    <style type="text/css">
        .select2-container {
            width: 45%
        }
    </style>
<?php
}

add_action('pre_get_posts', 'add_brands_criteria');
function add_brands_criteria(&$query)
{
    if ($query->is_tax('brands') && $query->is_main_query()) {

        //echo 'brands';
        $query->set('cat', '-1170');
        $query->set('post_type', 'post');
        return;
    }

}

/*
 * This is function initialize custom query (p2p_persons_query)
 */
add_filter('pre_get_posts', 'custom_wpquery');
function custom_wpquery(&$query)
{
    if ((isset($query->query_vars['find']) || $query->is_tax('music') || $query->is_tax('brands')) && $query->is_main_query()) {

        $query = new p2p_persons_query($query->query_vars);

        $query->init();

        return $query;
    }
}


/*
 * This function create HTML for contributor's Profile (http://tokyofashion.com/contributors)
 */
function contributor_profile($post_id)
{
    $profile = get_field('profile', $post_id);
    if ($profile)
        echo $profile;
}

/*
 * This function create HTML for contributor's Blood Type (http://tokyofashion.com/contributors)
 */
function contributor_bloodtype($post_id)
{
    $blood_type = get_field('blood_type', $post_id);
    if ($blood_type)
        echo 'Blood Type: ' . $blood_type . '<br/>';
}

/*
 * This function create HTML for contributor's Image (http://tokyofashion.com/contributors)
 */
function contributor_imagesrc($post_id)
{
    if (has_post_thumbnail($post_id)) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'thumbnail');
        echo $image[0];
    }
}

function createTaxonomyLink($field, $termId)
{

    // $taxonomy = ( $field == 'music' ) ? $field : 'brands';

    if ($field == 'music') {
        $taxonomy = 'music';
    } elseif ($field == 'favorite_brands') {
        $taxonomy = 'brands';
        $field = 'favorite-brands';
    } else {
        $taxonomy = 'brands';
    }

    $term = get_term_by('id', $termId, $taxonomy);

    if (!$term)
        return;
    $slug = $term->slug;

    $array = array(
        get_site_url(),
        'find',
        $field,
        $slug,
    );

    return apply_filters('filter_create_link', $html, implode('/', $array), $term->name);
}


/*
 * This function create HTML for contributor's Links (http://tokyofashion.com/contributors)
 */
function contributor_link($post_id)
{

    $links = array();
    while (has_sub_field('contact_and_links', $post_id)):
        $links[] = array(
            'title' => get_sub_field('cal_title', $post_id),
            'value' => get_sub_field('cal_value', $post_id),
            'main' => get_sub_field('cal_main', $post_id)
        );
    endwhile;

    if ($links) {
        foreach ($links as $link) {
            echo $link['title'] . ': ' . apply_filters('filter_create_link', $html, $link['value'], $link['main']) . '<br>';
        }
    }

}

add_action('wp_head', 'google_webmaster_tool');
function google_webmaster_tool()
{
    ?>
    <meta name="google-site-verification" content="veOmrOeB1FsvzizsWdEL_yLjPXCMVbNWeNmVopoahSc"/>
<?php
}

// add_filter('pre_get_posts', 'custom_preview_query');
// function custom_preview_query(&$query)
// {

//     if ( $query->is_preview() ) {

//         $query = new p2p_persons_query($query->query_vars);

//         $query->init();
        
//         return $query;        
//         // var_dump( $query );

//     }

//     //     printer($query);
        
//     //     $query = new p2p_persons_query($query->query_vars);

//     //     $query->init();
        
//     //     printer($query);

//     //     return $query;
//     // }


// }


/*function my_search()
{
    global $wp_query;
    if (is_search()) {
        printer($wp_query->request);
    }
}

add_action('pre_get_posts', 'my_search');*/
    
  
    
    add_action('wp_head','smart_app');

function smart_app()
{
wp_register_script('smart',  get_template_directory() . '/js/jquery.smartbanner.js', false, NULL);
wp_enqueue_script('smart');
$output=" <meta name='apple-itunes-app' content='app-id=398491911'><meta name='google-play-app' content='app-id=com.tokyofashion'>";
$output .='<script type="text/javascript">$().smartbanner();</script>';

echo $output;

}

    
function japan_language()
{
    $my_language = 'ja';

    $languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);


    if (in_array($my_language, $languages)) {
        wp_register_script('japan_language', get_stylesheet_directory_uri() . '/js/japan-lang.js', false, NULL);
        wp_enqueue_script('japan_language');
    }

}

add_action('wp_footer', 'japan_language');

add_action("wp_enqueue_scripts", "js_social_dropdown", 11);
function js_social_dropdown()
{
    wp_register_script('social_dropdown', get_template_directory_uri() . "/js/social_dropdown.js", false, NULL);
    wp_enqueue_script('social_dropdown');
}

add_action("wp_enqueue_scripts", "js_dropdown", 11);
function js_dropdown()
{
    wp_register_script('dropdown', get_template_directory_uri() . "/js/dropdown.js", false, NULL);
    wp_enqueue_script('dropdown');
}

//add_action("wp_enqueue_scripts", "scrollspy", 12);
//function scrollspy()
//{
//    wp_register_script('scrollspy', get_template_directory_uri() . "/js/scrollspy.js", false, NULL);
//    wp_enqueue_script('scrollspy');
//}

 /**
 * This function display post`s tags 
 */
add_action('tokyo_after_post', 'print_tags', 4);
function print_tags(){
    the_tags('Tags: ', ', ', '<br />'); 
}

/**
*   Featured Image low quaility bug
*/

function jpeg_quality_callback( $arg ) 
{
    return (int)100;
}
 
add_filter('jpeg_quality', 'jpeg_quality_callback');