<?php
/*
 * ACF Includes 
 */

add_action('acf/register_fields', 'tf_custom_register_acf_fields');
function tf_custom_register_acf_fields() {
    include_once('acf-taxonomy_select/taxonomy_select-v4.php');
}




/**
 *  Register Field Groups [ Contributor Information ]
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contributor-information-2',
		'title' => 'Contributor Information',
		'fields' => array (
			array (
				'key' => 'field_5189000b64685',
				'label' => 'TAB 1',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					'photographer' => 'Photographer',
					'writer' => 'Writer',
					'editor' => 'Editor',
					'support-staff' => 'Support Staff',
				),
				'default_value' => '',
				'key' => 'field_5188fce2eb480',
				'label' => 'Contributor Type',
				'name' => 'contributor_type',
				'type' => 'select',
			),
			array (
				'default_value' => '',
				'formatting' => 'none',
				'key' => 'field_5188fce2e8486',
				'label' => 'Contributor Shortname',
				'name' => 'contributor_shortname',
				'type' => 'text',
				'instructions' => 'Make sure this is unique!',
			),
			array (
				'default_value' => '',
				'formatting' => 'none',
				'key' => 'field_5188fce2e67e2',
				'label' => 'Blood Type',
				'name' => 'blood_type',
				'type' => 'text',
				'instructions' => 'eg. A+, O+, etc',
			),
			array (
				'key' => 'field_518bb84524dc9',
				'label' => 'Contact And Links',
				'name' => 'contact_and_links',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'default_value' => '',
						'formatting' => 'none',
						'key' => 'field_518bb8c124dca',
						'label' => 'Contact And Link Title:',
						'name' => 'cal_title',
						'type' => 'text',
						'column_width' => '',
					),
					array (
						'default_value' => '',
						'formatting' => 'none',
						'key' => 'field_518bb8f224dcb',
						'label' => 'Contact And Link Value:',
						'name' => 'cal_value',
						'type' => 'text',
						'column_width' => '',
					),
					array (
						'default_value' => '',
						'formatting' => 'html',
						'key' => 'field_518bb92624dcc',
						'label' => 'Contact and Link:',
						'name' => 'cal_main',
						'type' => 'text',
						'column_width' => '',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Link or Contact',
			),
			array (
				'key' => 'field_5189002c64686',
				'label' => 'TAB 2',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'toolbar' => 'full',
				'media_upload' => 'yes',
				'default_value' => '',
				'key' => 'field_5188fce2e9201',
				'label' => 'Profile',
				'name' => 'profile',
				'type' => 'wysiwyg',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'contributors',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'revisions',
				5 => 'author',
				6 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}



/**
 *  Register Field Groups [ Contributor Information ]
 */

// if(function_exists("register_field_group"))
// {
// 	register_field_group(array (
// 		'id' => 'acf_contributor-information-2',
// 		'title' => 'Contributor Information',
// 		'fields' => array (
// 			array (
// 				'key' => 'field_5189000b64685',
// 				'label' => 'TAB 1',
// 				'name' => '',
// 				'type' => 'tab',
// 			),
// 			array (
// 				'multiple' => 0,
// 				'allow_null' => 0,
// 				'choices' => array (
// 					'photographer' => 'Photographer',
// 					'writer' => 'Writer',
// 					'editor' => 'Editor',
// 					'support-staff' => 'Support Staff',
// 				),
// 				'default_value' => '',
// 				'key' => 'field_5188fce2eb480',
// 				'label' => 'Contributor Type',
// 				'name' => 'contributor_type',
// 				'type' => 'select',
// 			),
// 			array (
// 				'default_value' => '',
// 				'formatting' => 'none',
// 				'key' => 'field_5188fce2e8486',
// 				'label' => 'Contributor Shortname',
// 				'name' => 'contributor_shortname',
// 				'type' => 'text',
// 				'instructions' => 'Make sure this is unique!',
// 			),
// 			array (
// 				'default_value' => '',
// 				'formatting' => 'none',
// 				'key' => 'field_5188fce2e67e2',
// 				'label' => 'Blood Type',
// 				'name' => 'blood_type',
// 				'type' => 'text',
// 				'instructions' => 'eg. A+, O+, etc',
// 			),
// 			array (
// 				'key' => 'field_518bb84524dc9',
// 				'label' => 'Contact And Links',
// 				'name' => 'contact_and_links',
// 				'type' => 'repeater',
// 				'sub_fields' => array (
// 					array (
// 						'default_value' => '',
// 						'formatting' => 'none',
// 						'key' => 'field_518bb8c124dca',
// 						'label' => 'Contact And Link Title:',
// 						'name' => 'cal_title',
// 						'type' => 'text',
// 						'column_width' => '',
// 					),
// 					array (
// 						'default_value' => '',
// 						'formatting' => 'none',
// 						'key' => 'field_518bb8f224dcb',
// 						'label' => 'Contact And Link Value:',
// 						'name' => 'cal_value',
// 						'type' => 'text',
// 						'column_width' => '',
// 					),
// 					array (
// 						'default_value' => '',
// 						'formatting' => 'html',
// 						'key' => 'field_518bb92624dcc',
// 						'label' => 'Contact and Link:',
// 						'name' => 'cal_main',
// 						'type' => 'text',
// 						'column_width' => '',
// 					),
// 				),
// 				'row_min' => 0,
// 				'row_limit' => '',
// 				'layout' => 'table',
// 				'button_label' => 'Add Link or Contact',
// 			),
// 			array (
// 				'key' => 'field_5189002c64686',
// 				'label' => 'TAB 2',
// 				'name' => '',
// 				'type' => 'tab',
// 			),
// 			array (
// 				'toolbar' => 'full',
// 				'media_upload' => 'yes',
// 				'default_value' => '',
// 				'key' => 'field_5188fce2e9201',
// 				'label' => 'Profile',
// 				'name' => 'profile',
// 				'type' => 'wysiwyg',
// 			),
// 		),
// 		'location' => array (
// 			array (
// 				array (
// 					'param' => 'post_type',
// 					'operator' => '==',
// 					'value' => 'contributors',
// 					'order_no' => 0,
// 					'group_no' => 0,
// 				),
// 			),
// 		),
// 		'options' => array (
// 			'position' => 'normal',
// 			'layout' => 'default',
// 			'hide_on_screen' => array (
// 				0 => 'the_content',
// 				1 => 'excerpt',
// 				2 => 'custom_fields',
// 				3 => 'discussion',
// 				4 => 'revisions',
// 				5 => 'author',
// 				6 => 'send-trackbacks',
// 			),
// 		),
// 		'menu_order' => 0,
// 	));
// }

 /**
  *  Register Field Groups [ Snap Group ]
  *
  *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
  *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
  */




 /******************* ниже новое ***********************/

// if(function_exists("register_field_group"))
// {
//     register_field_group(array (
//         'id' => 'acf_snap-information',
//         'title' => 'Snap Information',
//         'fields' => array (
//             array (
//                 'key' => 'field_51bdd6ac55d8b',
//                 'label' => 'Using new functionality?',
//                 'name' => 'check',
//                 'type' => 'checkbox',
//                 'choices' => array (
//                     'yes' => 'Yes',
//                 ),
//                 'default_value' => '',
//                 'layout' => 'vertical',
//             ),
//             array (
//                 'key' => 'field_51bdd6ec55d8c',
//                 'label' => 'Persons',
//                 'name' => 'persons',
//                 'type' => 'repeater',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'sub_fields' => array (
//                     array (
//                         'key' => 'field_51bdd70155d8d',
//                         'label' => 'Name',
//                         'name' => 'name',
//                         'type' => 'text',
//                         'instructions' => 'This field will be used as person\'s post title',
//                         'column_width' => '',
//                         'default_value' => 'Persons Post Title',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd73555d8e',
//                         'label' => 'Photo #s',
//                         'name' => 'photos',
//                         'type' => 'textarea',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd74b55d8f',
//                         'label' => 'Gender',
//                         'name' => 'gender',
//                         'type' => 'select',
//                         'column_width' => '',
//                         'choices' => array (
//                             'male' => 'Male',
//                             'female' => 'Female',
//                             'unstated' => 'Unstated',
//                         ),
//                         'default_value' => '',
//                         'allow_null' => 1,
//                         'multiple' => 0,
//                     ),
//                     array (
//                         'key' => 'field_51bdd78755d90',
//                         'label' => 'Job',
//                         'name' => 'job',
//                         'type' => 'text',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd79655d91',
//                         'label' => 'Age',
//                         'name' => 'age',
//                         'type' => 'text',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7ab55d94',
//                         'label' => 'Facebook',
//                         'name' => 'facebook',
//                         'type' => 'website',
//                         'column_width' => '',
//                         'website_title' => 'true',
//                         'internal_link' => 'false',
//                         'value' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dd55d98',
//                         'label' => 'Twitter',
//                         'name' => 'twitter',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dc55d97',
//                         'label' => 'Instagram',
//                         'name' => 'instagram',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dc55d96',
//                         'label' => 'Tumblr',
//                         'name' => 'tumblr',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7d655d95',
//                         'label' => 'Blog',
//                         'name' => 'blog',
//                         'type' => 'website',
//                         'column_width' => '',
//                         'website_title' => 'true',
//                         'internal_link' => 'false',
//                         'value' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd81755d99',
//                         'label' => 'Brands - Outer',
//                         'name' => 'brands_outer',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84655da0',
//                         'label' => 'Brands - Tops',
//                         'name' => 'brands_tops',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84555d9f',
//                         'label' => 'Brands - Bottoms',
//                         'name' => 'brands_bottoms',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84455d9e',
//                         'label' => 'Brands - Bag',
//                         'name' => 'brands_bag',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84455d9d',
//                         'label' => 'Brands - Shoes',
//                         'name' => 'brands_shoes',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84355d9c',
//                         'label' => 'Brands - Accessories',
//                         'name' => 'brands_accessories',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84255d9b',
//                         'label' => 'Favorite Brands/Shops',
//                         'name' => 'favorite_brands',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84155d9a',
//                         'label' => 'Favorite Music',
//                         'name' => 'favorite_music',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'music',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                 ),
//                 'row_min' => 0,
//                 'row_limit' => '',
//                 'layout' => 'row',
//                 'button_label' => 'Add New Person',
//             ),
//             array (
//                 'key' => 'field_51bdd8e80df90',
//                 'label' => 'Admin Notes',
//                 'name' => 'admin_notes',
//                 'type' => 'wysiwyg',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'default_value' => '',
//                 'toolbar' => 'full',
//                 'media_upload' => 'yes',
//             ),
//             array (
//                 'key' => 'field_51bdd90f0df91',
//                 'label' => 'Location',
//                 'name' => 'photo-location',
//                 'type' => 'radio',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'choices' => array (
//                     'Harajuku' => 'Harajuku',
//                     'Shibuya' => 'Shibuya',
//                     'Shinjuju' => 'Shinjuju',
//                     'Daikanyama' => 'Daikanyama',
//                     'Shimokitazawa' => 'Shimokitazawa',
//                 ),
//                 'other_choice' => 0,
//                 'save_other_choice' => 0,
//                 'default_value' => 'Harajuku',
//                 'layout' => 'vertical',
//             ),
//             array (
//                 'key' => 'field_51bdd96c0df92',
//                 'label' => 'Contributors',
//                 'name' => 'contributors',
//                 'type' => 'repeater',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'sub_fields' => array (
//                     array (
//                         'key' => 'field_51bdd9800df93',
//                         'label' => 'Contributor Type',
//                         'name' => 'contributor_type',
//                         'type' => 'select',
//                         'column_width' => '',
//                         'choices' => array (
//                             'author' => 'Author',
//                             'photographer' => 'Photographer',
//                             'hunter' => 'Hunter',
//                         ),
//                         'default_value' => '',
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                     array (
//                         'key' => 'field_51bddb110df94',
//                         'label' => 'Contributor',
//                         'name' => 'contributor',
//                         'type' => 'post_object',
//                         'column_width' => '',
//                         'post_type' => array (
//                             0 => 'contributors',
//                         ),
//                         'taxonomy' => array (
//                             0 => 'all',
//                         ),
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                 ),
//                 'row_min' => 0,
//                 'row_limit' => '',
//                 'layout' => 'table',
//                 'button_label' => 'Add Contributor',
//             ),
//             array (
//                 'key' => 'field_51bddb670df95',
//                 'label' => 'Snap Date',
//                 'name' => 'snap_date',
//                 'type' => 'date_picker',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'date_format' => 'yymmdd',
//                 'display_format' => 'dd/mm/yy',
//                 'first_day' => 1,
//             ),
//         ),
//         'location' => array (
//             array (
//                 array (
//                     'param' => 'post_type',
//                     'operator' => '==',
//                     'value' => 'post',
//                     'order_no' => 0,
//                     'group_no' => 0,
//                 ),
//             ),
//         ),
//         'options' => array (
//             'position' => 'normal',
//             'layout' => 'default',
//             'hide_on_screen' => array (
//             ),
//         ),
//         'menu_order' => 0,
//     ));
// }
//
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_featured-image',
        'title' => 'Featured Image',
        'fields' => array (
            array (
                'key' => 'field_51d6de816b060',
                'label' => 'Upload your image:',
                'name' => 'image',
                'type' => 'image',
                'save_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array (
                
            ),
            // 'hide_on_screen' => array (
            //     0 => 'featured_image',
            // ),
        ),
        'menu_order' => 0,
    ));
}

















































// if(function_exists("register_field_group"))
// {
//     register_field_group(array (
//         'id' => 'acf_news-information',
//         'title' => 'News Information',
//         'fields' => array (
//             array (
//                 'key' => 'field_523733a9ebf9a',
//                 'label' => 'Admin Notes',
//                 'name' => 'admin_notes',
//                 'type' => 'wysiwyg',
//                 'default_value' => '',
//                 'toolbar' => 'full',
//                 'media_upload' => 'yes',
//             ),
//             array (
//                 'key' => 'field_523733a9ecea2',
//                 'label' => 'Location',
//                 'name' => 'photo-location',
//                 'type' => 'radio',
//                 'choices' => array (
//                     'Harajuku' => 'Harajuku',
//                     'Shibuya' => 'Shibuya',
//                     'Shinjuju' => 'Shinjuju',
//                     'Daikanyama' => 'Daikanyama',
//                     'Shimokitazawa' => 'Shimokitazawa',
//                 ),
//                 'other_choice' => 0,
//                 'save_other_choice' => 0,
//                 'default_value' => 'Harajuku',
//                 'layout' => 'vertical',
//             ),
//             array (
//                 'key' => 'field_523733a9ede0f',
//                 'label' => 'Contributors',
//                 'name' => 'contributors',
//                 'type' => 'repeater',
//                 'sub_fields' => array (
//                     array (
//                         'key' => 'field_51bdd9800df93',
//                         'label' => 'Contributor Type',
//                         'name' => 'contributor_type',
//                         'type' => 'select',
//                         'column_width' => '',
//                         'choices' => array (
//                             'author' => 'Author',
//                             'photographer' => 'Photographer',
//                             'hunter' => 'Hunter',
//                         ),
//                         'default_value' => '',
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                     array (
//                         'key' => 'field_51bddb110df94',
//                         'label' => 'Contributor',
//                         'name' => 'contributor',
//                         'type' => 'post_object',
//                         'column_width' => '',
//                         'post_type' => array (
//                             0 => 'contributors',
//                         ),
//                         'taxonomy' => array (
//                             0 => 'all',
//                         ),
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                 ),
//                 'row_min' => 0,
//                 'row_limit' => '',
//                 'layout' => 'table',
//                 'button_label' => 'Add Contributor',
//             ),
//             array (
//                 'key' => 'field_523733a9eed06',
//                 'label' => 'Snap Date',
//                 'name' => 'snap_date',
//                 'type' => 'date_picker',
//                 'date_format' => 'yymmdd',
//                 'display_format' => 'dd/mm/yy',
//                 'first_day' => 1,
//             ),
//         ),
//         'location' => array (
//             array (
//                 array (
//                     'param' => 'post_type',
//                     'operator' => '==',
//                     'value' => 'post',
//                     'order_no' => 0,
//                     'group_no' => 0,
//                 ),
//                 array (
//                     'param' => 'post_category',
//                     'operator' => '==',
//                     'value' => '216',
//                     'order_no' => 1,
//                     'group_no' => 0,
//                 ),
//             ),
//         ),
//         'options' => array (
//             'position' => 'normal',
//             'layout' => 'default',
//             'hide_on_screen' => array (
//             ),
//         ),
//         'menu_order' => 0,
//     ));
//     register_field_group(array (
//         'id' => 'acf_snap-information',
//         'title' => 'Snap Information',
//         'fields' => array (
//             array (
//                 'key' => 'field_51bdd6ec55d8c',
//                 'label' => 'Persons',
//                 'name' => 'persons',
//                 'type' => 'repeater',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd90f0df91',
//                             'operator' => '==',
//                             'value' => 'Harajuku',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'sub_fields' => array (
//                     array (
//                         'key' => 'field_51bdd70155d8d',
//                         'label' => 'Name',
//                         'name' => 'name',
//                         'type' => 'text',
//                         'instructions' => 'This field will be used as person\'s post title',
//                         'column_width' => '',
//                         'default_value' => 'Persons Post Title',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd73555d8e',
//                         'label' => 'Photo #s',
//                         'name' => 'photos',
//                         'type' => 'textarea',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'maxlength' => '',
//                         'formatting' => 'none',
//                     ),
//                     array (
//                         'key' => 'field_5237395bfb912',
//                         'label' => 'Gender',
//                         'name' => 'gender',
//                         'type' => 'radio',
//                         'column_width' => '',
//                         'choices' => array (
//                             'male' => 'Male',
//                             'female' => 'Female',
//                             'unstated' => 'Unstated',
//                         ),
//                         'other_choice' => 0,
//                         'save_other_choice' => 0,
//                         'default_value' => '',
//                         'layout' => 'vertical',
//                     ),
//                     array (
//                         'key' => 'field_51bdd78755d90',
//                         'label' => 'Job',
//                         'name' => 'job',
//                         'type' => 'text',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd79655d91',
//                         'label' => 'Age',
//                         'name' => 'age',
//                         'type' => 'text',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7ab55d94',
//                         'label' => 'Facebook',
//                         'name' => 'facebook',
//                         'type' => 'website',
//                         'column_width' => '',
//                         'website_title' => 'true',
//                         'internal_link' => 'false',
//                         'value' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dd55d98',
//                         'label' => 'Twitter',
//                         'name' => 'twitter',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dc55d97',
//                         'label' => 'Instagram',
//                         'name' => 'instagram',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7dc55d96',
//                         'label' => 'Tumblr',
//                         'name' => 'tumblr',
//                         'type' => 'text',
//                         'instructions' => 'Enter the user name of this social network',
//                         'column_width' => '',
//                         'default_value' => '',
//                         'placeholder' => '',
//                         'prepend' => '',
//                         'append' => '',
//                         'formatting' => 'none',
//                         'maxlength' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd7d655d95',
//                         'label' => 'Blog',
//                         'name' => 'blog',
//                         'type' => 'website',
//                         'column_width' => '',
//                         'website_title' => 'true',
//                         'internal_link' => 'false',
//                         'value' => '',
//                     ),
//                     array (
//                         'key' => 'field_51bdd81755d99',
//                         'label' => 'Brands - Outer',
//                         'name' => 'brands_outer',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84655da0',
//                         'label' => 'Brands - Tops',
//                         'name' => 'brands_tops',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84555d9f',
//                         'label' => 'Brands - Bottoms',
//                         'name' => 'brands_bottoms',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84455d9e',
//                         'label' => 'Brands - Bag',
//                         'name' => 'brands_bag',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84455d9d',
//                         'label' => 'Brands - Shoes',
//                         'name' => 'brands_shoes',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84355d9c',
//                         'label' => 'Brands - Accessories',
//                         'name' => 'brands_accessories',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84255d9b',
//                         'label' => 'Favorite Brands/Shops',
//                         'name' => 'favorite_brands',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'brands',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                     array (
//                         'key' => 'field_51bdd84155d9a',
//                         'label' => 'Favorite Music',
//                         'name' => 'favorite_music',
//                         'type' => 'taxonomy_select',
//                         'column_width' => '',
//                         'taxonomy' => 'music',
//                         'field_type' => 'multi_select',
//                         'allow_null' => 1,
//                         'multiple' => true,
//                         'return_format' => 'id',
//                     ),
//                 ),
//                 'row_min' => 0,
//                 'row_limit' => '',
//                 'layout' => 'row',
//                 'button_label' => 'Add New Person',
//             ),
//             array (
//                 'key' => 'field_51bdd8e80df90',
//                 'label' => 'Admin Notes',
//                 'name' => 'admin_notes',
//                 'type' => 'wysiwyg',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'default_value' => '',
//                 'toolbar' => 'full',
//                 'media_upload' => 'yes',
//             ),
//             array (
//                 'key' => 'field_51bdd90f0df91',
//                 'label' => 'Location',
//                 'name' => 'photo-location',
//                 'type' => 'radio',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd6ac55d8b',
//                             'operator' => '==',
//                             'value' => 'yes',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'choices' => array (
//                     'Harajuku' => 'Harajuku',
//                     'Shibuya' => 'Shibuya',
//                     'Shinjuju' => 'Shinjuju',
//                     'Daikanyama' => 'Daikanyama',
//                     'Shimokitazawa' => 'Shimokitazawa',
//                 ),
//                 'other_choice' => 0,
//                 'save_other_choice' => 0,
//                 'default_value' => 'Harajuku',
//                 'layout' => 'vertical',
//             ),
//             array (
//                 'key' => 'field_51bdd96c0df92',
//                 'label' => 'Contributors',
//                 'name' => 'contributors',
//                 'type' => 'repeater',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd90f0df91',
//                             'operator' => '==',
//                             'value' => 'Harajuku',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'sub_fields' => array (
//                     array (
//                         'key' => 'field_51bdd9800df93',
//                         'label' => 'Contributor Type',
//                         'name' => 'contributor_type',
//                         'type' => 'select',
//                         'column_width' => '',
//                         'choices' => array (
//                             'author' => 'Author',
//                             'photographer' => 'Photographer',
//                             'hunter' => 'Hunter',
//                         ),
//                         'default_value' => '',
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                     array (
//                         'key' => 'field_51bddb110df94',
//                         'label' => 'Contributor',
//                         'name' => 'contributor',
//                         'type' => 'post_object',
//                         'column_width' => '',
//                         'post_type' => array (
//                             0 => 'contributors',
//                         ),
//                         'taxonomy' => array (
//                             0 => 'all',
//                         ),
//                         'allow_null' => 0,
//                         'multiple' => 0,
//                     ),
//                 ),
//                 'row_min' => 0,
//                 'row_limit' => '',
//                 'layout' => 'table',
//                 'button_label' => 'Add Contributor',
//             ),
//             array (
//                 'key' => 'field_51bddb670df95',
//                 'label' => 'Snap Date',
//                 'name' => 'snap_date',
//                 'type' => 'date_picker',
//                 'conditional_logic' => array (
//                     'status' => 1,
//                     'rules' => array (
//                         array (
//                             'field' => 'field_51bdd90f0df91',
//                             'operator' => '==',
//                             'value' => 'Harajuku',
//                         ),
//                     ),
//                     'allorany' => 'all',
//                 ),
//                 'date_format' => 'yymmdd',
//                 'display_format' => 'dd/mm/yy',
//                 'first_day' => 1,
//             ),
//         ),
//         'location' => array (
//             array (
//                 array (
//                     'param' => 'post_type',
//                     'operator' => '==',
//                     'value' => 'post',
//                     'order_no' => 0,
//                     'group_no' => 0,
//                 ),
//                 array (
//                     'param' => 'post_category',
//                     'operator' => '==',
//                     'value' => '697',
//                     'order_no' => 1,
//                     'group_no' => 0,
//                 ),
//             ),
//         ),
//         'options' => array (
//             'position' => 'normal',
//             'layout' => 'default',
//             'hide_on_screen' => array (
//             ),
//         ),
//         'menu_order' => 0,
//     ));
//     register_field_group(array (
//         'id' => 'acf_using-new-functionality',
//         'title' => 'Using new functionality',
//         'fields' => array (
//             array (
//                 'key' => 'field_52373842b8daa',
//                 'label' => 'Using new functionality',
//                 'name' => 'check',
//                 'type' => 'checkbox',
//                 'choices' => array (
//                     'yes' => 'Yes',
//                 ),
//                 'default_value' => '',
//                 'layout' => 'vertical',
//             ),
//         ),
//         'location' => array (
//             array (
//                 array (
//                     'param' => 'post_type',
//                     'operator' => '==',
//                     'value' => 'post',
//                     'order_no' => 0,
//                     'group_no' => 0,
//                 ),
//             ),
//         ),
//         'options' => array (
//             'position' => 'side',
//             'layout' => 'default',
//             'hide_on_screen' => array (
//             ),
//         ),
//         'menu_order' => 0,
//     ));
// }
