(function($){
	
	
	/*
	*  acf/setup_fields
	*
	*  This event is triggered when ACF adds any new elements to the DOM. 
	*
	*  @type	function
	*  @since	1.0.0
	*  @date	01/01/12
	*
	*  @param	event		e: an event object. This can be ignored
	*  @param	Element		postbox: An element which contains the new HTML
	*
	*  @return	N/A
	*/
	
	$(document).live('acf/setup_fields', function(e, postbox){
		
		var flag = $(postbox).attr('id') == 'poststuff';

		tf_brands_ajax( flag, $(postbox) )
	
	});


	function tf_brands_ajax( flag, $element ){

		var selector = ( flag ) 
							? $element.find('.repeater .row .js-select2-init') 
							: $element.find('.js-select2-init')


		console.log( 'flag - ' + flag );		

		// console.log( $element );

			$.each(selector ,function(i, v){


				// console.log( $(v).find('select').find(':selected') );



				// $(v).find('select').find(':selected')
				var is_new = $(v).find('.f_brands').data().newfield;
				var ussed  = $(v).find('.f_brands').hasClass('ussed');


				var hidden = $(v).find('.f_brands');

				// console.log( 'new - '  + is_new     )
				// console.log( 'used - ' + ussed      )
				// // console.log( $(v).find('.f_brands') )

				// console.log(  $(v).find('.f_brands').prop('class')  );


			
				// console.log( $(hidden).data('select2') );
				// console.log( is_new );






				// if( !ussed && is_new ){






					// console.log('edit');
					var op_array     = [];

					$(v).find('select').find(':selected').each(function(index, option){

						if( $(option).val() )
							op_array.push( { id: $(option).val(), text: $(option).html() } );						
					});


					/*hardcode*/
					// if ( !$(hidden).data('select2') ){

					// if ( !$(hidden).hasClass('ussed') ) {

						if ( flag ){

					$(hidden).select2({

			            minimumInputLength: 3,
			            multiple: true,
			            placeholder: "Enter at least one character",
			            width: 'resolve' ,
			            tokenSeparators: [',', ';'],
			            /* added */
			            // allowNewValues: true,
			            /* added */
			            ajax: {
			                // url: 'http://clients.netweightit.com/tokyofashion2/wp-admin/admin-ajax.php',
			                url: ajax_url,
			                dataType: 'json',
			                data: function ( term ) {

			                	// console.log('results callback');

			                    // return { term: term, name: $(this).data().taxonomy, action: 'ajax_taxonomy' };
			                    return { term: term, name: $(hidden).data().taxonomy, action: 'reload_taxonomy' };
			                    
			                },
			                results: function (data) {
			                    return { results: data };
			                }
			            },

			            /* additional */
			            createSearchChoice:function(term, data) { 

				             if ( $(data).filter( function() { 
				               return ( this.text.toLowerCase().localeCompare( term ) === 0 ) || ( this.text.localeCompare( term ) === 0 ); 
				             }).length === 0) { 
				               return { id:term, text:term }; 
				               //return { id:term, text:term + " (new)" }; 
				             } 
				        } 
			            /* end additional */


					}).on('change', function(e){

						// console.log('change');
						var html = ''
						$.each($(this).select2('data'), function(index, option){

							html += '<option value="' + option.id + '" selected="selected">' + option.text + '</option>';

						})

						$(v).find('select.select2-acf-wrapper').html( html );

					}).on("select2-blur", function (e) {

					    setTimeout(function () {

					        var active = $(this).hasClass('select2-container-active');

					        if (active) {
					            return false;
					        }
					        $(hidden).select2('close');
					    }, 300);

					});



					} else {
						

						console.log( adminpage );


						if ( adminpage == 'post-new-php' ){


							if( !ussed ){
								$(hidden).addClass('ussed');	
							} else {

						// if( ussed ){


								$(hidden).select2({

						            minimumInputLength: 3,
						            multiple: true,
						            placeholder: "Enter at least one character",
						            width: 'resolve' ,
						            tokenSeparators: [',', ';'],
						            /* added */
						            // allowNewValues: true,
						            /* added */
						            ajax: {
						                // url: 'http://clients.netweightit.com/tokyofashion2/wp-admin/admin-ajax.php',
						                url: ajaxurl,
						                dataType: 'json',
						                data: function ( term ) {

						                	// console.log('results callback');
											console.log(term);
											console.table($(hidden).data("taxonomy"));
											console.log('reload_taxonomy' );

						                    // return { term: term, name: $(this).data().taxonomy, action: 'ajax_taxonomy' };
						                    return { term: term, name: $(hidden).data("taxonomy"), action: 'reload_taxonomy' };
						                    
						                },
						                results: function (data) {
											console.log('results');
											console.log(data);
						                    return { results: data };
						                }
						            },

						            /* additional */
						            createSearchChoice:function(term, data) { 

							             if ( $(data).filter( function() { 
							               return ( this.text.toLowerCase().localeCompare( term ) === 0 ) || ( this.text.localeCompare( term ) === 0 ); 
							             }).length === 0) { 
							               return { id:term, text:term }; 
							               //return { id:term, text:term + " (new)" }; 
							             } 
							        } 
						            /* end additional */


								}).on('change', function(e){

									// console.log('change');
									var html = ''
									$.each($(this).select2('data'), function(index, option){

										html += '<option value="' + option.id + '" selected="selected">' + option.text + '</option>';

									})

									$(v).find('select.select2-acf-wrapper').html( html );

								}).on("select2-blur", function (e) {

								    setTimeout(function () {

								        var active = $(this).hasClass('select2-container-active');

								        if (active) {
								            return false;
								        }
								        $(hidden).select2('close');
								    }, 300);

								});

					



							}


						} else {

$(hidden).select2({

						            minimumInputLength: 3,
						            multiple: true,
						            placeholder: "Enter at least one character",
						            width: 'resolve' ,
						            tokenSeparators: [',', ';'],
						            /* added */
						            // allowNewValues: true,
						            /* added */
						            ajax: {
						                // url: 'http://clients.netweightit.com/tokyofashion2/wp-admin/admin-ajax.php',
						                url: ajax_url,
						                dataType: 'json',
						                data: function ( term ) {

						                	// console.log('results callback');

						                    // return { term: term, name: $(this).data().taxonomy, action: 'ajax_taxonomy' };
						                    return { term: term, name: $(this).data().taxonomy, action: 'reload_taxonomy' };
						                    
						                },
						                results: function (data) {
						                    return { results: data };
						                }
						            },

						            /* additional */
						            createSearchChoice:function(term, data) { 

							             if ( $(data).filter( function() { 
							               return ( this.text.toLowerCase().localeCompare( term ) === 0 ) || ( this.text.localeCompare( term ) === 0 ); 
							             }).length === 0) { 
							               return { id:term, text:term }; 
							               //return { id:term, text:term + " (new)" }; 
							             } 
							        } 
						            /* end additional */


								}).on('change', function(e){

									// console.log('change');
									var html = ''
									$.each($(this).select2('data'), function(index, option){

										html += '<option value="' + option.id + '" selected="selected">' + option.text + '</option>';

									})

									$(v).find('select.select2-acf-wrapper').html( html );

								}).on("select2-blur", function (e) {

								    setTimeout(function () {

								        var active = $(this).hasClass('select2-container-active');

								        if (active) {
								            return false;
								        }
								        $(hidden).select2('close');
								    }, 300);

								});




						}


						

					}


					if( op_array.length ) $(hidden).select2('data', op_array)





				// }


				// if( ussed && !is_new ){


				// 	/*hardcode*/

				// 		$(hidden).select2({

			 //            minimumInputLength: 3,
			 //            multiple: true,
			 //            placeholder: "Enter at least one character",
			 //            width: 'resolve' ,
			 //            tokenSeparators: [',', ';'],
			 //            /* added */
			 //            // allowNewValues: true,
			 //            /* added */
			 //            ajax: {
			 //                // url: 'http://clients.netweightit.com/tokyofashion2/wp-admin/admin-ajax.php',
			 //                url: ajax_url,
			 //                dataType: 'json',
			 //                data: function ( term ) {

			 //                	// console.log('results callback');

			 //                    // return { term: term, name: $(this).data().taxonomy, action: 'ajax_taxonomy' };
			 //                    return { term: term, name: $(this).data().taxonomy, action: 'reload_taxonomy' };
			                    
			 //                },
			 //                results: function (data) {
			 //                    return { results: data };
			 //                }
			 //            },

			 //            /* additional */
			 //            createSearchChoice:function(term, data) { 

				//              if ( $(data).filter( function() { 
				//                return ( this.text.toLowerCase().localeCompare( term ) === 0 ) || ( this.text.localeCompare( term ) === 0 ); 
				//              }).length === 0) { 
				//                return { id:term, text:term }; 
				//                //return { id:term, text:term + " (new)" }; 
				//              } 
				//         } 
			 //            /* end additional */


				// 	}).on('change', function(e){

				// 		// console.log('change');
				// 		var html = ''
				// 		$.each($(this).select2('data'), function(index, option){

				// 			html += '<option value="' + option.id + '" selected="selected">' + option.text + '</option>';

				// 		})

				// 		$(v).find('select.select2-acf-wrapper').html( html );

				// 	}).on("select2-blur", function (e) {

				// 	    setTimeout(function () {

				// 	        var active = $(this).hasClass('select2-container-active');

				// 	        if (active) {
				// 	            return false;
				// 	        }
				// 	        $(hidden).select2('close');
				// 	    }, 300);

				// 	});
				// 	//end of select2 call


				// 	/*hardcode*/
				// 	console.log('new');	




				// 	$(v).find('.f_brands').addClass('ussed');
				// }

				// if( ussed && !is_new ){ 



				// }

				// console.log( 'one itteration' );


			} )
			
	}

})(jQuery);