<?php

class acf_field_taxonomy_select extends acf_field
{
	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options
		
		
	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	3.6
	*  @date	23/01/13
	*/
	
	function __construct()
	{
		// vars
		$this->name     = 'taxonomy_select';
		$this->label    = __('Taxonomy Select2');
		$this->category = __("Relational",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			'taxonomy' 			=> 'brands',
			'field_type' 		=> 'multi_select',
			'allow_null' 		=> 1,
			// 'load_save_terms' 	=> 0,
			'multiple'			=> true,
			'return_format'		=> 'id'
		);
		
		
		// do not delete!
    	parent::__construct();
    	
    	
    	// settings
		$this->settings = array(
			'path' => apply_filters('acf/helpers/get_path', __FILE__),
			'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
			'version' => '1.0.0'
		);


        // JSON
        add_action('wp_ajax_ajax_func',        array($this, 'ajax_taxonomy'), 10, 1);
        add_action('wp_ajax_nopriv_ajax_func', array($this, 'ajax_taxonomy'), 10, 1);

	}
	
	
	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field	- an array holding all the field's data
	*/
	
	function create_options( $field )
	{
		// vars
		$key = $field['name'];
		
		?>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Taxonomy",'acf'); ?></label>
	</td>
	<td>
		<?php
		
		$choices = array();
		$taxonomies = get_taxonomies( array('public' => true), 'objects' );
		
		foreach($taxonomies as $taxonomy)
		{
			$choices[ $taxonomy->name ] = $taxonomy->labels->name;
		}
		
		// unset post_format (why is this a public taxonomy?)
		if( isset($choices['post_format']) )
		{
			unset( $choices['post_format']) ;
		}
				
		do_action('acf/create_field', array(
			'type'	=>	'select',
			'name'	=>	'fields['.$key.'][taxonomy]',
			'value'	=>	$field['taxonomy'],
			'choices' => $choices,
		));
		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Field Type",'acf'); ?></label>
	</td>
	<td>
		<?php	
		do_action('acf/create_field', array(
			'type'	=>	'select',
			'name'	=>	'fields['.$key.'][field_type]',
			'value'	=>	$field['field_type'],
			'optgroup' => true,
			'choices' => array(
				__("Multiple Values",'acf') => array(

					'multi_select' => __('Multi Select', 'acf')
				),

			)
		));
		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Allow Null?",'acf'); ?></label>
	</td>
	<td>
		<?php 
		do_action('acf/create_field', array(
			'type'	=>	'radio',
			'name'	=>	'fields['.$key.'][allow_null]',
			'value'	=>	$field['allow_null'],
			'choices'	=>	array(
				1	=>	__("Yes",'acf'),
				// 0	=>	__("No",'acf'),
			),
			'layout'	=>	'horizontal',
		));
		?>
	</td>
</tr>



<?php if ( 0 ) { ?>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Load & Save Terms to Post",'acf'); ?></label>
	</td>
	<td>
		<?php	
		do_action('acf/create_field', array(
			'type'	=>	'true_false',
			'name'	=>	'fields['.$key.'][load_save_terms]',
			'value'	=>	$field['load_save_terms'],
			'message' => __("Load value based on the post's terms and update the post's terms on save",'acf')
		));
		?>
	</td>
</tr> 
<?php } ?>




<?php if ( 0 ) { ?>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Return Value",'acf'); ?></label>
	</td>
	<td>
		<?php
		do_action('acf/create_field', array(
			'type'		=>	'radio',
			'name'		=>	'fields['.$key.'][return_format]',
			'value'		=>	$field['return_format'],
			'layout'	=>	'horizontal',
			'choices'	=> array(
				// 'object'	=>	__("Term Object",'acf'),
				'id'		=>	__("Term ID",'acf')
			)
		));
		?>
	</td>
</tr>
<?php } ?>




		<?php
		
	}
	
	
	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/
	
	function create_field( $field )
	{

		// vars
		$single_name = $field['name'];
		$field['multiple'] = 1;
		$field['field_type'] = 'select';
		$field['name'] .= '[]';


		if( empty( $field['value'] ) ){
			$dataNew = 'true';
		} else {
			$dataNew = 'false';
		}
		// var_dump( $field['value'] );

		// value must be array!
		if( !is_array($field['value']) )
		{
			$field['value'] = array( $field['value'] );
		}

		?>
	<div class="acf-taxonomy-field js-select2-init">
		<input type="hidden" name="<?php echo $single_name; ?>" value="" />

		<input type="hidden" class="f_brands" data-taxonomy="<?php echo $field['taxonomy']; ?>" data-hidden="<?php echo $single_name; ?>" data-newfield="<?php echo $dataNew; ?>" value="" style="width:170px;" />


		<select class="select2-acf-wrapper" name="<?php echo $field['name']; ?>" style="display:none;"
			    <?php if( $field['multiple'] ): ?>multiple="multiple" size="5"<?php endif; ?> >

			<?php if( $field['allow_null'] ): ?>
				<option value=""><?php _e("None", 'acf'); ?></option>
			<?php endif; ?>

			<?php

				foreach ($field['value'] as $key => $value1) {

					$term = get_term_by( 'id', $value1, $field['taxonomy'] );

			?>
					<option value="<?php echo $term->term_id; ?>" selected="selected">
						<?php echo $term->name; ?>
					</option>;

				<?php }

				// wp_list_categories( array(
				// 	'taxonomy'      => $field['taxonomy'],
				// 	'hide_empty'   => false,
				// 	'style'        => 'none',
				// 	'walker'       => new acf_taxonomy_field_walker( $field ),
				// ));

			?>

		</select>

	</div>
	<?php
	
	}
	
	
	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add css + javascript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_enqueue_scripts()
	{
		// Note: This function can be removed if not used
		
			


		// register acf scripts
		wp_register_script( 'select2', $this->settings['dir'] . 'js/select2.js', array('acf-input'), $this->settings['version'] );
		
		// wp_register_script( 'select2', $this->settings['dir'] . 'js/select2.min.js', array('acf-input'), $this->settings['version'] );
		wp_register_script( 'acf-input-taxonomy_select', $this->settings['dir'] . 'js/input.js', array('acf-input'), $this->settings['version'] );

		wp_register_style(  'select2-style', $this->settings['dir'] . 'css/select2.css', array('acf-input'), $this->settings['version'] ); 
		wp_register_style(  'acf-input-taxonomy_select', $this->settings['dir'] . 'css/input.css', array('acf-input'), $this->settings['version'] ); 
		
		
		// scripts

		wp_enqueue_script(array(
			'select2',	
		));

		wp_enqueue_script(array(
			'acf-input-taxonomy_select',	
		));

		// styles

		wp_enqueue_style(array(
			'select2-style',	
		));

		wp_enqueue_style(array(
			'acf-input-taxonomy_select',	
		));


	}
	
	
	/*
	*  input_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is created.
	*  Use this action to add css and javascript to assist your create_field() action.
	*
	*  @info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_head
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_head()
	{
		// Note: This function can be removed if not used

        ?>
        <script>

            var ajax_url = "<?php echo admin_url( 'admin-ajax.php' ); ?>" ;

        </script>
        <?php

	}
	
	
	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add css + javascript to assist your create_field_options() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function field_group_admin_enqueue_scripts()
	{
		// Note: This function can be removed if not used
	}

	
	/*
	*  field_group_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is edited.
	*  Use this action to add css and javascript to assist your create_field_options() action.
	*
	*  @info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_head
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function field_group_admin_head()
	{
		// Note: This function can be removed if not used
	}


	/*
	*  load_value()
	*
	*  This filter is appied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value - the value found in the database
	*  @param	$post_id - the $post_id from which the value was loaded from
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the value to be saved in te database
	*/
	
	function load_value( $value, $post_id, $field )
	{
		// if( $field['load_save_terms'] )
		// {
		// 	$value = array();
			
		// 	$terms = get_the_terms( $post_id, $field['taxonomy'] );
			
		// 	if( is_array($terms) ){ foreach( $terms as $term ){
				
		// 		$value[] = $term->term_id;
				
		// 	}}
			
		// }
		

		return $value;
	}
	
	/*
	*  update_value()
	*
	*  This filter is appied to the $value before it is updated in the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value - the value which will be saved in the database
	*  @param	$post_id - the $post_id of which the value will be saved
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the modified value
	*/

	function update_value( $value, $post_id, $field )
	{

		// var_dump( $value );
		// echo '<br />';

		$buff = array();
		if( is_array($value) )
		{

			$value = array_filter($value);

			foreach ( $value as $k1 => $term ) {
				
				if ( !strlen(trim($term)) || is_numeric( $term ) ){
					$buff[] = $term;
					// continue;
				} else {


					if ( !taxonomy_exists( $field['taxonomy'] ) )
						return new WP_Error('invalid_taxonomy', __('Invalid taxonomy'));

					$term_exist = term_exists($term, $field['taxonomy']);


// if ($term_exist !== 0 && $term_exist !== null) {
//   echo "'Uncategorized' category exists!";
// }
					$existing = term_exists($term, $field['taxonomy']);
					if ( !$existing ){

						// var_dump( $term );
						// echo '<br />';
						$term_info = wp_insert_term( $term, $field['taxonomy'] );
						// var_dump( $term_info );
						// echo '<br />';

						$buff[] = (string) $term_info['term_id'];
						
						// $value[$k1] = 
					} else {

						// $existing = term_exists($term, $field['taxonomy']);
						$buff[] = (string) $existing['term_id'];

					}

				}

					

				// if ( is_numeric( $term ) ){
				// 	$buff[] = $term;
				// 	// continue;
				// }
					

				// if ( !taxonomy_exists( $field['taxonomy'] ) )
				// 	return new WP_Error('invalid_taxonomy', __('Invalid taxonomy'));

				

				// if ( !term_exists($term, $field['taxonomy']) ){

				// 	var_dump( $term );
				// 	echo '<br />';
				// 	$term_info = wp_insert_term( $term, $field['taxonomy'] );
				// 	var_dump( $term_info );
				// 	echo '<br />';

				// 	$buff[] = (string) $term_info['term_id'];
					
				// 	// $value[$k1] = 
				// }

			}

			
		} else {

		}


		// var_dump( $value );
		// echo '<br />';

		// var_dump( $buff );
		// if( $field['load_save_terms'] )
		// {
		// 	// Parse values
		// 	$value = apply_filters( 'acf/parse_types', $value );
		
		// 	wp_set_object_terms( $post_id, $value, $field['taxonomy'], false );
		// }
		
		// var_dump( $buff );

		return $buff;


	}


	// function update_value( $value, $post_id, $field )
	// {
	// 	// vars
	// 	if( is_array($value) )
	// 	{
	// 		$value = array_filter($value);
	// 	}
		
		
	// 	// if( $field['load_save_terms'] )
	// 	// {
	// 	// 	// Parse values
	// 	// 	$value = apply_filters( 'acf/parse_types', $value );
		
	// 	// 	wp_set_object_terms( $post_id, $value, $field['taxonomy'], false );
	// 	// }
		
		
	// 	return $value;



	// }
	
	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is passed to the create_field action
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/
	
	function format_value( $value, $post_id, $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/
		
		// perhaps use $field['preview_size'] to alter the $value?
		
		
		// Note: This function can be removed if not used

		return $value;
	}
	
	
	/*
	*  format_value_for_api()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is passed back to the api functions such as the_field
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/
	
	function format_value_for_api( $value, $post_id, $field )
	{
		// no value?
		if( !$value )
		{
			return $value;

		}
		
		
		// temp convert to array
		$is_array = true;
		
		if( !is_array($value) )
		{
			$is_array = false;
			$value = array( $value );
		}
		
		
		// format
		if( $field['return_format'] == 'object' )
		{
			foreach( $value as $k => $v )
			{
				$value[ $k ] = get_term( $v, $field['taxonomy'] );
			}
		}
		
		
		// de-convert from array
		if( !$is_array && isset($value[0]) )
		{
			$value = $value[0];
		}

		// Note: This function can be removed if not used
		return $value;
	}
	
	/*
	*  load_field()
	*
	*  This filter is appied to the $field after it is loaded from the database
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$field - the field array holding all the field options
	*/
	
	function load_field( $field )
	{
		// Note: This function can be removed if not used

		return $field;



	}
	
	
	/*
	*  update_field()
	*
	*  This filter is appied to the $field before it is saved to the database
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field - the field array holding all the field options
	*  @param	$post_id - the field group ID (post_type = acf)
	*
	*  @return	$field - the modified field
	*/

	function update_field( $field, $post_id )
	{
		// Note: This function can be removed if not used
		return $field;
	}

	public function ajax_taxonomy(){

        global $wpdb;
        $slug =  $_GET['term'] . '%';
        //not in the middle of term
        // $slug =  '%' . $_GET['term'] . '%';
        $brands_fields = array( 'brands' );

        if( in_array( $_GET['name'], $brands_fields ) )
            $taxonomy = 'brands';
        else
            $taxonomy = 'music';

            $query = "SELECT wp_term_taxonomy.term_id as id, wp_terms.name as text
         FROM   wp_terms
           JOIN wp_term_taxonomy USING (term_id)
         WHERE  wp_term_taxonomy.taxonomy = '%s'
            AND wp_terms.name LIKE '%s'";

            $sql_results = $wpdb->get_results( $wpdb->prepare($query, array($taxonomy, $slug)), ARRAY_A );
            echo json_encode($sql_results);

            die();

    }







}


// create field
new acf_field_taxonomy_select();

if ( !class_exists( 'acf_taxonomy_field_walker' ) ){

class acf_taxonomy_field_walker extends Walker
{
	// vars
	var $field = null,
		$tree_type = 'category',
		$db_fields = array ( 'parent' => 'parent', 'id' => 'term_id' );


	// construct
	function __construct( $field )
	{
		$this->field = $field;
	}

	
	// start_el
	function start_el( &$output, $term, $depth, $args = array(), $current_object_id = 0)
	{
		// vars
		$selected = in_array( $term->term_id, $this->field['value'] );
		
		// if( $this->field['field_type'] == 'select' )
		// {
			$indent = str_repeat("&mdash;", $depth);
			$output .= '<option value="' . $term->term_id . '" ' . ($selected ? 'selected="selected"' : '') . '>' . $indent . ' ' . $term->name . '</option>';
		// }
		
	}
	
	
	//end_el
	function end_el( &$output, $term, $depth = 0, $args = array() )
	{
		if( in_array($this->field['field_type'], array('checkbox', 'radio')) )
		{
			$output .= '</li>';
		}
		
		$output .= "\n";
	}
	
	
	// start_lvl
	function start_lvl( &$output, $depth = 0, $args = array() )
	{
		// indent
		//$output .= str_repeat( "\t", $depth);
		
		
		// wrap element
		if( in_array($this->field['field_type'], array('checkbox', 'radio')) )
		{
			$output .= '<ul class="children">' . "\n";
		}
	}

	
	// end_lvl
	function end_lvl( &$output, $depth = 0, $args = array() )
	{
		// indent
		//$output .= str_repeat( "\t", $depth);
		
		
		// wrap element
		if( in_array($this->field['field_type'], array('checkbox', 'radio')) )
		{
			$output .= '</ul>' . "\n";
		}
	}
	
}


}




?>