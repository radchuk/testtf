<?php get_header(); ?>

<?php global $wp_query; ?>

<div id="content">

	<?php $queried_object = get_queried_object(); ?>

	<h3 class="contentpaneltitle">

		Articles that sound like:

		<?php echo $queried_object->name; ?>

	</h3>

	<?php while (have_posts()) : the_post(); ?>

		<div class="snippet">

			<div class="snippet-left">

                <?php get_template_part( '/partials/tokyo', 'image_small' ); ?>

            </div>

			<div class="snippet-right">

				<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

				<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?> in <?php the_category(', '); ?></p>

				<p class="snippet-intro"><?php the_excerpt(); ?></p>

				<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>

			</div>

		</div><!-- .snippet -->

	<?php endwhile;?>

	<div class="navigation">

		<div class="navleft"><?php next_posts_link('&laquo; Older Posts', '0') ?></div>

		<div class="navright"><?php previous_posts_link('Newer Posts &raquo;', '0') ?></div>

	</div>

</div><!-- #content -->

<?php get_sidebar() ?>

<?php get_footer(); ?>