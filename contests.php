<?php
/*
Template Name: Contests Page
*/
?>
<?php get_header() ?>

	<div id="content">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post" style="margin-bottom:0px;">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
		</div> 	
		
<?php
 $querystr = "
    SELECT wposts.* 
    FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
    WHERE wposts.ID = wpostmeta.post_id 
    AND wpostmeta.meta_key = 'TF-Contest' 
    AND wpostmeta.meta_value = '1' 
    AND wposts.post_type = 'post'
    AND wposts.post_status = 'publish'   
    ORDER BY wposts.post_date DESC";

$pageposts = $wpdb->get_results($querystr, OBJECT);

//print_r($pageposts);
?>
 
 <?php if ($pageposts): ?>
 <?php 
 $posts_per_page = intval(get_query_var('posts_per_page'));
 $post_counter = 0; 
 $page = intval(get_query_var('page'));
 if(empty($page)){
	$page = 1;
	$start_id = 0;
	$end_id = $posts_per_page - 1;
 }else{
	$start_id = ($page-1)*$posts_per_page;
	$end_id = $page*$posts_per_page;
 }
 $this_page = $page;
 $this_page_id = $post->ID;
 ?>
  <?php while ( $post_counter < $end_id && !empty($pageposts[$post_counter])): ?>
	<?php if($post_counter >= $start_id){?>
	<?php $post = $pageposts[$post_counter];?>
    <?php setup_postdata($post); ?>

<div style="clear:both;"></div>

		<div class="snippet">
			<div class="snippet-left">


                
                <?php    /*******************************************************************************************************************/ ?>
                <?php   get_template_part( '/partials/tokyo', 'image_small2' ); ?>
                <?php   /*******************************************************************************************************************/ ?>



            </div>
			<div class="snippet-right">
				<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?> in <?php the_category(', '); ?></p>
				<p class="snippet-intro"><?php the_excerpt(); ?></p>
				<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>
			</div>
		</div><!-- .snippet -->
		<?php } ?>
		<?php $post_counter++;?>		
		<?php endwhile; ?>
		
		<div class="navigation">
			<div class="navleft">
			<?php if(!empty($pageposts[$post_counter])){ ?>
			<a href="<?php echo add_query_arg (array('page'=>$this_page+1), get_permalink($this_page_id));?>">&laquo; Older Posts</a>
			<?php } ?>	
			</div>
			<div class="navright">			
			<?php if($this_page > 1){ ?>
			<a href="<?php echo add_query_arg (array('page'=>$this_page-1), get_permalink($this_page_id));?>">Newer Posts &raquo;</a>
			<?php } ?>
			</div>
		</div>
<?php else : ?>
    <h2 class="center">Not Found</h2>
    <p class="center">Sorry, but you are looking for something that isn't here.</p>
 <?php endif; ?>
	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>