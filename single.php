<?php get_header(); ?>
<?php
$temp_url = trim($_SERVER["REQUEST_URI"], "/");
$our_url = explode('/', $temp_url);
$id_post = url_to_postid($our_url[0]);
$our_post = get_page($id_post);
$img_lazy_load = get_post_meta($post->ID, 'img-lazy-load', true);
/* This is id Category Pictorials*/
$id_pictorials = '5945';

if (($id_post > 0) AND (isset($our_url[1]))) {
    $vews_full_post = 1;
} else {
    $vews_full_post = 0;
}
?>
<div id="content"
    <?php if (!empty($img_lazy_load)) {
        echo 'class="content_images"';
    }
    if (in_category($id_pictorials)) {
        echo 'style="width:950px;" ';
    } ?>
    >
    <?php the_post(); ?>
    <div id="post-<?php the_ID() ?>" class="post">
        <h1 class="post-title<? if (in_category($id_pictorials)) {
            echo '-pictorials" ';
        } ?>"><?php the_title() ?></h1>

        <div class="post-meta" <? if (in_category($id_pictorials)) {
            echo ' style="float:left;"';
        } ?>>Posted on <?php the_time('F j, Y'); ?> in: <?php the_category(', '); ?><span
                class="sep">|</span><?php comments_popup_link('Be the first to Comment &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
        </div>
        <?php if (in_category($id_pictorials)) { ?>
            <div id="fb-root"></div>

        <?php } ?>
        <div style="clear:both;"></div>
        <div class="post-content">

            <?php get_template_part('social-share', ''); ?>

            <?php th_before_content(); // hook for displaying information before content ?>

            <?php the_content();
            if ($vews_full_post == 1) {
                echo 'Full details: <a title="' . $our_post->post_title . '" href="/' . $our_url[0] . '">' . $our_post->post_title . '</a><br /><br />';
            }
            ?>
            <?php wp_link_pages('before=<p class="my_paginator">' . __('Pages:')); ?>
        </div>
        <div><img src="<?php bloginfo('template_directory'); ?>/images/bottom_of_article_divider.gif" height="13"
                  width="300" alt=""></div>


        <?php if (get_field('check', get_the_ID()))
        {

            if (get_post_type(get_the_ID()) == 'post')
            {
                tokyo_after_post(); //hook for diplaying new version of snap information
            }


        }

        if (function_exists('sociable_html')) {
            echo sociable_html();
        } ?>

    </div>
    <!-- .post -->

    <?php comments_template(); ?>

    <div class="post-content">
        <br/><br/>
        <?php if (function_exists('related_posts')) {
            related_posts();
        } ?>
    </div>

</div><!-- #content -->

<?php if (in_category($id_pictorials) == false) get_sidebar(); ?>
<?php get_footer() ?>
