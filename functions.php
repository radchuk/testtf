<?php
/*
 * Include ACF
 */

require_once('includes/acf/acf_integrate.php');

/*
 * Include P2P
*/
require_once('includes/p2p/posts-to-posts.php');

require_once('includes/theme-actions.php');
/*
 * ************************************
*/
require_once('framework/developer.php');
require_once('framework/custom_rewrite.php');
require_once('framework/admin-script.php');
require_once('framework/custom_post_type/custom_post_type.php');
require_once('framework/p2p-init.php');
require_once('framework/p2p-query.php');


// Fix to prevent WordPress from adding year from url as query var
add_action('pre_get_posts', 'photo_page_hack');
function photo_page_hack() {
    global $wp_query;
    if($wp_query->query['pagename'] == 'photos') {
        unset($wp_query->query_vars['year']);
        unset($wp_query->query['year']);
    }
}

	function truncate($string, $del) {
		$len = strlen($string);
		if ($len > $del) {
			$new = substr($string, 0, $del)."...";
			return $new;
		}
		else return $string;
	}

	remove_filter('the_excerpt', 'wpautop');

	if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '<div class="sidepanel">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

//Adding performance check
define( 'SAVEQUERIES', true );

// Changes to add Brands
add_action( 'init', 'create_my_taxonomies', 0 );
function create_my_taxonomies() {    
	register_taxonomy(
        'brands',
        'post',
        array(
            'hierarchical' => false,
            'label' => 'Brands',
            'query_var' => true,
            'rewrite' => true  ,
            'update_count_callback' => '_update_post_term_count'
        ));

    register_taxonomy( 'music', 'post', array( 'hierarchical' => false, 'label' => 'Music', 'query_var' => true, 'rewrite' => true , 'update_count_callback' => '_update_post_term_count' ));
}
add_filter('manage_posts_columns', 'add_extra_columns');

function add_extra_columns($post_columns) {
    $post_columns['brands'] = __('Brands');
    $post_columns['music'] = __('Music');
    return $post_columns;
}

add_action('manage_posts_custom_column', 'my_extra_columns');

function my_extra_columns($attr) {
    global $post;
    if($attr == 'brands') {
        $brands = get_the_terms($post->ID, 'brands');
        if ( !empty( $brands ) ) {
            $out = array();
            foreach ( $brands as $c )
                $out[] = "<a href='edit.php?brands=$c->slug'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'brands', 'display')) . "</a>";
            echo join( ', ', $out );
        } else {
            _e('No Brands');
        }
    } elseif ($attr == 'music') {
        $music = get_the_terms($post->ID, 'music');
        if ( !empty( $music ) ) {
            $out = array();
            foreach ( $music as $m )
                $out[] = "<a href='edit.php?music=$m->slug'> " . esc_html(sanitize_term_field('name', $m->name, $m->term_id, 'music', 'display')) . "</a>";
            echo join( ', ', $out );
        } else {
            _e('No Music');
        }
	}
}

add_action('admin_head', 'js_for_brands');
add_action('admin_head', 'js_for_music');

// Function to add javascript to replace the text "Add new tag" under Brands post meta box to "Add new brand"
function js_for_brands() {
?>
    <script type="text/javascript">
        jQuery(document).ready( function($) {

            // Change "Separate tags with commas." to "Separate brands with commas." under brands
            jQuery('#tagsdiv-brands').find('.howto').html('Separate brands with commas.');
            // Change "Choose from the most used tags in Brands" to "Choose from the most used Brands" under brands
            jQuery('#tagsdiv-brands').find('#link-brands').html('Choose from the most used Brands');

            function update_input_tips_to_brand() {
                var brandsbox;
                brandsbox = jQuery('#tagsdiv-brands');

                // is the input box empty (i.e. showing the 'Add new tag' tip)?
                if ( brandsbox.find('input.newtag').hasClass('form-input-tip') ) {
                    brandsbox.find('.taghint').html("Add new brand");
                }
            }

            function update_input_tips_to_brand_var_focus() {
                postL10n.addTag = 'Add new tag';
            }
            function update_input_tips_to_brand_var_blur() {
                postL10n.addTag = 'Add new brand';
            }
            jQuery('#new-tag-post_tag').focus(function() { update_input_tips_to_brand_var_focus(); });
            jQuery('#new-tag-brands').blur(function() { update_input_tips_to_brand_var_blur(); });
            update_input_tips_to_brand();
        });
    </script>
<?php
}

// Function to add javascript to replace the text "Add new tag" under Music post meta box to "Add new music"
function js_for_music() {
?>
    <script type="text/javascript">
        jQuery(document).ready( function($) {

            // Change "Separate tags with commas." to "Separate music with commas." under music
            jQuery('#tagsdiv-music').find('.howto').html('Separate music with commas.');
            // Change "Choose from the most used tags in Music" to "Choose from the most used Music" under music
            jQuery('#tagsdiv-music').find('#link-music').html('Choose from the most used Music');

            function update_input_tips_to_music() {
                var musicbox;
                musicbox = jQuery('#tagsdiv-music');

                // is the input box empty (i.e. showing the 'Add new tag' tip)?
                if ( musicbox.find('input.newtag').hasClass('form-input-tip') ) {
                    musicbox.find('.taghint').html("Add new music");
                }
            }

            function update_input_tips_to_music_var_focus() {
                postL10n.addTag = 'Add new tag';
            }
            function update_input_tips_to_music_var_blur() {
                postL10n.addTag = 'Add new music';
            }
            jQuery('#new-tag-post_tag').focus(function() { update_input_tips_to_music_var_focus(); });
            jQuery('#new-tag-music').blur(function() { update_input_tips_to_music_var_blur(); });
            update_input_tips_to_music();
        });
    </script>
<?php
}
// Changes to have custom title on Brands page. eg. "Vivienne Westwood - Brands - TokyoFashion.com"
// Implemented because of interference by All in One SEO and the same is used as reference to create this

add_action('template_redirect', 'replace_title_for_custom_pages', 0);
function replace_title_for_custom_pages() {
    global $wp_query;
    if(is_page('photos') && isset($_GET['location'])) {
        ob_start(replace_title_for_photo_page);
    } else if(is_page('contributors')) {
		if(isset($_GET['cid'])) {
	        ob_start(replace_title_for_contributors_page);
		} else {
			ob_start(replace_title_as_contributors);
		}
    } else {
        if($wp_query->query_vars['taxonomy'] == 'brands') { 
        	ob_start(replace_title_for_brands);
		} elseif($wp_query->query_vars['taxonomy'] == 'music') {
        	ob_start(replace_title_for_music);
		} else {
			return;
		}
    }
}

function replace_title_as_contributors($content) {
	$title = 'Tokyo Fashion Contributors';
	$header = replace_title_common($content, $title);

	return $header;
}

function replace_title_for_contributors_page($content) {
	global $wpdb;
	$cid = $_GET['cid'];
	$sql = $wpdb->prepare("SELECT name FROM $wpdb->contributors WHERE id = %d LIMIT 1", $cid);
	$result = $wpdb->get_var($sql);
	$title = $result . " - Tokyo Fashion Contributors";
	$header = replace_title_common($content, $title);

	return $header;
}

function replace_title_for_photo_page($content) {
    $year = (int)$_GET['year'];
    $month = (int)$_GET['month'];
    $this_month = date('n');
    $this_year = date('Y');

    $title = $_GET['location'] . " Street Fashion Photos - " .date('F Y', mktime(0, 0, 0, $this_month, 1, $this_year));
	$header = replace_title_common($content, $title);

    return $header;

}

function replace_title_for_brands($content) {
    global $wp_query;

    $title =  "TokyoFashion.com - Brands - " . $wp_query->queried_object->name;
	$header = replace_title_common($content, $title);
    return $header;
}

function replace_title_for_music($content) {
    global $wp_query;

    $title = "TokyoFashion.com - Music - " . $wp_query->queried_object->name;
	$header = replace_title_common($content, $title);
    return $header;
}

function replace_title_common($content, $title) {
    $title = trim(strip_tags($title));

    $title_tag_start = "<title>";
    $title_tag_end = "</title>";
    $len_start = strlen($title_tag_start);
    $len_end = strlen($title_tag_end);
    $title = stripcslashes(trim($title));
    $start = strpos($content, $title_tag_start);
    $end = strpos($content, $title_tag_end);

    if ($start && $end) {
        $header = substr($content, 0, $start + $len_start) . $title .  substr($content, $end);
    } else {
        $header = $content;
    }
	return $header;
}

/**
 * Since WP 3.1 there is a 404 error triggers if we pass a GET parameter which match one of taxonomies(custom or builtin).
 * There is 'brand' taxonomy. And there is 'iphone_feed' page that takes 'brands' GET parameter. (/iphone_feed/?brands=217)
 * This filter helps to resolve this collision.
 */
add_filter ('request', 'sanitize_iphone_feed_request');
function sanitize_iphone_feed_request($query_vars)
{
	if ($query_vars['pagename'] == 'iphone_feed')
	{
		unset($query_vars['brands']);
	}

	return $query_vars;
}

/*Open-Graph-image for your Wordpress blog.*/
function header_for_facebook_og_image($buffer)
{	/*
	This is the main function that actually writes the meta tags to the
	appropriate page.
	*/
	global $posts, $include_keywords_in_single_posts, $wpdb;
	
	$my_metatags ='';
	$sql = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'image' AND post_id = '".$posts[0]->ID."'";
	$result = $wpdb->get_results($sql);

	foreach ($result AS $row)	{
		$row->meta_value = str_replace('http://','',$row->meta_value);
		$my_metatags .= get_bloginfo('url')."/facebook_thumb/".$row->meta_value;
	}
	
	return (str_replace('og:image" content=""', 'og:image" content="'.$my_metatags.'"', $buffer));
}


/* NetWeight Start */
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function getBrandsTaxonomy(){
    return 'brands';
}

function getBrandsArticles(){
    return 'brand_articles';
}

function getPersonTypeName(){
    return 'persons';
}

function get_category_id( $cat_name ){
    $term = get_term_by('name', $cat_name, 'category');
    return $term->term_id;
}


add_theme_support('post-thumbnails', array('post','brand_articles'));
add_theme_support('post-thumbnails', array('post','contributors'));

function my_featured( $right = true, $show_empty = true ){

    $id = get_the_ID();
    $html = '';

    if( has_post_thumbnail() ){
        $link = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
        $url  = '/thumb.php?src=' . $link . '&amp;w=140&amp;h=180&amp;zc=1&amp;q=95';
       // logConsole( 'url', $url );
        $thumb_path = $url;
    } else {
        $image       = get_post_meta($id, "image", $single = true);
        $thumb_path  = strlen( $image )
            ? '/thumb.php?src=' . $image . '&amp;w=140&amp;h=180&amp;zc=1&amp;q=95'
            : '/images/nothumb_small.gif';

        if( !strlen( $image ) && !$show_empty )  return;
    }

    $class = ( $right ) ? 'snippet-thumb' : 'alignright';

    echo '<a href="' . get_permalink() . '" title="' . get_the_title() . '"> '.
        '<img src="' . get_template_directory_uri() . $thumb_path . '" class="' .  $class . '"  />' . '</a>';

}


/*
 * Registration Nav Menu
 */
add_action( 'init', 'tf_custom_register_nav_menu' );
function tf_custom_register_nav_menu() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' ),
        )
    );
}


function listRecentBrandsCustom() {

    $count_posts = 60; // count posts
	$outputBrands = array();
	$used_arr = array();
    $args = array(
        'showposts' => $count_posts,
        'post_type' => 'post',
        'fields'    => 'ids'
    );

    $firstQuery = new WP_Query($args);

    while ($firstQuery->have_posts()) : $firstQuery->the_post();

        $connected = new WP_Query( array(
            'post_type' => 'post',
            'connected_type'  => 'post_to_persons',
            'connected_items' => get_post( get_the_ID() ),
            'nopaging'        => true
        ) );
        while ($connected->have_posts()) : $connected->the_post();
            global $post;
            $brandsTerms = p2p_get_meta( $post->p2p_id, 'brands', true ) ;
			if($brandsTerms){
				$terms_ids   = array_values_recursive( $brandsTerms );
				$terms_ids = array_unique($terms_ids);
				foreach($terms_ids as $t_id){
					if(!in_array($t_id,$outputBrands)){
						$outputBrands[$connected->query_vars['connected_items']->ID][] = $t_id;
					}
				}
			
			}
        endwhile;
        wp_reset_postdata();

    endwhile;
    wp_reset_postdata();
	foreach($outputBrands as $key=>$temp_b){
		foreach($temp_b as $t_b){
			if(!in_array($t_b,$used_arr)){
				$outputBrands_[$key][] = $t_b;
				$used_arr[] = $t_b;
			}
		}
	}
	// echo '<pre>';
	// print_r($outputBrands_);
	// echo '</pre>';
	foreach($outputBrands_ as $key=>$b){
	$term = get_term_by( 'id', $b[0], 'brands' );
		if(isset($term->term_taxonomy_id))
		{?>

			<li>

				<?php if (get_post_meta($key, "image", true) != "")
				{ ?>

					<?php $get_url_image  =   get_field('image',$key) ;
					tf_bfi_image_with_brand($get_url_image,140,180,$term->name);
					
				}
				else
				{  ?>

					<a href="<?php echo get_permalink($key); ?>">

						<img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif"/>

						<span><?php echo $term->name; ?></span>

					</a>

				<?php

				}?>

			</li>
		<?php
		}
	}
}





function listRecentBrands() {

    $count_posts = 12; // count posts

    $args = array(
        'showposts' => $count_posts,
        'post_type' => 'post',
        'fields'    => 'ids'
    );

    $firstQuery = new WP_Query($args);

    while ($firstQuery->have_posts()) : $firstQuery->the_post();

        $connected = new WP_Query( array(
            'post_type' => 'post',
            'connected_type'  => 'post_to_persons',
            'connected_items' => get_post( get_the_ID() ),
            'nopaging'        => true
        ) );
        while ($connected->have_posts()) : $connected->the_post();
            global $post;
            $brandsTerms = p2p_get_meta( $post->p2p_id, 'brands', true ) ;
			print_r($brandsTerms);
			if($brandsTerms){
            $terms_ids   = array_values_recursive( $brandsTerms );

            /*********************************************** START DON`T DELETE!!!! ***************************************************************************/
//                echo'$connected';
//                printer($connected->query_vars['connected_items']->ID);
//                echo'<br>';
//
//                echo'$firstQuery->posts[$i]';
//                 printer($firstQuery->posts[$i]);
//                echo'<br>';
//
//
//                echo'$brandsTerms';
//                 printer($brandsTerms);
//                echo'$brandsTerms<br>';
//
//
//                echo'<br>$terms_ids';
//                 printer($terms_ids);
//                echo'$terms_ids<br>';
//
//                echo'$post->p2p_id';
//                 printer($post->p2p_id);
//                echo'$post->p2p_id@<br>';
            if($terms_ids[0] == null)
            {
                if($terms_ids[1] == null)
                {
                    if($terms_ids[2] == null)
                    {
                        if($terms_ids[3] == null)
                        {
                            if($terms_ids[4] == null)
                            {
                                if($terms_ids[5] == null)
                                {
                                    $terms_ids[0] = false;
                                }
                                else
                                {
                                    $terms_ids[0] = $terms_ids[5];
                                }
                            }
                            else
                            {
                                $terms_ids[0] = $terms_ids[4];
                            }
                        }
                        else
                        {
                            $terms_ids[0] = $terms_ids[3];
                        }
                    }
                    else
                    {
                        $terms_ids[0] = $terms_ids[2];
                    }
                }
                else
                {
                    $terms_ids[0] = $terms_ids[1];
                }
            }
            /*********************************************** END  DON`T DELETE!!!! ****************************************************************************/
			
            $term = get_term_by( 'id', $terms_ids[0], 'brands' );


            if(isset($term->term_taxonomy_id))
            {?>

                <li>

                    <?php if (get_post_meta($connected->query_vars['connected_items']->ID, "image", true) != "")
                    { ?>

                        <?php $get_url_image  =   get_field('image',$connected->query_vars['connected_items']->ID) ; 
					
						tf_bfi_image_with_brand($get_url_image,140,180,$term->name);
					
					}
                    else
                    {  ?>

                        <a href="<?php echo get_permalink($connected->query_vars['connected_items']->ID); ?>" title="<?php  echo $connected->query_vars['connected_items']->post_title; ?>">

                            <img src=" <?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php echo $connected->query_vars['connected_items']->post_title; ?>" />

                            <span><?php echo $term->name; ?></span>

                        </a>

                    <?php

                    }?>

                </li>
            <?php
            }
		}
        endwhile;
        wp_reset_postdata();

    endwhile;
    wp_reset_postdata();
}


function listRecentMusics2() {

    $query = custom_music_page_posts_query();

    while ($query->have_posts()) : $query->the_post();

        $connected = custom_music_page_p2p_query( get_the_ID() );

        // var_dump( $connected->have_posts() );

        if ( $connected->have_posts() ){ 

            echo 'connected isset <br/>';

        } else {


            echo 'connected not isset <br />';
            $term_list = wp_get_post_terms( get_the_ID(), 'music', array("fields" => "names") );
            var_dump( $term_list );
            echo '<br/>';
        }

    endwhile;

    wp_reset_postdata();
}
/*
 * What we do with this old and new functionality:
 * we`ve checked if count of new posts is less 
 * then we`ve need to display - we`re using old functionality.
 * if we have all music page with newest functionality - we`ve print all from new functionality.
 * this will save time for understanding how to create a custom sql query.
 * Work on page-music.php
 **/
function listRecentMusics() {

    global $wpdb;
    $music_count = 9 * 4;	

    $querystr = "SELECT term_taxonomy_id, object_id FROM (SELECT object_id, term_taxonomy_id, count(*) as count FROM wp_term_relationships JOIN wp_posts ON object_id = ID WHERE term_taxonomy_id IN (SELECT term_taxonomy_id as id FROM wp_term_taxonomy WHERE taxonomy = 'music') AND (post_type = 'post' OR post_type = 'persons') AND post_status = 'publish' GROUP BY object_id ORDER BY ID DESC) as tmp GROUP BY term_taxonomy_id ORDER BY object_id DESC LIMIT ".($music_count*2);

    $recent_musics = $wpdb->get_results($querystr, OBJECT);


    if($recent_musics) {
		
        $i=0;
		$music_show_arr = array();
        
        foreach($recent_musics as $music) {

            $post = get_post($music->object_id);

            if ( $post->post_type == 'persons' && isset($post->p2p_id) || $post->post_type == 'post' ) {
				if( isset($music_show_arr)&&(count($music_show_arr)<$music_count)&&($i<$music_count) ){
						$music_show_arr[$i] = $music;
						$i++;
				}
            }            
        }

		foreach($music_show_arr as $m_arr){
			showMusicPostImage($m_arr->object_id, $m_arr->term_taxonomy_id); 
		}
    }    
}

function showMusicPostImage($post_id, $music_id) {
    global $wpdb;
    
    if ( ! $music = wp_cache_get($music_id, 'music_thumb') ) {
            $music = $wpdb->get_var( $wpdb->prepare( "SELECT t.name FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = 'music' AND tt.term_taxonomy_id = %s LIMIT 1", $music_id) );
            wp_cache_add($music_id, $music, 'music_thumb');
        }        

    $post = get_post($post_id);

    if ( $post->post_type == 'persons' ) {
        $permalink = get_permalink($post->p2p_from);
        $get_url_image = get_url_image( $post->p2p_from );
    } else {
        $permalink = get_permalink($post->ID);
        $get_url_image = get_url_image( $post->ID );
    }        

?>
    <li>
        <?php if ( $get_url_image != '' ) { 

           tf_bfi_image_with_brand($get_url_image,140,180,$term->name);

       }else{ ?>

            <a href="<?php echo $permalink; ?>" title="<?php echo $post->post_title; ?>">
                <img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php echo $post->post_title; ?>" />
                <span><?php echo $music;?></span>
            </a>
    
        <?php } ?>
       
    </li>
<?php
}



function custom_music_page_posts_get_posts(){
    // $count_posts = 15 * 4;

    $count_posts = 9 * 4;

    $args = array(
                    'showposts' => $count_posts,
                    'post_type' => 'post',
                    'fields'    => 'ids',
                );

    $posts = get_posts( $args );
    return $posts;    
}


function custom_music_page_posts_query(){

    $count_posts = 15 * 4;

    // $count_posts = 9 * 4;    

    $args = array(
                    'showposts' => $count_posts,
                    'post_type' => 'post',
                    'fields'    => 'ids',
                );

    $query = new WP_Query($args);

    // var_dump( $query->request );

    return $query;

}


function custom_music_page_p2p_query( $id ){

    $connected = new WP_Query( array(
                                        'connected_type'   => 'post_to_persons',
                                        'connected_items'  => get_post( $id ),
                                        'nopaging'         => true,
                                        'suppress_filters' => false,
                                    ) );

    // var_dump( $query->request );

    return $connected;
}



function printer($value)
{
    echo"<pre>";
    print_r($value);
    echo"<br>";
    echo"</pre>";
}
function array_values_recursive( $ary )
{
    $lst = array();
    foreach( array_keys($ary) as $k ){
        $v = $ary[$k];
        if (is_scalar($v)) {
            $lst[] = $v;
        } elseif (is_array($v)) {
            $lst = array_merge( $lst,
                array_values_recursive($v)
            );
        }
    }
    return $lst;
}

register_sidebar(array(
    'name'          => 'Mail Chimp',
    'id'            => 'mail_chimp_tokyo',
    'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
    'after_widget'  => '</div></section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
));

add_action('acf/register_fields', 'my_register_fields');

function my_register_fields()
{
    include_once('fields/contributor_sort-v4.php');
}


require_once( 'BFI_Thumb.php' );

function get_url_image($id){
    $get_url_image  =   get_field('image',$id) ; 

    if ( empty( $get_url_image ) && has_post_thumbnail( $id ) ){
     $get_url_image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'thumbnail' ) ;
     $get_url_image = $get_url_image[0];
    }   
    return $get_url_image;
}





function tf_bfi_image($src, $w, $h, $class = '', $title  = '',$link =''){

    
    $params = array( 'width' => $w, 'height' => $h );

    if( empty($class) ){
	   $class = 'snippet-thumb';
    }
     
    if( empty($title) ){
	   $title = get_the_title();
    }
    
    if( empty($link) ){
	    $link = get_the_permalink();
    }
    
    $image  = bfi_thumb( $src, $params );
    $parsed_url = parse_url( $image);
	if( $parsed_url['host'] == $_SERVER['HTTP_HOST'] ){
	    $html = '<a href="' . $link . '" title="' . $title . '">';
	    $html .= '<img src="' . $image . '" alt="' . $title . '" class="' . $class  . '" />';
	    $html .= '</a>';
	   echo $html;
	}
	else { 
		$html = '<a href="' . $link . '" title="' . $title . '">';
		$html .= '<img src="'.get_template_directory_uri().'/thumb.php?src='.$image.'&amp;w=140&amp;h=180&amp;zc=1&amp;q=95 " alt="' . $title . '" class="' . $class  . '" />';
		$html .= '</a>';
		echo $html;
		
		};
  
}
function tf_bfi_image_with_brand($src, $w, $h, $brand ='', $class = '', $title  = '',$link ='' ){

    
    $params = array( 'width' => $w, 'height' => $h );

    if( empty($class) ){
	   $class = 'snippet-thumb';
    }
     
    if( empty($title) ){
	   $title = get_the_title();
    }
    
    if( empty($link) ){
	    $link = get_the_permalink();
    }
    
    $image  = bfi_thumb( $src, $params );
     $parsed_url = parse_url( $image);
	if( $parsed_url['host'] == $_SERVER['HTTP_HOST'] ){
	    $html = '<a href="' . $link . '" title="' . $title . '">';
	    $html .= '<img src="' . $image . '" alt="' . $title . '" class="' . $class  . '" />';
	    if($brand){
		$html .= '<span>'.$brand.'</span>';
	    }
	    $html .= '</a>';
	   echo $html;
	}
	else { 
		$html = '<a href="' . $link . '" title="' . $title . '">';
		$html .= '<img src="'.get_template_directory_uri().'/thumb.php?src='.$image.'&amp;w=140&amp;h=180&amp;zc=1&amp;q=95 " alt="' . $title . '" class="' . $class  . '" />';
		 if($brand){
		$html .= '<span>'.$brand.'</span>';
	    }
		$html .= '</a>';
		echo $html;
		
		};
}
