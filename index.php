<?php get_header();

	//remove later
	global $wp_query;
	// var_dump( $wp_query );


?>

<?php
//file functions custom-queries.php!!!
if (file_exists(TEMPLATEPATH . '/custom-queries.php')) {
	require_once(TEMPLATEPATH . '/custom-queries.php');
?>

<div id="content">
	<?php $template = 'normal'; // To add template for archives ?>
	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
	<?php /* If this is a category archive */ if (is_category()) { ?>
	<h2 class="page-subtitle-cat-tf"><?php single_cat_title(); ?></h2>
	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
	<h3 class="contentpaneltitle">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h3>
	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
	<h3 class="page-subtitle">Archive for <?php the_time('F jS, Y'); ?></h3>
	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<h3 class="page-subtitle">Archive for <?php the_time('F, Y'); ?></h3>
	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<h3 class="page-subtitle">Archive for <?php the_time('Y'); ?></h3>
	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
	<h3 class="page-subtitle">Author Archive</h3>
	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
	<h3 class="page-subtitle">Blog Archives</h3>
	<?php /* Added for Brands & Music Page */ } else { ?>
	<?php
	global $wp_query;
	if($wp_query->query_vars['taxonomy'] == 'brands') {
		brand_profile_snap();
		//$template = 'overlay';
	?>
		<h3 class="contentpaneltitle">Articles containing the brand: <?php echo $wp_query->queried_object->name; ?></h3>
	<?php
		// Remove posts from "Brand Profile"
		global $query_string;
		query_posts($query_string . "&cat=-1170");
	} elseif($wp_query->query_vars['taxonomy'] == 'music') {?>
		<h3 class="contentpaneltitle">Articles that sound like: <?php echo $wp_query->queried_object->name; ?></h3>
	<?php } ?>
	<?php } ?>

		<?php
		// Snippet to hide "Tokyo Street Snaps" category articles from Articles page
		if(is_home()) {
			query_posts($query_string . '&cat=-697');
			echo '<h1 class="post-title">Most Recent Tokyo Fashion Articles</h1><p id="aplink">(Please check this link for our <a href="/photos/" title="Tokyo Street Fashion Pictures">Tokyo Street Fashion Pictures</a>)</p>';
		}

		switch($template) {
			case 'overlay':
				loop_photo_overlay();
				break;
			case 'normal':
			default:
				loop_normal();
				break;
		}

		?>

		<div class="navigation">
			<div class="navleft"><?php next_posts_link('&laquo; Older Posts', '0') ?></div>
			<div class="navright"><?php previous_posts_link('Newer Posts &raquo;', '0') ?></div>
		</div>

	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>


