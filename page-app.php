<?php
/*
Template Name: APP
*/
?>
<?php get_header() ?>

	<div id="content">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<div class="post-content">
				<!-- <div id="mixi_button">
					<a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="<?=MIXI_KEY?>">Check</a>
					<script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
				</div>
                <div id="facebook_button">
                    <?php //if (function_exists('fbshare_manual')) echo fbshare_manual(); ?>
                </div>
				<div id="ftwitter_button">
					<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="tokyofashion" data-related="tokyofashion">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
				</div> -->
				<?php get_template_part( 'social-share', '' ); ?>
				<?php the_content() ?>
			</div>
		</div><!-- .post -->
		<?php comments_template(); ?>

	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>