<?php
 include get_template_directory().'/Flickr.php';


/*
Template Name: Photo Page
*/
 
 
 
$photos_per_location = 12;
$FLICKR_DATA_URL = 'http://api.flickr.com/services/feeds/photoset.gne?set=72157616624742453&nsid=31167840@N08&lang=en-us&format=rss_200';				

?>

<?php
//file functions custom-queries.php!!!
if (file_exists(TEMPLATEPATH . '/custom-queries.php')) {
	require_once(TEMPLATEPATH . '/custom-queries.php');
?>

<?php get_header() ?>
<style type="text/css">
	.bm-half { margin-bottom:25px !important; }
	.location-photos { clear:both; width:700px; }
	.location-photos li { margin:0 20px 0 0; width:140px; float:left; list-style-type:none; }
	.location-photos img { margin-bottom:10px; }
	.bm-half .contentpaneltitle { text-transform:uppercase; margin-bottom:15px; }
	#photos-page-title { font-size:140%; margin:20px 0 10px; padding:10px 0 5px; }
	.backlink { color:#000000; font-size:110%; font-weight:bold; margin:4px 0 0; }
        #photonav { clear:both; width:620px; }
	#ltnav, #rtnav { overflow:hidden; border:1px solid #888; padding:3px 5px; background:#D9D5BB; font-weight:bold; }
	#ltnav { float:left; }
	#rtnav { float:right; }
</style>

<div id="content">
<?php
// Check if request is to show particular location or all locations.
$location = $_GET['location'];
if($location) {
	listPhotosSingle($location);
} else {
	the_post() ?>
	<div id="post-<?php the_ID(); ?>" class="post">
		<h1 class="post-title"><?php the_title(); ?></h1>
		<div class="post-content">
			<?php get_template_part( 'social-share', '' ); ?>
			<?php the_content() ?>
		</div>
	<?php listPhotosAll(); ?>
	</div><!-- .post -->
<?php
}
?>
</div><!-- #content -->
<?php get_sidebar() ?>
<?php get_footer() ?>






