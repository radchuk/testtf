<?php $site_url =  get_site_url();?>

</div><!-- #container -->

</div>
<div id="footer">
	<div class="wrapper">
		<?php if (has_nav_menu('footer-menu')) {  
		    wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => false, 'menu_id' => 'footerlinks'));
		} ?>
<!--		<p>Copyright 1998-2013 TokyoFashion.com. All rights reserved. <a href="http://tokyofashion.com/privacy-policy/">Privacy Policy</a></p>-->
		<p>Copyright 1998-2013 TokyoFashion.com. All rights reserved. <a href="<?php echo $site_url?>/privacy-policy/">Privacy Policy</a></p>
	</div>
</div><!-- #footer -->

<?php wp_footer() ?>

<?php

global $wpdb;

// Get the total page generation time
$totaltime = timer_stop( false, 22 );

// Calculate the time spent on MySQL queries by adding up the time spent on each query
$mysqltime = 0;
foreach ( $wpdb->queries as $query )
	$mysqltime = $mysqltime + $query[1];

// The time spent on PHP is the remainder
$phptime = $totaltime - $mysqltime;

// Create the percentages
$mysqlper = number_format_i18n( $mysqltime / $totaltime * 100, 2 );
$phpper   = number_format_i18n( $phptime / $totaltime * 100, 2 );

// Output the stats
echo "<!-- ";
echo 'Page generated in ' . number_format_i18n( $totaltime, 5 ) . " seconds ( {$phpper}% PHP, {$mysqlper}% MySQL )";
echo " mem:".memory_get_usage();
//if ( function_exists('get_useronline')) get_useronline();
echo " -->";
?>
<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>

<script>
    var _gaq = _gaq || [];

    _gaq.push(['_setAccount', 'UA-43664638-1']);
    _gaq.push(['_trackPageview']);
        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    _gaq.push(['_trackPageview']);

    (function($) {

        $('body').delegate("#custom_pinit_button", "mouseup", function()
        {
            _gaq.push(['_trackEvent', 'Pinit-button custom', 'Click custom', 'Extra Label if you like! custom']);

            console.log( 'U are cliked on CUSTOM pin it btn' );

        });

        $('body').delegate(".pinit-button", "mouseup", function()
        {

            _gaq.push(['_trackEvent', 'Pinit-button', 'Click', 'Extra Label if you like!']);

            console.log( 'U are cliked on pin it btn' );
        });

    })( jQuery );

</script>

<!-- <div id="nav" style="display: none">
    <li>
        <a href="#" title="">
            <div class="a2-edit-icon icon"></div>
        </a>
        <ul>
            <li class="social_list"><a href="#">The product</a></li>
            <li class="social_list"><a href="#">Meet the team</a></li>
        </ul>

    </li>
</div> -->

<div id="nav" style="display: none"> 
    <div class="dropdown">
        <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
        <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li>                    
                <div id="custom-tweet-button">
                  <a href="#" data-text="hello" target="_blank">Tweet</a>
                </div>
            </li>
            <li>
                <div id="custom-fb-button">
                    <a href="#" target="_blank">Facebook</a>
                </div>
            </li>
        </ul>
    </div>
</div>

</body>
</html>