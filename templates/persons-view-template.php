<?php get_header(); ?>

<?php global $wp_query;?>

<?php
//echo '$wp_query->query_vars<pre>';
//print_r($wp_query->query_vars);
//echo '<pre><br><br><br><br>';
?>

	<div id="content">

	<?php if (have_posts()) : ?>

		<?php

		$field_name = $wp_query->query_vars['field_name' ];

		$field_value = $wp_query->query_vars['field_value'];

		$contributor_ids = get_posts( array(
                                            'fields'        => 'ids',
                                            'post_type'     => 'contributors',
                                            'meta_query'    => array(
                                                                        array(
                                                                                'key'     => 'contributor_shortname',
                                                                                'value'   => $field_value,
                                                                                'compare' => '=',
                                                                                'type'    => 'CHAR',
                                                                               )
                                                                     ),
		                                    )
                                     );

		$contributor_id = $contributor_ids[0];

		$profile = get_field('profile', $contributor_id);

		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $contributor_id ), 'single-post-thumbnail' );

		if( $field_name === 'photographer' || $field_name === 'hunter' || $field_name === 'author' ) { ?>

			<div class="contributors">

				<h1 class="post-title"><?php echo urldecode(ucfirst($field_value)); ?></h1>

				<div class="post-content">

					<p>

						<?php if( !empty( $image ) ) { ?>

							<img src="<?php echo $image[0]; ?>" class="alignright size-full wp-image-1362">

						<?php } ?>

						<?php if( !empty( $profile ) )
                        {
                            echo $profile;

						} ?>

					</p>

				</div>

				<div style="clear: both;"></div>

			</div>

		<?php } ?>

		<h3 class="contentpaneltitle"> Articles containing the <?php echo  $field_name ." : ".  urldecode(ucfirst($field_value)); ?></h3>

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="snippet">

			<div class= "snippet-left">

                <?php get_template_part( '/partials/tokyo', 'image_view_template' ); ?>

            </div>

			<div class="snippet-right">

				<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

				<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?> in <?php the_category(', '); ?></p>

				<p class="snippet-intro"><?php the_excerpt(); ?></p>

				<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>

			</div>
		</div><!-- .snippet -->

		<?php endwhile; ?>

		<div class="navigation">

			<div class="navleft"><?php next_posts_link('&laquo; Older Posts', '0') ?></div>

			<div class="navright"><?php previous_posts_link('Newer Posts &raquo;', '0') ?></div>

		</div>

	<?php else : ?>

		<div id="post-0" class="post">

			<h2 class="post-title">Nothing Found</h2>

			<div class="post-content">

				<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>

			</div>

			<form id="searchform" method="get" action="<?php bloginfo('home') ?>">

				<div>

					<input id="s" name="s" type="text" value="<?php echo wp_specialchars(stripslashes($_GET['s']), true) ?>" size="40" />

					<input id="searchsubmit" name="searchsubmit" type="submit" value="Find" />

				</div>

			</form>

		</div><!-- .post -->

	<?php endif; ?>

	</div><!-- #content -->

<?php get_sidebar() ?>

<?php get_footer() ?>