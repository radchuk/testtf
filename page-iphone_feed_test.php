<?php

/******************************************************
	Template Name: iPhone feed
	allowed model set 
		iphone3, iphone4 , iphone5, android, ipad
******************************************************/


	ini_set("memory_limit", "100M"); 
	include (TEMPLATEPATH . '/iphone_include_header.php'); 
	
	//set model
	$model_allow = array('iphone3', 'iphone4', 'iphone5', 'android', 'ipad');
	$model = 'iphone4'; //iphone4 as default
	if(isset($_GET["model"]))
	{
		if(in_array($_GET["model"], $model_allow))
		{
			$model = $_GET["model"];
		}
	}
	
	//set retina
	$retina = false;
	if(isset($_GET['ipad-retina']) && $_GET['ipad-retina'] == '1')
	{
		$retina = true;
	}
	
	if (isset($_GET["articleType"])) 
	{
		$str_temp=$_GET["articleType"];	
		if (isset($_GET["perPage"]) && ($_GET["perPage"]!="")) {$perPage=(int) $_GET["perPage"];} else {$perPage=30;}
		if (isset($_GET["page"]) && ($_GET["page"]!="")) {$page=(int) $_GET["page"];} else {$page=1;}
		if (isset($_GET["id"]) && ($_GET["id"]!="")) {$id=(int) $_GET["id"];} else {$id=0;}
		
		if ($str_temp == 'FeaturedArticle')
		{
			$str = iphone_news_ipad(1,$model,$perPage,$page, $retina);
			echo ($str);
		}
		if ($str_temp == 'TokyoFashionNews')
		{
			$str = iphone_news_ipad(2,$model,$perPage,$page, $retina);
			echo ($str);
		}
		if ($str_temp == 'OtherStuff')
		{
			$str = iphone_news_ipad(6,$model,$perPage,$page, $retina);
			echo ($str);
		}
		if ($str_temp == 'StreetSnaps')
		{
			$str = iphone_StreetSnaps_ipad($model,$perPage,$page,$id, $retina);
			echo ($str);
			
		}
	}

	if (isset($_GET["query"])) 
	{
		$str_temp=$_GET["query"];
		if ($str_temp == 'popularBrands')
		{		
			$str = iphone_popularBrands();
			echo ($str);
		}
	}

	if ((isset($_GET["brands"])) AND (empty($_GET["debug"]))) {
		$brand_id=intval($_GET["brands"]);
		$str = iphone_Brands_ipad($brand_id, $model, $retina);
		echo ($str);
	}

	if ((isset($_GET["id_post"])) AND (empty($_GET["debug"]))) {
		$post_id=intval($_GET["id_post"]);
		$str = iphone_post_id($post_id,$model);
		echo ($str);
	}

	/* SEARCH */
	if (isset($_GET["search"]) AND empty($_GET["debug"])) {
		$sfs = $_GET["search"];
		if (isset($_GET["page"])){$paged=intval($_GET["page"]);} else {$paged=1;}
		if (isset($_GET["perPage"])){$perPage=intval($_GET["perPage"]);} else {$perPage=10;}

		$str = search_for_sait_ipad(
					$sfs,
					$paged,
					$perPage,
					$model,
					@$_GET['format']=='format2'?'format2':'' ,
					$retina
				);
		echo ($str);
	}
	/* END SEARCH */
	
	/* COMMENTS FOR POST */
	if ((isset($_GET["comments_post_id"])) AND (empty($_GET["debug"]))) {
		$id_post_comments_id=intval($_GET["comments_post_id"]);		
		$str = iphone_comments($id_post_comments_id);
		echo ($str);
	}
	/* END COMMENT */
	/* ADD COMMENT */
	if ((isset($_POST['comment_post_ID'])) AND (empty($_GET["debug"]))) {
		$str = add_comment($_POST);
		echo ($str);
	}
	/* END ADD COMMENT */
	
include (TEMPLATEPATH . '/iphone_include_footer.php'); 
