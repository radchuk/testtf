	</div><!-- #container -->

</div>

    <div class="stas">
        <a class="stas_btn" href="#">clik me</a>
    </div>

<div id="footer">
	<div class="wrapper">
		<?php if (has_nav_menu('footer-menu')) {  
		    wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => false, 'menu_id' => 'footerlinks'));
		} ?>
		<p>Copyright 1998-2013 TokyoFashion.com. All rights reserved. <a href="http://tokyofashion.com/privacy-policy/">Privacy Policy</a></p>
	</div>
</div><!-- #footer -->

<?php wp_footer() ?>

<?php

global $wpdb;

// Get the total page generation time
$totaltime = timer_stop( false, 22 );

// Calculate the time spent on MySQL queries by adding up the time spent on each query
$mysqltime = 0;
foreach ( $wpdb->queries as $query )
	$mysqltime = $mysqltime + $query[1];

// The time spent on PHP is the remainder
$phptime = $totaltime - $mysqltime;

// Create the percentages
$mysqlper = number_format_i18n( $mysqltime / $totaltime * 100, 2 );
$phpper   = number_format_i18n( $phptime / $totaltime * 100, 2 );

// Output the stats
echo "<!-- ";
echo 'Page generated in ' . number_format_i18n( $totaltime, 5 ) . " seconds ( {$phpper}% PHP, {$mysqlper}% MySQL )";
echo " mem:".memory_get_usage();
//if ( function_exists('get_useronline')) get_useronline();
echo " -->";
?>

<!-- Google +1 button tag. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<!-- Google Analytics. -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>



<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12581430-1");
if(typeof(is404) != "undefined")
{
	pageTracker._trackPageview("/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);
}
pageTracker._trackPageview();
} catch(err) {}</script>

<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">

var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-41905040-1']);
  _gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
	jQuery('.pinit-button').click(function(){

		var title = $(this).closest('a').attr('title');
        _gaq.push(['_trackEvent', 'PinIt ', title]);
    });
</script>
</body>
</html>
