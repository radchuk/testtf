<?php
/*
Template Name: Front Page
*/
?>

<?php get_header() ?>

<?php //include (TEMPLATEPATH . '/config.php'); ?>

	<div id="content">
	
		<div class="contentpanel">
			<a href="http://tokyofashion.com/category/tokyo-fashion-news/" class="morelink">Browse All News &raquo;</a>
			<h2 class="contentpaneltitle">Tokyo Fashion News</h2>
			<div id="last">
				<?php
					$ids = array();
					$latestNewsPosts = new WP_Query();
					$latestNewsPosts->query('showposts=1&cat=216');				
				?>
				<?php if ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>



                    <?php    /*******************************************************************************************************************/ ?>
                    <?php get_template_part( '/partials/tokyo', 'image_big2' ); ?>
                    <?php    /*******************************************************************************************************************/ ?>


                    <h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				<p class="snippet-intro"><?php echo truncate($post->post_excerpt, 200); ?></p>
				<?php $ids[]= $post->ID; endif; ?>	
			</div>
			
			<div id="latest">
				<ul id="latest-snippets">
					<?php
						$latestNewsPosts = new WP_Query();
						$latestNewsPosts->query('showposts=2&cat=216&offset=1');					
					?>
					<?php while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>
					<li>
						<?php if (get_post_meta($post->ID, "image", $single = true) != "") { ?>					
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/thumb.php?src=<?php echo get_post_meta($post->ID, "image", $single = true); ?>&amp;w=140&amp;h=180&amp;zc=1&amp;q=95" alt="<?php the_title(); ?>" class="snippet-thumb" /></a>						
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php the_title(); ?>" class="snippet-thumb" /></a>
						<?php } ?>
						<h4 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
						<p class="snippet-intro"><?php echo truncate($post->post_excerpt, 100); ?></p>
					</li><!-- .post -->
					<?php $ids[]= $post->ID; endwhile ?>
				</ul>
				
				<h3 class="blocksubtitle">More Tokyo Fashion News</h3>
				<ul id="latest-list">
					<?php
						$latestNewsPosts = new WP_Query();
						$latestNewsPosts->query('showposts=3&cat=216&offset=3');					
					?>
					<?php while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>
					<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
					<?php $ids[]= $post->ID; endwhile ?>
				</ul>
				
			</div>
		</div>		

		<?php include (TEMPLATEPATH . '/carousel_features.php'); ?>
		
		<div class="contentpanel">
			<h2 class="contentpaneltitle">Latest Tokyo Fashion Articles</h2>
			<?php
				$latestNewsPosts = new WP_Query();
				$latestNewsPosts->query('showposts=5&cat=819,1,822,1170');					
			?>
			<?php
				while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post();
				if (!in_array($post->ID, $ids)) {
			?>
			<div class="snippet">
				<div class="snippet-left">
					<?php if (get_post_meta($post->ID, "image", $single = true) != "") { ?>					
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/thumb.php?src=<?php echo get_post_meta($post->ID, "image", $single = true); ?>&amp;w=140&amp;h=180&amp;zc=1&amp;q=95" alt="<?php the_title(); ?>" class="snippet-thumb" /></a>						
					<?php } else { ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php the_title(); ?>" class="snippet-thumb" /></a>
					<?php } ?>
				</div>
				<div class="snippet-right">
					<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
					<p class="snippet-meta">Posted in <?php the_category(', '); ?></p>
					<p class="snippet-intro"><?php the_excerpt(); ?></p>
					<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>
				</div>
			</div><!-- .snippet -->
			<?php } endwhile ?>
		</div>

	</div><!-- #content -->

	<div id="sidebar">		

		<div class="sidepanel">
			<h3>About TokyoFashion.com</h3>
			<p><strong>TokyoFashion.com</strong> is an online magazine devoted to clothing, accessories, and style in Tokyo, Japan.</p>
	
			<p>Sounds simple, eh? But it's not as simple as it once was - Tokyo Fashion these days is a large and fast moving organism. There are new brands and new trends starting and old ones dying every day in Japan's capital of style.</p>
	
			<p>Our goal is to keep you up to date with what's going on in Japanese fashion, day to day, hour to hour.</p>
		</div>
		
		<div class="sidepanel isacc">
			<h3>Shopping in Tokyo</h3>
			<div class="accordion">
				<a href="javascript:void(0);" class="acc-head">Fashion Brands</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=415'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Clothing Stores</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=823'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Department Stores</a>			
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=824'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Tokyo Fashion Maps</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=825'); ?>
					</ul>
				</div>
			</div>
		</div>	
	
		<div class="sidepanel">
			<h3>Our Sponsors</h3>
<br />
<!-- Start AdSense Code -->
<script type="text/javascript"><!--
google_ad_client = "pub-3685786506219492";
/* TF 300x250, created 6/10/09 */
google_ad_slot = "5102142911";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			<!-- End AdSense Code -->
		</div>
		
		<div class="sidepanel">
			<h3>Tokyo Fashion Photos</h3>
			<?php get_flickrRSS(); ?>
		</div>
		
		<div class="sidepanel">
			<h3>More Tokyo Fashion</h3>
			<div class="accordion">
				<a href="javascript:void(0);" class="acc-head">Fashion Magazines</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=827'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Japan Forums</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=826'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Fashion Trends</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=276'); ?>
					</ul>
				</div>
			</div>
		</div>
	
	</div><!-- #sidebar -->
	
<?php get_footer() ?>