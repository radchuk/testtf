<?php
// Get the ID of a given category
$category_id = get_cat_ID('Tokyo Street Snaps');

// Get the URL of this category
$category_link = get_category_link($category_id);
?>

<div class="contentpanel">
    <!--	<a href="http://tokyofashion.com/category/tokyo-street-snaps/" class="morelink">Browse All Street Snaps &raquo;</a>
    -->
    <a href="<?php echo esc_url($category_link); ?>" class="morelink">Browse All Street Snaps &raquo;</a>

    <h2 class="contentpaneltitle">Tokyo Street Snaps</h2>

    <div id="mycarousel-nav">

        <a href="#" id="mycarousel-prev">&laquo; Previous</a>

        <a href="#" id="mycarousel-next">Next &raquo;</a>

    </div>

    <ul id="mycarousel" class="jcarousel-skin">

        <?php

        $featPosts = new WP_Query();

        $featPosts->query('showposts=8&cat=697');

        ?>
        <?php while ($featPosts->have_posts()) : $featPosts->the_post() ?>

            <li>

                <?php get_template_part('/partials/tokyo', 'image_small2'); ?>

                <h4 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

                <p class="snippet-intro"><?php echo truncate($post->post_excerpt, 100); ?></p>

            </li>

            <?php $ids[] = $post->ID; endwhile; ?>
    </ul>

</div>