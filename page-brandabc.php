<?php
/*
Template Name: Brands in Alphabet
*/
?>
<?php get_header() ?>

<?php global $post; ?>

    <div id="content">

        <h2 class="contentpaneltitle">A-Z BRANDS</h2>

        <?php
        $records = get_terms('brands');

        $lastChar = '';

        $arrr = array();

        foreach ($records as $val => $key2) {
            $arr = array('name' => $key2->name, 'termId' => $key2->term_id);

            $char = mb_substr($arr['name'], 0, 1, "UTF-8");

            $arrr[$char][] = array('name' => $arr['name'], 'termId' => $key2->term_id);
        }


        ?>

        <div class="alphabet" data-spy="scroll" data-target="#navbar-example">
            <?php
            foreach ($arrr as $key => $letter) {

                ?>

                <ul>

                    <li>

                        <a class="scroll" href="#<?php echo $key; ?>"
                           style="float: left;padding: 5px;"><?php echo $key; ?></a>

                    </li>

                </ul>

            <?php
            }
            ?>

        </div>

        <?php     foreach ($arrr as $key => $letter) {

            ?>

            <div class="letter-row">

                <hr>

                <div class="letter" id="<?php echo $key; ?>"><?php echo $key; ?></div>

                <?php $counter = 0; ?>

                <?php foreach ($letter as $keys => $brand) {

                    $term_link = get_term_link((int)$brand['termId'], 'brands');
                    ?>

                    <?php if ($counter == 0) { ?>

                        <div class="brandblock">

                    <?php } ?>

                    <ul id="brandlist">

                        <li><a href="<?php echo $term_link; ?>"><?php echo $brand['name'] ?></a></li>

                    </ul>

                    <?php if ($counter == 24 || empty($letter[$keys + 1])) { ?>

                        <?php $counter = 0; ?>

                        </div>

                    <?php } else { ?>

                        <?php $counter++; ?>

                    <?php } ?>

                <?php } ?>

            </div>
        <?php
        }

        ?>
    </div>

<?php get_sidebar() ?>



    <script>

        jQuery(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                //calculate destination place
                var dest = 0;
                if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
                    dest = $(document).height() - $(window).height();
                } else {
                    dest = $(this.hash).offset().top;
                }
                //go to destination
                $('html,body').animate({scrollTop: dest}, 1000, 'swing');
            });
        });
    </script>

<?php get_footer() ?>