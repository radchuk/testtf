<?php get_header();
	
		// logConsole('a', '123');
?>

	<div id="content">

	<?php if (have_posts()) : ?>

		<h3 class="page-subtitle">Search results for: <?php echo esc_html(stripslashes($_GET['s']), true); ?></h3>

		<?php while ( have_posts() ) : the_post(); ?>
		<div class="snippet">
			<div class="snippet-left">


                <?php    /*******************************************************************************************************************/ ?>
                <?php get_template_part( '/partials/tokyo', 'image_search' ); ?>
                <?php    /*******************************************************************************************************************/ ?>



            </div>
			<div class="snippet-right">
				<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?> in <?php the_category(', '); ?></p>
				<p class="snippet-intro"><?php the_excerpt(); ?></p>
				<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>
			</div>
		</div><!-- .snippet -->
		<?php endwhile; ?>

		<div class="navigation">
			<div class="navleft"><?php next_posts_link('&laquo; Older Posts', '0') ?></div>
			<div class="navright"><?php previous_posts_link('Newer Posts &raquo;', '0') ?></div>
		</div>

	<?php else : ?>

		<div id="post-0" class="post">
			<h2 class="post-title">Nothing Found</h2>
			<div class="post-content">
				<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
			</div>
			<form id="searchform" method="get" action="<?php bloginfo('home') ?>">
				<div>
					<input id="s" name="s" type="text" value="<?php echo wp_specialchars(stripslashes($_GET['s']), true) ?>" size="40" />
					<input id="searchsubmit" name="searchsubmit" type="submit" value="Find" />
				</div>
			</form>
		</div><!-- .post -->

	<?php endif; ?>

	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>