<?php
/*
Template Name: Music
*/
?>
<?php get_header() ?>

    <div id="content">

        <?php the_post() ?>

        <div id="post-<?php the_ID(); ?>" class="post">

            <h1 class="post-title"><?php the_title(); ?></h1>

            <div class="post-content">

				<?php get_template_part( 'social-share', '' ); ?>
                <?php the_content() ?>

            </div>
            <br/>

            <h2 class="contentpaneltitle">Popular Music on Tokyo Fashion</h2>

            <div id="music-tag-cloud">

                <?php wp_tag_cloud_custom( array( 'taxonomy' => 'music', 'number' => 45 ) ); ?> 

            </div>
            <br/><br/>

            <h2 class="contentpaneltitle">Music Recently Heard on the Streets of Tokyo...</h2>

            <ul class="music-photos">

                <?php listRecentMusics(); ?>

            </ul>            

        </div><!-- .post -->
        
    </div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>
