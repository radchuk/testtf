<?php
/*
Template Name: Contributors
*/

get_header();
$args = array(
			'post_type'  => array('contributors'),
			'posts_per_page' => 20
	);

$custom_query = new WP_Query($args); ?>


<div id="content">
	<?php the_post() ?>
	<div id="post-<?php the_ID(); ?>" class="post">
		<h1 class="post-title">Tokyo Fashion Contributors</h1>
		<div class="post-content">
					<!-- <div id="mixi_button">
						<a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="<?=MIXI_KEY?>">Check</a>
						<script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
					</div>
	                <div id="facebook_button">
	                    <?php //if (function_exists('fbshare_manual')) echo fbshare_manual(); ?>
	                </div>
					<div id="ftwitter_button">
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="tokyofashion" data-related="tokyofashion">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
					</div> -->
			<?php get_template_part( 'social-share', '' ); ?>
			<?php the_content() ?>
		</div>

		<br/>
		<?php if( $custom_query->have_posts() ) { ?>
			<h2 class="contentpaneltitle">TokyoFashion.com Roll Call...</h2>
			<ul id="contributors-full">
				<?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
					<li>
						<img src="<?php contributor_imagesrc( get_the_ID() ); ?>" alt="" class="c-left" width="75" height="75" />
						<h3 class="snippet-title"><a href="<?php echo the_permalink(); ?>" title="<?php echo the_title(); ?>"><?php echo the_title();?></a></h3>
						<p class="snippet-intro"><?php contributor_profile( get_the_ID() ); ?></p><br/>
						<p class="photometa">
							<?php contributor_bloodtype( get_the_ID() ); ?>
							<?php contributor_link( get_the_ID() ); ?>
						</p>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php }
		wp_reset_query(); ?>

	</div><!-- .post -->

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>