<?php
/*
Template Name: Brands
*/
?>
<?php get_header() ?>

	<div id="content">

		<?php the_post() ?>

		<div id="post-<?php the_ID(); ?>" class="post">

			<h1 class="post-title"><?php the_title(); ?></h1>

			<div class="post-content">

				<?php the_content() ?>

			</div>

			<h2 class="contentpaneltitle">Fashion Brands on TokyoFashion.com...</h2>

            <div><?php wp_tag_cloud_custom( array( 'taxonomy' => 'brands', 'number' => 45 ) ); ?></div><br/>

			<h2 class="contentpaneltitle">Brands Recently Seen on the Streets of Tokyo...</h2>


            <ul class="overlay-photos"><?php listRecentBrandsCustom(); ?></ul>

		</div><!-- .post -->

	</div><!-- #content -->

<?php get_sidebar() ?>

<?php get_footer() ?>