<?php get_header(); 
	$connectedSnaps = new WP_Query( array(
			'post_type'            => 'post',
			'connected_type'       => 'post_to_persons',
			'connected_items'      => get_the_ID(),
			'connected_direction'  => 'to'

		) );
?>

	<style type="text/css">
		.bm-half { margin-bottom:25px !important; }
		.location-photos { clear:both; width:700px; }
		.location-photos li { margin:0 20px 0 0; width:140px; float:left; list-style-type:none; }
		.location-photos img { margin-bottom:10px; }
		.bm-half .contentpaneltitle { text-transform:uppercase; margin-bottom:15px; }
		#photos-page-title { font-size:140%; margin:20px 0 10px; padding:10px 0 5px; }
		.backlink { color:#000000; font-size:110%; font-weight:bold; margin:4px 0 0; }
	        #photonav { clear:both; width:620px; }
		#ltnav, #rtnav { overflow:hidden; border:1px solid #888; padding:3px 5px; background:#D9D5BB; font-weight:bold; }
		#ltnav { float:left; }
		#rtnav { float:right; }
	</style>

	<div id="content" >


		<?php the_post(); ?>
		<div id="post-<?php the_ID() ?>" class="post">
			<div style="clear:both;"></div>

				<div class="contentpanel bm-half">
					<h2 class="contentpaneltitle"> View all snaps with <?php the_title() ?></h2>
					<?php if( $connectedSnaps->have_posts() ) : ?>
						<ul class="location-photos">

							<?php  while( $connectedSnaps->have_posts() ) : $connectedSnaps->the_post(); ?>

								<li>



                                    <?php    /*******************************************************************************************************************/ ?>
                                    <?php get_template_part( '/partials/tokyo', 'image_persons' ); ?>
                                    <?php    /*******************************************************************************************************************/ ?>




                                </li>

							<?php  endwhile; ?> 
							<?php wp_reset_postdata(); ?>
						</ul>		
					<?php endif; ?>
					
				</div>	

			<div>
				<img src="<?php bloginfo('template_directory'); ?>/images/bottom_of_article_divider.gif" height="13" width="300" alt="">
			</div>

		</div><!-- .post -->

	</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>