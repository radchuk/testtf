<?php
/*
Template Name: Front Page
*/
?>

<?php get_header() ?>

<?php
$features_category_id = 819;
$news_category_id = 216;
$other_recent_categories_id = '1,819,822,1170'; // "Other Stuff", "Brand Profiles", "Interviews"
?>

<?php //include (TEMPLATEPATH . '/config.php'); ?>

	<div id="content">
	
		<div class="contentpanel">
			<a href="<?php echo get_category_link($features_category_id); ?>" class="morelink">Browse All Features &raquo;</a>
			<h2 class="contentpaneltitle">Tokyo Fashion Features</h2>
			<div id="last">
				<?php
					$ids = array();
					$latestNewsPosts = new WP_Query();
					$latestNewsPosts->query('showposts=1&cat='.$features_category_id);
				?>
				<?php if ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>
                    <?php    /*******************************************************************************************************************/ ?>
                    <?php   get_template_part( '/partials/tokyo', 'image_big2' ); ?>
                    <?php   /*******************************************************************************************************************/ ?>


                    <h3 class="snippet-title">
                       <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                    </h3>
				<p class="snippet-intro"><?php echo truncate($post->post_excerpt, 200); ?></p>
				<?php $ids[]= $post->ID; endif; ?>	
			</div>
			
			<div id="latest">
				<ul id="latest-snippets">
					<?php
						$latestNewsPosts = new WP_Query();
						$latestNewsPosts->query('showposts=2&offset=1&cat='.$features_category_id);
					?>
					<?php while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>
					<li>
						<?php
						if(has_post_thumbnail( $post->ID )) { 

								$url= get_url_image($post->ID);
								tf_bfi_image($url, 140, 180,'1');
							
							
							} elseif
							(get_post_meta($post->ID, "image", $single = true) != "") { 					
						$url = get_post_meta($post->ID, "image", $single = true);
					
							tf_bfi_image($url, 140, 180,'2' );
							
							} else { ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php the_title(); ?>" class="snippet-thumb" /></a>
						<?php } ?>  

						<h4 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
						<p class="snippet-intro"><?php echo truncate($post->post_excerpt, 100); ?></p>
					</li><!-- .post -->
					<?php $ids[]= $post->ID; endwhile ?>
				</ul>
				
				<h3 class="blocksubtitle">More Tokyo Fashion Features</h3>
				<ul id="latest-list">
					<?php
						$latestNewsPosts = new WP_Query();
						$latestNewsPosts->query('showposts=3&offset=3&cat='.$features_category_id);					
					?>
					<?php while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post() ?>
					<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
					<?php $ids[]= $post->ID; endwhile ?>
				</ul>
				
			</div>
		</div>		

		<?php include (TEMPLATEPATH . '/carousel_features.php'); ?>
		
		<div class="contentpanel3">
			<h2 class="contentpaneltitle">Tokyo Fashion News</h2>
			<?php
				$latestNewsPosts = new WP_Query();
				$latestNewsPosts->query('cat='.$news_category_id); // 819,1,822,1170
				$postcounter = 1;
			?>
			<?php
				while ($latestNewsPosts->have_posts()) : $latestNewsPosts->the_post();
				if (!in_array($post->ID, $ids) && $postcounter < 6) {
			?>
			<div class="snippet">
				<div class="snippet-left">

                    <?php    /*******************************************************************************************************************/ ?>
                    <?php    get_template_part( '/partials/tokyo', 'image_small2' ); ?>
                    <?php    /*******************************************************************************************************************/ ?>

                </div>
				<div class="snippet-right">
					<h3 class="snippet-title js-share"><a data-twitter="url=<?php the_permalink() ?>&text=<?php the_title(); ?>" data-fb="u=<?php the_permalink() ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
					<p class="snippet-meta">Posted in <?php the_category(', '); ?></p>
					<p class="snippet-intro"><?php the_excerpt(); ?></p>
					<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>
				</div>
			</div><!-- .snippet -->
				<?php $ids[]= $post->ID; $postcounter++; ?>
			<?php } endwhile ?>
			
		</div>

		<div class="contentpanel4">
			<h2 class="contentpaneltitle">Other Recent Tokyo Fashion Articles</h2>
			<?php
				
				$other_recent_posts_array = array();
				
				$sql="
									SELECT 
										".$table_prefix."posts.ID,
										".$table_prefix."posts.post_title
									FROM ".$table_prefix."posts
									INNER JOIN ".$table_prefix."term_relationships ON ( ".$table_prefix."posts.ID = ".$table_prefix."term_relationships.object_id )
									INNER JOIN ".$table_prefix."term_taxonomy ON ( ".$table_prefix."term_relationships.term_taxonomy_id = ".$table_prefix."term_taxonomy.term_taxonomy_id )
									WHERE  ".$table_prefix."term_taxonomy.taxonomy = 'category'
									AND ".$table_prefix."term_taxonomy.term_id
									IN (
										$other_recent_categories_id
									)
									AND ".$table_prefix."posts.post_type = 'post'
									AND (
										".$table_prefix."posts.post_status = 'publish'
									)
									AND ID not IN (".implode(",",$ids).")
									GROUP BY ".$table_prefix."posts.ID
									ORDER BY ".$table_prefix."posts.post_date DESC
									LIMIT 0 , 6
							";			
							
				$res = mysql_query($sql);
				
				
				while($other_recent_post = mysql_fetch_object($res)) 
				{				  				  				  
					$other_recent_posts_array[] = array("ID" => $other_recent_post->ID, "post_title" => $other_recent_post->post_title);					
				}						
				
				// this version uses too much unneeded data
				/*
				$other_recent_posts = get_posts('numberposts=-1&offset=1&category='.$other_recent_categories_id);
				$postcounter = 1;
				foreach($other_recent_posts as $other_recent_post) :
				  if (!in_array($other_recent_post->ID, $ids) && $postcounter < 7) {
					$other_recent_posts_array[] = array("ID" => $other_recent_post->ID, "post_title" => $other_recent_post->post_title);
					$postcounter++;
				  }
				endforeach;
				*/
				
				$total_records = count($other_recent_posts_array);
				if ($total_records > 0) {
				  $half_records = ceil($total_records / 2);
				  $cnt_records = 1;
			?>
			<div class="snippet">
			  <ul>
			<?php
				for($p=0; $p<count($other_recent_posts_array); $p++) {
			?>
				<li><h3 class="snippet-title"><a href="<?php echo get_permalink($other_recent_posts_array[$p]["ID"]) ?>"><?php echo $other_recent_posts_array[$p]["post_title"]; ?></a></h3></li>
			<?php
				  if ($cnt_records == $half_records) {
			?>
			  </ul><ul>
			<?php
			      }
				  $cnt_records++;
				}
			?>
			  </ul>
			</div><!-- .snippet -->
			<?php } ?>
		</div>

	</div><!-- #content -->

	<div id="sidebar">		

		<div class="sidepanel">
			<h3>About TokyoFashion.com</h3>
			<p><strong>TokyoFashion.com</strong> is an online magazine devoted to clothing, accessories, and style in Tokyo, Japan.</p>
	
			<p>Sounds simple, eh? But it's not as simple as it once was - Tokyo Fashion these days is a large and fast moving organism. There are new brands and new trends starting and old ones dying every day in Japan's capital of style.</p>
	
			<p>Our goal is to keep you up to date with what's going on in Japanese fashion, day to day, hour to hour.</p>
		</div>
		
		<div class="sidepanel isacc">
			<h3>Shopping in Tokyo</h3>
			<div class="accordion">
				<a href="javascript:void(0);" class="acc-head">Fashion Brands</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=415'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Clothing Stores</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=823'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Department Stores</a>			
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=824'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Tokyo Fashion Maps</a>
				<div class="acc-content">
					<ul class="acc-grid">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=825'); ?>
					</ul>
				</div>
			</div>
		</div>	
	
		<div class="sidepanel">
			<h3>Our Sponsors</h3>
<br />
<!-- Start AdSense Code -->
<script type="text/javascript"><!--
google_ad_client = "pub-3685786506219492";
/* TF 300x250, created 6/10/09 */
google_ad_slot = "5102142911";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
			<!-- End AdSense Code -->
		</div>
		
		<div class="sidepanel">
			<h3>Tokyo Fashion Photos</h3>
			<?php get_flickrRSS(); ?>
		</div>
		
		<div class="sidepanel">
			<h3>More Tokyo Fashion</h3>
			<div class="accordion">
				<a href="javascript:void(0);" class="acc-head">Fashion Magazines</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=827'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Japan Forums</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=826'); ?>
					</ul>
				</div>
				<a href="javascript:void(0);" class="acc-head">Fashion Trends</a>
				<div class="acc-content">
					<ul class="acc-list">
						<?php wp_list_bookmarks('title_li=&categorize=0&category=276'); ?>
					</ul>
				</div>
			</div>
		</div>
	
	</div><!-- #sidebar -->
	
<?php get_footer() ?>