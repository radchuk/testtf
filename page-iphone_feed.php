<?php
ini_set("memory_limit", "100M"); 
/*
Template Name: iPhone feed
*/

include (TEMPLATEPATH . '/iphone_include_header.php'); 

	if (isset($_GET["articleType"])) {
		$str_temp=$_GET["articleType"];	
		if (($_GET["perPage"]) AND ($_GET["perPage"]!="")) {$perPage=(int) $_GET["perPage"];} else {$perPage=30;}
		if (($_GET["page"]) AND ($_GET["page"]!="")) {$page=(int) $_GET["page"];} else {$page=1;}
		if (($_GET["id"]) AND ($_GET["id"]!="")) {$id=(int) $_GET["id"];} else {$id=0;}
		if ($_GET["model"]=="iphone3") {$model=3;} else {$model=4;}
		
		if ($str_temp == 'FeaturedArticle')
		{
			$str = iphone_news(1,$model,$perPage,$page);
			echo ($str);
		}
		if ($str_temp == 'TokyoFashionNews')
		{
			$str = iphone_news(2,$model,$perPage,$page);
			echo ($str);
		}
		if ($str_temp == 'OtherStuff')
		{
			$str = iphone_news(6,$model,$perPage,$page);
			echo ($str);
		}
		if ($str_temp == 'StreetSnaps')
		{
			$str = iphone_StreetSnaps($model,$perPage,$page,$id);
			echo ($str);
			
		}
	}

	if (isset($_GET["query"])) {
		$str_temp=$_GET["query"];
		
		if ($str_temp == 'popularBrands')
		{		
			$str = iphone_popularBrands();
			echo ($str);
		}
	}

	if ((isset($_GET["brands"])) AND (empty($_GET["debug"]))) {
		$brand_id=intval($_GET["brands"]);
		$str = iphone_Brands($brand_id);
		echo ($str);
	}

	if ((isset($_GET["id_post"])) AND (empty($_GET["debug"]))) {
		$post_id=intval($_GET["id_post"]);
		if ($_GET["model"]=="iphone3") {$model=3;} else {$model=4;}
		$str = iphone_post_id($post_id,$model);
		echo ($str);
	}

	/* SEARCH */
	$allowed_models = array('iphone4', 'iphone3', 'android');
	
	if (isset($_GET["search"]) AND empty($_GET["debug"])) {
		$sfs = $_GET["search"];
		if (in_array($_GET['model'], $allowed_models)) $model = $_GET['model']; else $model = $allowed_models[0];
		if (isset($_GET["page"])){$paged=intval($_GET["page"]);} else {$paged=1;}
		if (isset($_GET["perPage"])){$perPage=intval($_GET["perPage"]);} else {$perPage=10;}

		$str = search_for_sait(
					$sfs,
					$paged,
					$perPage,
					$model,
					@$_GET['format']=='format2'?'format2':'' 
				);
		echo ($str);
	}
	/* END SEARCH */
	
	/* COMMENTS FOR POST */
	if ((isset($_GET["comments_post_id"])) AND (empty($_GET["debug"]))) {
		$id_post_comments_id=intval($_GET["comments_post_id"]);		
		$str = iphone_comments($id_post_comments_id);
		echo ($str);
	}
	/* END COMMENT */
	/* ADD COMMENT */
	if ((isset($_POST['comment_post_ID'])) AND (empty($_GET["debug"]))) {
		$str = add_comment($_POST);
		echo ($str);
	}
	/* END ADD COMMENT */
	
include (TEMPLATEPATH . '/iphone_include_footer.php'); 
