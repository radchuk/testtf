<?php
/***
**
**	for LOCAL version:
**		disabled:	1. Read the current time
**					5. Update DB for count posts of the brands
**					6. Cache for feeds
**		enabled:	2. Authorization
**					3. function to parse the http auth header
**					4. Debug
**
**	for TEST-SERVER (http://67.23.1.18/) version:
**		disabled:	1. Read the current time
**					2. Authorization
**					3. function to parse the http auth header
**					5. Update DB for count posts of the brands
**		enabled:	4. Debug
**					6. Cache for feeds
**
**	for MAIN-SITE (http://tokyofashion.com/) version:
**		disabled:	1. Read the current time
**					5. Update DB for count posts of the brands
**		enabled:	2. Authorization
**					3. function to parse the http auth header
**					4. Debug
**					6. Cache for feeds
**
***/


// 1. Read the current time
/*
$start_time = microtime();
// разделяем секунды и миллисекунды
//(становятся значениями начальных ключей массива-списка)
$start_array = explode(" ",$start_time);
// это и есть стартовое время
$start_time = $start_array[1] + $start_array[0];
*/

// 2. Authorization for iPhoneFeed
$realm = 'Restricted area iphone_feed';

//user => password
$users = array('feed_iphone' => 'J5wXbXpE');

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.
           '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('Text to send if user hits Cancel button');
}

// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($users[$data['username']]))
    die('Wrong Credentials!');


// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
    die('Wrong Credentials!');

// 3. function to parse the http auth header
//file functions custom-queries.php!!!
if (file_exists(TEMPLATEPATH . '/custom-queries.php')) {
	require_once(TEMPLATEPATH . '/custom-queries.php');


// 4. Debug
if (!empty($_GET["debug"]))
{
	$post_id=intval($_GET["id_post"]);	
	$str = iphone_post_id($post_id);
	$xml = new SimpleXmlElement($str);
	foreach ($xml->post->post_content as $entry){
	    $namespaces = $entry->getNameSpaces(true);
		echo $entry;
	}
	exit;
} else 
{
	Header('Content-type: text/xml');
}

// 5. Update DB for count posts of the brands
/*if (!empty($_GET["update_base"]))
{
	global $wpdb;
	$querystr = "SELECT wp_t_t.term_id AS term_id, count(wp_t_t.term_id) AS count_me
					FROM wp_term_taxonomy AS wp_t_t
					LEFT JOIN wp_term_relationships AS wp_t_p ON wp_t_t.term_taxonomy_id = wp_t_p.term_taxonomy_id
					LEFT JOIN wp_posts AS wp_p ON wp_t_p.object_id = wp_p.ID
					WHERE wp_p.post_status = 'publish'
						AND wp_t_t.taxonomy = 'brands'
					GROUP BY wp_t_t.term_id";
	$count_temp = $wpdb->get_results($querystr, OBJECT);
	$querystr = "SELECT *
					FROM wp_term_taxonomy AS wp_t_t
					WHERE wp_t_t.taxonomy = 'brands'";
	$count_temp1 = $wpdb->get_results($querystr, OBJECT);
	foreach ($count_temp1 AS $row)
	{
		$querystr = "UPDATE wp_term_taxonomy SET count = 0 WHERE term_id = '".$row->term_id."'";
		$wpdb->query($querystr);
	}
	foreach ($count_temp AS $row)
	{
		$querystr = "UPDATE wp_term_taxonomy SET count = '".$row->count_me."' WHERE term_id = '".$row->term_id."'";	
		$wpdb->query($querystr);
	}
	$querystr = "SELECT wp_t_t.term_id AS term_id, count(wp_t_t.term_id) AS count_me
					FROM wp_term_taxonomy AS wp_t_t
					LEFT JOIN wp_term_relationships AS wp_t_p ON wp_t_t.term_taxonomy_id = wp_t_p.term_taxonomy_id
					LEFT JOIN wp_posts AS wp_p ON wp_t_p.object_id = wp_p.ID
					WHERE wp_p.post_status = 'publish'
						AND wp_t_t.taxonomy = 'music'
					GROUP BY wp_t_t.term_id";
	$count_temp = $wpdb->get_results($querystr, OBJECT);
	$querystr = "SELECT *
					FROM wp_term_taxonomy AS wp_t_t
					WHERE wp_t_t.taxonomy = 'music'";
	$count_temp1 = $wpdb->get_results($querystr, OBJECT);
	foreach ($count_temp1 AS $row)
	{
		$querystr = "UPDATE wp_term_taxonomy SET count = 0 WHERE term_id = '".$row->term_id."'";
		$wpdb->query($querystr);
	}
	foreach ($count_temp AS $row)
	{
		$querystr = "UPDATE wp_term_taxonomy SET count = '".$row->count_me."' WHERE term_id = '".$row->term_id."'";	
		$wpdb->query($querystr);
	}
	exit;
}*/


// 6. Cache for feeds
$uri_str = $_SERVER['QUERY_STRING'].'.php';
$search = array("=", "&");
$name_file = str_replace($search, "_", $uri_str);
$file_url='./wp-content/cache/'.$name_file;

//if ((file_exists($file_url)) AND (empty($_POST['comment_post_ID'])) AND (empty($_GET["comments_post_id"])) AND (empty($_GET["search"]))) {
//	Header('Content-type: text/xml');
//	echo file_get_contents($file_url);
//	die();
//}
