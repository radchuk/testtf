<?php
/*
Template Name: Music
*/
$music_count = 9 * 4; // n Rows x 4 columns, change only n
?>
<?php get_header() ?>

	<div id="content">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<div class="post-content">
				<div id="mixi_button">
					<a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="<?=MIXI_KEY?>">Check</a>
					<script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
				</div>
                <div id="facebook_button">
                    <?php if (function_exists('fbshare_manual')) echo fbshare_manual(); ?>
                </div>
				<div id="ftwitter_button">
					<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="tokyofashion" data-related="tokyofashion">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
				</div>
				<?php the_content() ?>
			</div>

			<br/>
			<h2 class="contentpaneltitle">Popular Music on Tokyo Fashion</h2>
			<div id="music-tag-cloud">
			<?php wp_tag_cloud( array( 'taxonomy' => 'music', 'number' => 45 ) ); ?>
			</div>

			<br/>
			<br/>
			<h2 class="contentpaneltitle">Music Recently Heard on the Streets of Tokyo...</h2>
			<style type="text/css">
			<!--
			.music-photos { clear:both; width:620px; margin-left:0 !important; }
			.music-photos li { margin:0 15px 0 0; width:140px; float:left; list-style-type:none; height:195px; position:relative; }
			.music-photos img { margin:15px 10px 5px 5px; }
			.music-photos li a span { position:absolute; left:0; bottom:0; margin-left:5px; width:100%; z-index:1; text-align:center; padding:5px 0; background:#000; -moz-opacity:.70; filter:alpha(opacity=70); opacity:.70; color:#FFF; font-weight:bold; }
			-->
			</style>
			<ul class="music-photos"><?php listRecentMusics(); ?></ul>
		</div><!-- .post -->

	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>

<?php
function listRecentMusics() {
	global $wpdb, $music_count;
	$querystr = "SELECT term_taxonomy_id, object_id FROM (SELECT object_id, term_taxonomy_id, count(*) as count FROM $wpdb->term_relationships JOIN $wpdb->posts ON object_id = ID WHERE term_taxonomy_id IN (SELECT term_taxonomy_id as id FROM $wpdb->term_taxonomy WHERE taxonomy = 'music') AND post_type = 'post' AND post_status = 'publish' GROUP BY object_id ORDER BY ID DESC) as tmp GROUP BY term_taxonomy_id ORDER BY object_id DESC LIMIT $music_count";

	$recent_musics = $wpdb->get_results($querystr, OBJECT);
	if($recent_musics) {
		foreach($recent_musics as $music) {
			showMusicPostImage($music->object_id, $music->term_taxonomy_id);
		}
	}
}

// Function to show image of given post highlighting a music
function showMusicPostImage($post_id, $music_id) {
	global $wpdb;
	$post = get_post($post_id);
	if ( ! $music = wp_cache_get($music_id, 'music_thumb') ) {
			$music = $wpdb->get_var( $wpdb->prepare( "SELECT t.name FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = 'music' AND tt.term_taxonomy_id = %s LIMIT 1", $music_id) );
			wp_cache_add($music_id, $music, 'music_thumb');
		}
?>
<li>
	<?php if (get_post_meta($post->ID, "image", true) != "") { ?>
	<a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo $post->post_title; ?>"><img src="<?php bloginfo('template_directory'); ?>/thumb.php?src=<?php echo get_post_meta($post->ID, "image", true); ?>&amp;w=140&amp;h=180&amp;zc=1&amp;q=95" alt="<?php echo $post->post_title; ?>" /><span><?php echo $music;?></span></a>
	<?php } ?>
</li>
<?php
}
