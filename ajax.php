<?php
$root = dirname(dirname(dirname(dirname(__FILE__))));
if (file_exists($root.'/wp-load.php')) {
	require_once($root.'/wp-load.php');
	
//file functions custom-queries.php!!!	
if (file_exists(TEMPLATEPATH . '/custom-queries.php')) {
	require_once(TEMPLATEPATH . '/custom-queries.php');
	
	header('Content-Type: text/html; charset='.get_option('blog_charset').'');
	$action = $_GET['action'];
	if($action == 'ajax-tag-search') {
		$term = stripslashes($_GET['term']);

		$s = $_GET['q']; // is this slashed already?
		if ( false !== strpos( $s, ',' ) ) {
			$s = explode( ',', $s );
			$s = $s[count( $s ) - 1];
		}
		$s = trim( $s );
		if ( strlen( $s ) < 2 )
			die; // require 2 chars for matching

		if($term == 'post_tag' || $term == 'brands' || $term == 'music') {
			global $wpdb;
			$results = $wpdb->get_col( "
						  SELECT t.name
						  FROM $wpdb->term_taxonomy AS tt
						  INNER JOIN $wpdb->terms AS t
						  ON tt.term_id = t.term_id
						  WHERE tt.taxonomy = '$term'
						  AND t.name LIKE ('%" . $s . "%')" );

			echo join( $results, "\n" );
			die;
		}
	} else if($action = 'ajax-photo-search') {

		$limit = 24;

		$post_tag = my_cleanup($_POST['post_tag']);
		$brands = my_cleanup($_POST['brands']);
		$music = my_cleanup($_POST['music']);
		$location = my_cleanup($_POST['loc']);
		$sex = my_cleanup($_POST['sex']);
		$start = (int) ($_POST['start']) * $limit;

		$querystr = "SELECT * FROM $wpdb->posts as wpost";

		// Limit to Category Tokyo Street Snaps
		$querystr .= " INNER JOIN $wpdb->term_relationships r0 ON (wpost.ID = r0.object_id)
					    INNER JOIN $wpdb->term_taxonomy t0 ON (r0.term_taxonomy_id = t0.term_taxonomy_id) AND t0.taxonomy = 'category' AND t0.term_id = '697'";

		if($post_tag) {
			$querystr .= " INNER JOIN $wpdb->term_relationships r1
						    ON (wpost.ID = r1.object_id)
						    AND r1.term_taxonomy_id IN
							(SELECT term_taxonomy_id FROM $wpdb->term_taxonomy as tt1
								INNER JOIN $wpdb->terms t1 ON (tt1.term_id = t1.term_id) AND t1.name IN ($post_tag) AND tt1.taxonomy = 'post_tag') ";
		}

		if($brands) {
			$querystr .= " INNER JOIN $wpdb->term_relationships r2
						    ON (wpost.ID = r2.object_id)
						    AND r2.term_taxonomy_id IN
							(SELECT term_taxonomy_id FROM $wpdb->term_taxonomy as tt2
								INNER JOIN $wpdb->terms t2 ON (tt2.term_id = t2.term_id) AND t2.name IN ($brands) AND tt2.taxonomy = 'brands') ";
		}

		if($music) {
			$querystr .= " INNER JOIN $wpdb->term_relationships r3
						    ON (wpost.ID = r3.object_id)
						    AND r3.term_taxonomy_id IN
							(SELECT term_taxonomy_id FROM $wpdb->term_taxonomy as tt3
								INNER JOIN $wpdb->terms t3 ON (tt3.term_id = t3.term_id) AND t3.name IN ($music) AND tt3.taxonomy = 'music') ";
		}

		$querystr .= " WHERE wpost.post_status = 'publish' ";

		if($location) {
			$querystr .= " AND wpost.ID IN 
					      (SELECT m.post_id FROM wp_postmeta m WHERE m.meta_key = 'photo-location' AND m.meta_value = $location)";
		}
		if($sex) {
			$querystr .= " AND wpost.ID IN
					      (SELECT m.post_id FROM wp_postmeta m WHERE m.meta_key = 'photo-people' AND m.meta_value = $sex)";
		}
		$querystr .= "GROUP BY wpost.ID ORDER BY wpost.post_date DESC LIMIT $start, $limit";

		$pageposts = $wpdb->get_results($querystr, OBJECT);
		$content = '';
		$count = '';
		if ($pageposts): 
		    foreach ($pageposts as $post): 
				$count++;
        		setup_postdata($post);
		        //display your post data with the usual functions like the_title(), etc

?>



                <?php    /*******************************************************************************************************************/ ?>
                <?php   get_template_part( '/partials/tokyo', 'image_ajax' ); ?>
                <?php   /*******************************************************************************************************************/ ?>


<?php

            endforeach;
		endif;
		if($count == $limit) { $more = 1; } else { $more = 0; }

	    //Add a custom field, let's us know which template is being use
	   	$response = new WP_Ajax_Response();
    	$response->add( array(
	        'supplemental' => array('more' => $more),
	        'data' => $content
	    ));
	    $response->send();
	}
}
