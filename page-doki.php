<?php
/*
Template Name: 6%DokiDoki World Tour
*/
?>
<?php get_header() ?>

	<div id="fullwidth">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<div class="post-content">
				<?php the_content() ?>
			</div>

		</div><!-- .post -->

	</div><!-- #fullwidth -->

<?php get_footer() ?>

