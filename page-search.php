<?php
/*
Template Name: Private Search
*/
include_once('header-search.php');
remove_filter('the_content', 'tm_update'); // To stop re-tweet button from showing.
?>

	<div id="content-full" style="width:930px;">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<h1 class="post-title">Welcome Artists</h1>
			<div class="post-content">
				<?php the_content() ?>
			</div>
<?php
	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] == $post->post_password) {  // and it match the cookie
			show_search_form();
		}
	}
?>
		</div><!-- .post -->

	</div><!-- #content -->

<?php get_footer() ?>

<?php
function show_search_form() {
?>
<style type="text/css">
#private-search #stitle { font-size:14px; margin-top:10px; }
#private-search table th { font-weight:bold; font-style:italic; }
#private-search input { border-top:1px solid #222; border-left:1px solid #222; border-right:1px solid #666; border-bottom:1px solid #666; color:#333; }
#private-search input[type=text] { width:150px; }
#private-search .switch { color:#CCC; background:#000; margin:3px; padding:0 10px; }
#private-search .overlay-photos { width:950px; }

.more { clear:both; background-color:#FFFFFF; background-image:url(<?php echo get_bloginfo('template_url');?>/images/more.gif);  background-position:left top; background-repeat:repeat-x; border-color:#DDDDDD #AAAAAA #AAAAAA #DDDDDD; border-style:solid; border-width:1px; display:block; font-size:14px; font-weight:bold; height:22px; line-height:1.5em; margin:10px 0; padding:6px 0; text-align:center; text-shadow:1px 1px 1px #FFFFFF; width:925px; }
.more:hover { background-position:left -78px; border:1px solid #BBBBBB; text-decoration:none; }
.more:active { background-position:left -38px; color:#666666; }
.more.loading { background-color:#FFFFFF; background-image:url(<?php echo get_bloginfo('template_url');?>/images/ajax.gif); background-position:50% 50%; background-repeat:no-repeat; border:1px solid #EEEEEE; cursor:default !important; }
.round{-moz-border-radius:5px;-webkit-border-radius:5px;}

.ac_results { border: 1px solid gray; background-color: white; padding: 0; margin: 0; list-style: none; position: absolute; z-index: 10000;  display: none; }
.ac_results li { padding: 2px 5px; 	white-space: nowrap; color: #101010; text-align: left; }
.ac_over { 	cursor: pointer; background-color: #F0F0B8; }
.ac_match { text-decoration: underline; color: black; }

</style>

<script type='text/javascript' src='<?php echo get_bloginfo('url');?>/wp-includes/js/jquery/suggest.js'></script>
<script type='text/javascript' src='<?php echo get_bloginfo('url');?>/wp-includes/js/wp-ajax-response.js'></script>
<script type="text/javascript">
jQuery(document).ready( function($) {
	var $j = jQuery;
	var start = $j("#start").attr("value");

	// auto-suggest stuff
	$j('.tag').each(function(){
		var term = $j(this).attr('id');
		$j(this).suggest( '<?php echo get_bloginfo('template_url');?>/ajax.php?action=ajax-tag-search&term='+term, { delay: 500, minchars: 2, multiple: true, multipleSep: ", " } );
	});

	$j('#search').bind("click", function() {
		$j("#s_post_tag").attr("value", $j("#post_tag").attr("value"));
		$j("#s_brands").attr("value", $j("#brands").attr("value"));
		$j("#s_music").attr("value", $j("#music").attr("value"));
		$j("#s_loc").attr("value", $j("#location option:selected").attr("value"));
		$j("#s_sex").attr("value", $j("#sex option:selected").attr("value"));
		search_now(true); return false;
	});
	$j('#more').bind("click", function() {
		search_now(false); return false;
	});
	$j('#reset').bind("click", function() {
		$j("#s_post_tag").attr("value", "");
		$j("#s_brands").attr("value", "");
		$j("#s_music").attr("value", "");
		$j("#s_loc").attr("value", "");
		$j("#s_sex").attr("value", "");
		search_now(true);
		return true;
	});

	function search_now(replace_data) {
		var s = { };
		s.nonce = $j("#search_nonce").attr("value");
		s.response = "ajax-response";
		s.action = "ajax-photo-search";
		s.post_tag = $j("#s_post_tag").attr("value");
		s.brands = $j("#s_brands").attr("value");
		s.music = $j("#s_music").attr("value");
		s.loc = $j("#s_loc").attr("value");
		s.sex = $j("#s_sex").attr("value");

		s.type = "POST";
		if(replace_data == true) {
			$j("#start").attr("value", "0");
			start = 0;
		}
		s.start = $j("#start").attr("value");
		s.url = '<?php echo get_bloginfo('template_url');?>/ajax.php';

		s.data = $j.extend({ action: s.action }, { _ajax_nonce: s.nonce }, { _wpnonce: s.nonce }, { post_tag:s.post_tag }, {brands:s.brands}, {music:s.music}, {loc:s.loc}, {sex:s.sex}, {start:s.start});
		s.global = false;
		s.timeout = 30000;
		//Change the edit text and events
		$j("#search").attr("value", "Loading...");
		$j("#search").attr("disabled", "disabled");
		$j("#more").addClass("loading");
		$j("#more").html("");
		s.success = function(r) {
			var res = wpAjax.parseAjaxResponse(r, s.response,s.element);
			var rdata = res.responses[0].data;
			var rmore = res.responses[0].supplemental.more;

			if(rdata == '') {
				alert('No match found for this Search');
			} else {
				start++;
				$j("#start").attr("value", start);
			}
			if(rmore == 1) {
				$j('#more').show();
			} else {
				$j('#more').hide();
			}
			if(replace_data == true) {
				$j("#search-result").html(rdata);
			} else {
				$j("#search-result").append(rdata);
			}
			$j("#search").attr("value", "Search");
			$j("#search").removeAttr("disabled", "disabled");
			$j("#more").removeClass("loading");
			$j("#more").html("SHOW MORE PICTURES");
		}

		$j.ajax(s);
	}
});
</script>
	<div id="private-search">
	<h2 id="stitle">Find Pics By:</h2>
	<form action="" method="POST" id="searchform">
		<?php wp_nonce_field('search_nonce');?>
		<table width="100%">
			<tr>
				<th>Keywords</th>
				<th>Fashion Brands</th>
				<th>Music Tastes</th>
				<th>Location</th>
				<th>Sex</th>
				<td><input type="reset" name="reset" id="reset" value="Reset" class="switch" /></td>
			</tr>
			<tr>
				<td><input type="text" class="tag" name="post_tag" id="post_tag" value="" /></td>
				<td><input type="text" class="tag" name="brands" id="brands" value="" /></td>
				<td><input type="text" class="tag" name="music" id="music" value="" /></td>
				<td><?php echo get_location_list(); ?></td>
				<td><?php echo get_sex_list(); ?></td>
				<td>
					<input type="button" name="search" id="search" value="Search" class="switch" />
					<input type="hidden" id="start" value="1" />
					<input type="hidden" id="s_post_tag" value="" />
					<input type="hidden" id="s_brands" value="" />
					<input type="hidden" id="s_music" value="" />
					<input type="hidden" id="s_loc" value="" />
					<input type="hidden" id="s_sex" value="" />
				</td>
			</tr>
		</table>
	</form>

	<br/><br/>
	<h2 class="contentpaneltitle">Tokyo Street Fashion Photos</h2>
	<ul id="search-result" class="overlay-photos"><?php $more = show_recent_photos(); ?></ul>
	<br style="clear:both;"/>
	<a id="more" class="more round" href="#" <?php if(!$more) { echo 'style="display:none;"'; }?>>SHOW MORE PICTURES</a>
	</div>
<?php
}

function get_location_list() {
	global $wpdb;
	$location = $wpdb->get_col("SELECT meta_value  FROM $wpdb->postmeta WHERE `meta_key` = 'photo-location' AND `meta_value` IS NOT NULL GROUP BY meta_value");
	$output .= '<select name="location" id="location"><option value="">All Locations</option>';
	foreach($location as $place) {
		$output .= '<option value="'.$place.'">'.$place.'</option>';
	}
	$output .= '</select>';
	return $output;
}

function get_sex_list() {
	$sex = array('Girls', 'Guys', 'Group');
	$output .= '<select name="sex" id="sex"><option value="">All</option>';
	foreach($sex as $s) {
		$output .= '<option value="'.$s.'">'.$s.'</option>';
	}
	$output .= '</select>';
	return $output;
}

function show_recent_photos() {
		global $wpdb;
		$limit = 24;
		$querystr = "SELECT * FROM $wpdb->posts as wpost";

		// Limit to Category Tokyo Street Snaps
		$querystr .= " INNER JOIN $wpdb->term_relationships r0 ON (wpost.ID = r0.object_id)
						INNER JOIN $wpdb->term_taxonomy t0 ON (r0.term_taxonomy_id = t0.term_taxonomy_id) AND t0.taxonomy = 'category' AND t0.term_id = '697'";

		$querystr .= " WHERE wpost.post_status = 'publish' ";

		$querystr .= "GROUP BY wpost.ID ORDER BY wpost.post_date DESC LIMIT $limit";

		$pageposts = $wpdb->get_results($querystr, OBJECT);
		$count = 0;
		if ($pageposts):
			foreach ($pageposts as $post):
				$count++;
				setup_postdata($post);
				//display your post data with the usual functions like the_title(), etc
?>

<li>
    <?php    /*******************************************************************************************************************/ ?>
    <?php get_template_part( '/partials/tokyo', 'image_search' ); ?>
    <?php    /*******************************************************************************************************************/ ?>
</li>

<?php
			endforeach;
		endif;
		if($count == $limit) {
			return true;
		}
		return false;
}
