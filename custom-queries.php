<?php
//function from ajax.php
function my_cleanup($str) {
	$str = trim(stripslashes($str));
	if($str == '') return false;
	if(substr($str, -1, 1) == ',') {
		$str = substr($str, 0, -1);
	}
	$str = explode(',', $str);
	$output = ' ';
	foreach($str as $s) {
		if($s == '') continue;
		$output .= " '$s',";
	}
	$output = substr($output, 0, -1);
	return $output;
}
?>
<?php
//functions from index.php
function loop_normal() {
		global $post;
		while ( have_posts() ) : the_post() ?>
		<div class="snippet">
			<div class="snippet-left">

                <?php    /*******************************************************************************************************************/ ?>
                <?php get_template_part( '/partials/tokyo', 'image_small2' ); ?>
                <?php    /*******************************************************************************************************************/ ?>

            </div>
			<div class="snippet-right">
				<h3 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?> in <?php the_category(', '); ?></p>
				<p class="snippet-intro"><?php the_excerpt(); ?></p>
				<p class="snippet-readmore"><a href="<?php the_permalink() ?>">Read the full article &raquo;</a></p>
			</div>
		</div><!-- .snippet -->
		<?php endwhile;
}

function loop_photo_overlay() {
	global $post, $query_string;
	query_posts($query_string . '&posts_per_page=36');
?>
    <ul id="brand-page" class="overlay-photos">
            <?php while ( have_posts() ) : the_post() ?>
    <li>
    
        <?php    /*******************************************************************************************************************/ ?>
        <?php get_template_part( '/partials/tokyo', 'image_small3' ); ?>
        <?php    /*******************************************************************************************************************/ ?>
    
    </li>
            <?php endwhile ?>
    </ul>
<?php
}

function brand_profile_snap() {
	global $BrandAssociation, $wp_query;
	$brand = $wp_query->queried_object->name;
	if(isset($BrandAssociation)) {
		$post = $BrandAssociation->get_brand_profile();
		if(!$post) return;
		$my_query = new WP_Query('p='.$post->ID);
		if($my_query->have_posts()): $my_query->the_post();
?>
		<h3 id="brandtitle" class="contentpaneltitle"><?php the_title(); ?></h3>
		<div class="post-content">
            <?php    /*******************************************************************************************************************/ ?>
            <?php get_template_part( '/partials/tokyo', 'image_alignright' ); ?>
            <?php    /*******************************************************************************************************************/ ?

            <h3 class="snippet-title brand-profile"><a href="<?php the_permalink() ?>" title="<?php echo $brand;?>"><?php the_title(); ?> Brand Profile</a></h3>
			<p class="snippet-intro"><? the_excerpt();?></p>
		
			<p class="snippet-readmore"><a href="<?php the_permalink() ?>" title="<?php echo $brand;?>">Read more about <?php echo $brand; ?> &raquo;</a></p>
			<div style="clear:both;"></div>
		</div>

<?php
		endif;
	}
}
?>

<?php
//function from iphone_include_header.php
function http_digest_parse($txt)
{
	$txt= stripslashes($txt);
	//echo $txt; exit;
	
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}
?>

<?php
//functions from photo.php
function listPhotosAll() {
	global $wpdb, $photos_per_location, $FLICKR_DATA_URL;
	echo '<h2 id="photos-page-title">Recent Tokyo Street Fashion Photos...</h2>';
 	$querystr = "
		SELECT pm.meta_value, count(*) as photo_count
		FROM $wpdb->posts p, $wpdb->postmeta pm
		WHERE p.ID = pm.post_id 
		AND p.post_type = 'post'
		AND p.post_status = 'publish'
		AND pm.meta_key = 'photo-location'
		GROUP BY pm.meta_value
		ORDER BY photo_count DESC
		";
	$photoposts = $wpdb->get_results($querystr, OBJECT); 
	if($photoposts) {
		foreach($photoposts as $post) {
			if ($post->meta_value!='')
			{
		?>
				<div class="contentpanel bm-half">
					<a class="morelink" href="<?php echo $baseurl;?>?location=<?php echo $post->meta_value;?>">Browse all <?php echo $post->meta_value;?> photos &raquo;</a>
					<h2 class="contentpaneltitle"><?php echo $post->meta_value;if ($post->meta_value=="Shinjuku"){echo " & others";}?></h2>
					<?php 
						$query = "
							SELECT p.*
							FROM $wpdb->posts p, $wpdb->postmeta pm, $wpdb->term_relationships t_r
							WHERE p.ID = pm.post_id
							AND p.post_type = 'post'
							AND p.post_status = 'publish'
							AND pm.meta_key = 'photo-location'
							AND pm.meta_value = '{$post->meta_value}'
							AND t_r.term_taxonomy_id = '711'
							AND t_r.object_id = pm.post_id
							ORDER BY p.post_date DESC
							LIMIT $photos_per_location
						";
						showLocationPhotos($query); 
                                        ?>
                                        
				</div> <!-- .contentpanel -->
		<?php
			}
		}
		?>
		<div class="contentpanel bm-half">
			<!--a target="_blank" class="morelink" href="http://www.flickr.com/photos/tokyofashion/sets/72157616624742453/">Browse More Flickr Snaps &raquo;</a-->
			<h2 class="contentpaneltitle">Most Popular Recent Street Snaps</h2>
			<?php
				$photos = get_photos_popular();
				echo '<ul class="location-photos">';
				foreach ($photos as $photo)
				{
					echo '<li>';						
						echo '<a target="_blank" title="'.$photo->title.'" href="'.$photo->link.'"><img class="nopinit" alt="'.$photo->title.'" src="'.get_bloginfo('template_directory').'/thumb.php?src='.$photo->photo_url.'&amp;w=140&amp;h=180&amp;zc=1&amp;q=95"></a>';					
					echo '</li>';					
				}
				echo '</ul>';
			?>
		</div> <!-- .contentpanel -->	
		<div class="contentpanel bm-half">
			<a target="_blank" class="morelink" href="http://www.flickr.com/photos/tokyofashion/sets/72157616624742453/">Browse More Flickr Snaps &raquo;</a>
			<h2 class="contentpaneltitle">Flickr Street Snaps</h2>
			<?php 				
				$flickr = new Flickr($FLICKR_DATA_URL);
				$photos = $flickr->get_photos($photos_per_location);
				echo '<ul class="location-photos">';
				foreach ($photos as $photo)
				{
					echo '<li>';						
						echo '<a target="_blank" title="'.$photo['title'].'" href="'.$photo['link'].'"><img class="nopinit" alt="'.$photo['title'].'" src="'.get_bloginfo('template_directory').'/thumb.php?src='.$photo['photo_url'].'&amp;w=140&amp;h=180&amp;zc=1&amp;q=95"></a>';					
					echo '</li>';					
				}
				echo '</ul>';
			?>
		</div> <!-- .contentpanel -->	
<?php
	}
}

function get_photos_popular() {
	global $wpdb;
	
	$query = "
		SELECT p.post_name as link, p.post_title as title, pm.meta_value as photo_url
		FROM wp_popularity_posts p_p
		LEFT JOIN $wpdb->posts p ON p_p.id_post = p.id
		LEFT JOIN $wpdb->postmeta pm ON p_p.id_post = pm.post_id
		WHERE pm.meta_key = 'image'
		ORDER BY p_p.total DESC
		LIMIT 12
	";
	$posts = $wpdb->get_results($query, OBJECT);
	return $posts;
}

function listPhotosSingle($location) {
	global $wpdb;
	$year = (int)$_GET['year'];
	$month = (int)$_GET['month'];
	$start = (int)$_GET['start'];
	if($start <=0){ $start=0; }
	/*$this_month = date('n');						//2011.01.10
	$this_year = date('Y');*/

	if($year <= 0)
	{
		//$year = $this_year;						//2011.01.10
		$year=0;		
	}	

	if($month <=0 || $month > 12) 
	{
		// $month = $this_month;					//2011.01.10
		$month=0;
	}
	
	if ($month>0 && $year>0)
	{
?>
	<a class="backlink" href="<?php echo bloginfo('url');?>/photos/">&laquo; Back to Recent Tokyo Street Fashion photos</a><br/><br/>
	<div class="contentpanel bm-half">
		<h2 class="contentpaneltitle"><?php echo $location;?> Street Fashion Pictures<?php if ($location=="Shinjuku") {echo ' (and other areas)';}?> - <?php echo date('F Y', mktime(0, 0, 0, $month, 1, $year));?></h2>
		<?php
			$query = "
				SELECT p.*
				FROM $wpdb->posts p, $wpdb->postmeta pm, $wpdb->term_relationships t_r
				WHERE p.ID = pm.post_id
				AND p.post_type = 'post'
				AND p.post_status = 'publish'
				AND pm.meta_key = 'photo-location'
				AND pm.meta_value = '{$location}'
				AND MONTH(p.post_date) = '$month'
				AND YEAR(p.post_date) = '$year'
				AND t_r.term_taxonomy_id = '711'
				AND t_r.object_id = pm.post_id
				ORDER BY p.post_date DESC
			";
			$status = showLocationPhotos($query);
			echo '<br style="clear:both;" />';
			if($status == FALSE) {
			?>
			<p class="snippet-intro">Sorry, there are no <?php echo $location;?> street fashion photos for <?php echo date('F Y', mktime(0, 0, 0, $month, 1, $year));?> yet, please check back later!</p><br/>
			<p class="snippet-more">Please check the main <a href="<?php echo bloginfo('url');?>/photos/" title="STREET FASHION PHOTOS">STREET FASHION PHOTOS</a> section for more pictures.</p><br/><br/>
			<?php }

			$prev_year = ($month == 1) ? $year - 1 : $year;
			$prev_month = ($month == 1) ? 12 : $month - 1;
			$next_year = ($month == 12) ? $year + 1 : $year;
			$next_month = ($month == 12) ? 1 : $month + 1;
			$this_month = date('n');
			$this_year = date('Y');		
			?>
                        <div id="photonav">
			<div class="navleft"><a href="<?php echo bloginfo('url');?>/photos/?location=<?php echo $location;?>&month=<?php echo $prev_month;?>&year=<?php echo $prev_year;?>">&laquo; <?php echo $location . ' - ' . date('F Y', mktime(0, 0, 0, $prev_month, 1, $prev_year));?></a></div>
			<?php if(($this_year > $next_year) || (($this_year == $next_year) && ($this_month >= $next_month))) { ?>
			<div class="navright"><a href="<?php echo bloginfo('url');?>/photos/?location=<?php echo $location;?>&month=<?php echo $next_month;?>&year=<?php echo $next_year;?>"><?php echo $location . ' - ' . date('F Y', mktime(0, 0, 0, $next_month, 1, $next_year));?> &raquo;</a></div>
			<?php } ?>
                        </div>
	</div> <!-- .contentpanel -->
<?php
	} else
	{
?>
		<a class="backlink" href="<?php echo bloginfo('url');?>/photos/">&laquo; Back to Recent Tokyo Street Fashion photos</a><br/><br/>
		<div class="contentpanel bm-half">
			<h2 class="contentpaneltitle"><?php echo $location;?> Street Fashion Pictures<?php if ($location=="Shinjuku") {echo ' (and other areas)';}?></h2>
			<?php
				$query = "
					SELECT p.*
					FROM $wpdb->posts p, $wpdb->postmeta pm, $wpdb->term_relationships t_r
					WHERE p.ID = pm.post_id
					AND p.post_type = 'post'
					AND p.post_status = 'publish'
					AND pm.meta_key = 'photo-location'
					AND pm.meta_value = '{$location}'
					AND t_r.term_taxonomy_id = '711'
					AND t_r.object_id = pm.post_id
					ORDER BY p.post_date DESC
					LIMIT $start, 40
				";
				
				$status = showLocationPhotos($query);
				
				echo '<br style="clear:both;" />';

				$prev_year = ($month == 1) ? $year - 1 : $year;
				$prev_month = ($month == 1) ? 12 : $month - 1;
				$next_year = ($month == 12) ? $year + 1 : $year;
				$next_month = ($month == 12) ? 1 : $month + 1;
				
				$this_month = date('n');
				$this_year = date('Y');				
				?>
				<div class="navigation">
					<? if (($status) AND ($status==40))
					{ ?>
						<div class="navleft"><a href="<?php echo bloginfo('url');?>/photos/?location=<?php echo $location;?>&start=<?php echo $start+40;?>">« Show Older <?php echo $location;?> Photos</a></div>
					<?php 
					}
					?>
					<? if ($start>0)
					{ ?>
						<div class="navright"><a href="<?php echo bloginfo('url');?>/photos/?location=<?php echo $location;?>&start=<?php echo $start-40;?>">Show Newer <?php echo $location;?> Photos »</a></div>
					<?php 
					}
					?>
				</div>
				<div id="photonav" align="center">
				<a href="<?php echo bloginfo('url');?>/photos/?location=<?php echo $location;?>&month=<?php echo $this_month;?>&year=<?php echo $this_year;?>">Browse <?php echo $location;?> Photos By Month
				</a>
							</div>
		</div> <!-- .contentpanel -->
<?php	
	}
}

function showLocationPhotos($query) {
	global $wpdb;
	global $post;
	$posts = $wpdb->get_results($query, OBJECT);
	$count = 0;
	if($posts) {
		echo '<ul class="location-photos">';
		foreach($posts as $post) {
			$count++;
			setup_postdata($post);
			showPostImage($post);
		}
		echo '</ul>';
	}
	if($count == 0) {
		return FALSE;
	}
	return $count;
}

function showPostImage($post) {
?>
<li>

    <?php    /*******************************************************************************************************************/ ?>
    <?php get_template_part( '/partials/tokyo', 'image_photo' ); ?>
    <?php    /*******************************************************************************************************************/ ?>
    
</li>
<?php
}


<?php
//file functions custom-queries.php!!!
//if (file_exists(TEMPLATEPATH . '/custom-queries.php')) {
	//require_once(TEMPLATEPATH . '/custom-queries.php');
?>
