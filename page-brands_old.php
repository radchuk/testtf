<?php
/*
Template Name: Brands
*/
$brands_count = 12;

?>
<?php get_header() ?>

	<div id="content">

		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<div class="post-content">
				<?php the_content() ?>
			</div>

			<h2 class="contentpaneltitle">Fashion Brands on TokyoFashion.com...</h2>
			<div><?php wp_tag_cloud( array( 'taxonomy' => 'brands', 'number' => 45 ) ); ?></div><br/>

			<h2 class="contentpaneltitle">Brands Recently Seen on the Streets of Tokyo...</h2>
			<ul class="overlay-photos"><?php listRecentBrands(); ?></ul>
		</div><!-- .post -->

	</div><!-- #content -->

<?php get_sidebar() ?>
<?php get_footer() ?>

<?php
function listRecentBrands() {
	global $wpdb, $brands_count;
	$querystr = "SELECT term_taxonomy_id, object_id FROM (SELECT object_id, term_taxonomy_id, count(*) as count FROM $wpdb->term_relationships JOIN $wpdb->posts ON object_id = ID WHERE term_taxonomy_id IN (SELECT term_taxonomy_id as id FROM $wpdb->term_taxonomy WHERE taxonomy = 'brands') AND post_type = 'post' AND post_status = 'publish' GROUP BY term_taxonomy_id ORDER BY count) as tmp GROUP BY object_id ORDER BY object_id DESC LIMIT $brands_count";

	$recent_brands = $wpdb->get_results($querystr, OBJECT);
	if($recent_brands) {
		foreach($recent_brands as $brand) {
			showBrandPostImage($brand->object_id, $brand->term_taxonomy_id);
		}
	}
}

// Function to show image of given post highlighting a brand
function showBrandPostImage($post_id, $brand_id) {
	global $wpdb;
	$post = get_post($post_id);
	if ( ! $brand = wp_cache_get($brand_id, 'brands_thumb') ) {
			$brand = $wpdb->get_var( $wpdb->prepare( "SELECT t.name FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = 'brands' AND tt.term_taxonomy_id = %s LIMIT 1", $brand_id) );
			wp_cache_add($brand_id, $brand, 'brands_thumb');
		}
?>
<li>
	<?php if (get_post_meta($post->ID, "image", true) != "") { ?>
	<a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo $post->post_title; ?>"><img src="<?php bloginfo('template_directory'); ?>/thumb.php?src=<?php echo get_post_meta($post->ID, "image", true); ?>&amp;w=140&amp;h=180&amp;zc=1&amp;q=95" alt="<?php echo $post->post_title; ?>" /><span><?php echo $brand;?></span></a>
	<?php } ?>
</li>
<?php
}
