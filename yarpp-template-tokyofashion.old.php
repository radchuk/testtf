<?php /*
Ttokyofashion template
Author:  PavloTyurin
*/ 
?>
<style type="text/css">
<!--
.related_title{ padding: 5px 0 0 5px; color: #000000; font-weight: bold; font-size: 120%;}
#related_block {	height: 250px; width: 605px;}
#related_block li.related_item{ display:block; float:left; width: 140px; height: 250px; padding-left: 0px; padding-right: 15px;}
#related_block li.last{ padding-left: 0px; padding-right: 0px; 
-->
</style>

<div class="contentpanel">
<h2 class="contentpaneltitle">&nbsp;</h2>
<div class="related_title">Possibly Related Articles...</div>
<?php if ($related_query->have_posts()):?>
<ul id="related_block">
	<?php $counter = 1;
	$num_related = intval(get_query_var('posts_per_page'));
	?>
	<?php while ($related_query->have_posts()) : $related_query->the_post(); ?>
	<?php 
	$fashion_images = get_post_meta(get_the_ID(), 'image'); 
	$fashion_image = $fashion_images[0];
	?>
		<li class="related_item <?php if($counter == $num_related){ echo 'last';}?>">
			<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			<img src="http://tokyofashion.com/wp-content/themes/tokyofashion/thumb.php?src=<?php echo $fashion_image; ?>&w=140&h=180&zc=1&q=95" alt="<?php the_title(); ?>" class="snippet-thumb"></a>
				<h4 class="snippet-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h4>
		</li>
	<?php $counter++; ?>	
	<?php endwhile; ?>
</ul>

<?php else: ?>
<p>No related posts.</p>
<?php endif; ?>
</div>