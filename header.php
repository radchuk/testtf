<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns#">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <meta name="generator" content="WordPress <?php bloginfo('version') ?>"/>
    <!-- Please leave for stats -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@TokyoFashion">
    <meta name="twitter:domain" content="Tokyo Fashion News">
    <meta name="twitter:creator" content="@TokyoFashion">
    <meta name="twitter:title" content="Home - Tokyo Fashion News">
    <meta name="twitter:url" content="http://clients.netweightit.com/tokyofashion2/">
    <meta name="twitter:image" content="http://simonenko.su/i/u/twitter-thumb.jpg">
    <meta name="twitter:description" content="Fashion news from Tokyo, Japan">


    <meta name="apple-itunes-app" content="app-id=398491911">
    <meta name="google-play-app" content="app-id=com.tokyofashion">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/jquery.smartbanner.css" type="text/css" media="screen">

    <title><?php bloginfo('name'); ?> - <?php if (is_single()) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>"/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Comments RSS Feed" href="<?php bloginfo('comments_rss2_url') ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.accordion.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-init.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.smartbanner.js"></script>

    <?php
    ob_start("header_for_facebook_og_image");
    wp_head();
    ob_end_flush();
    ?>

    <?php if (is_front_page()) { ?>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.jcarousel.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/jquery.jcarousel.css"/>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/jcarousel-skin.css"/>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/dropdown.css"/>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css"/>

        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jcarousel-init.js"></script>
        <meta property="fb:page_id" content="90924711751"/>
    <?php } ?>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?version=2" type="text/css" media="screen"/>

</head>

<body>

<div id="header">

    <div class="wrapper">

        <?php if (is_single()or is_page() and !is_front_page()) { ?>

            <h2 id="logo"><a href="<?php echo get_option('home'); ?>"
                             title="<?php bloginfo('name'); ?>"><span><?php bloginfo('name'); ?></span></a></h2>

        <?php
        } else {
            ?>

            <h1 id="logo"><a href="<?php echo get_option('home'); ?>"
                             title="<?php bloginfo('name'); ?>"><span><?php bloginfo('name'); ?></span></a></h1>

        <?php } ?>

        <ul id="tools">

            <li>
                <a target="_blank" href="http://twitter.com/tokyofashion">

                    <img src="<?php bloginfo('template_directory'); ?>/images/twitter_32.png" alt="Twitter" class="ico-small"/>Twitter

                </a>

            </li>

            <li>

                <a target="_blank" href="http://www.facebook.com/TokyoFashion">

                    <img src="<?php bloginfo('template_directory'); ?>/images/facebook_32.png" alt="Facebook" class="ico-small"/>Facebook

                </a>

            </li>

            <li>

                <img src="<?php bloginfo('template_directory'); ?>/images/ico_rss.png" alt="" class="ico-small"/>

                <a target="_blank" href="http://feeds.feedburner.com/TokyoFashion">RSS</a>

                <br/>

                <img src="<?php bloginfo('template_directory'); ?>/images/ico_email.png" alt="" class="ico-small"/>
                <a target="_blank" href="http://feedburner.google.com/fb/a/mailverify?uri=TokyoFashion&amp;loc=en_US">Email Updates
                </a>
            </li>

            <li>
                <?php $search_text = "Type keywords and hit Enter to search"; ?>

                <form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">

                    <input type="text" value="<?php echo $search_text; ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '<?php echo $search_text; ?>';}" onfocus="if (this.value == '<?php echo $search_text; ?>') {this.value = '';}"/>

                    <input type="hidden" id="searchsubmit"/>

                </form>

            </li>

        </ul>

        <?php if (has_nav_menu('header-menu')) {
            wp_nav_menu(array('theme_location' => 'header-menu', 'container' => 'div', 'container_id' => 'menu'));

        } ?>
    </div>

</div>
<!--  #header -->

<div class="wrapper">

    <div id="container">