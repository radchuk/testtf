<?php 

class p2p_persons_query extends WP_Query {

    var $connection  ;
    var $is_favorite ;
    var $taxonomy    ;
    var $term        ;
    var $meta_key    ;
    var $meta_value  ;
    var $query_vars  ;

    var $parser      ;

    var $query_arguments = array();

    function __construct( $args = array() ) {
        $parser = new Parse_Request( $args );

        $this->parser = $parser;

        if ( $parser->isPersonData()  ){

            if( $parser->isFavorite() ){

                $this->taxonomy = 'brands';
                $this->term     = get_term_by('slug', $parser->getFieldValue(), $this->taxonomy) ;
                $meta_key       = 'favorite_brands';

                $this->query_vars =  array(

                            'connected_type'      => 'post_to_persons',
                            'connected_items'     => 'any',
                            'connected_direction' => 'to', // needed
                            'connected_meta'      => array(array( 
                                                            'key'     => $meta_key, 
                                                            'value'   => '"' . $this->term->term_id . '"',
                                                            'compare' => 'LIKE',
                                                            'type'    => 'CHAR',
                                                     ))

                        );

                if ( $parser->isPaged() )                 
                    $this->query_vars = array_merge( $this->query_vars, array( 'page' => $parser->isPaged() ));
					
					//var_dump( $this->query_vars );

            } else {
                $compare2 = array(
                        'things' => '=',
                        'job'    => '=',
                        'sex'    => '=',
                        'age'    => '=',
                        'name'   => '='
                        );

                $type2    = array(
                        'age'    => 'numeric',
                        'sex'    => 'CHAR',
                        'job'    => 'CHAR',
                        'things' => 'CHAR',
                        'name'   => 'CHAR'
                    );


                $meta_key   = $parser->getFieldName() ;    
                $meta_value = $parser->getFieldValue();
                $meta_value = urldecode($meta_value)  ;

                $compare    = $compare2[$parser->getFieldName()] ;
                $type       = $type2[$parser->getFieldName()]    ;

                $this->query_vars =  array(

                            'connected_type'      => 'post_to_persons',
                            'connected_items'     => 'any',
                            'connected_direction' => 'to', // needed
                            'connected_meta'      => array(array( 
                                                            'key'     => $meta_key   ,
                                                            'value'   => $meta_value ,
                                                            'compare' => $compare    ,
                                                            'type'    => $type       ,
                                                     ))
                        );

                if ( $parser->isPaged() )                 
                    $this->query_vars = array_merge( $this->query_vars, array('page' => $parser->isPaged() ));

            }

            add_filter( 'posts_groupby', array( $this, 'group_by_snap' ), 10, 1);

 
        } 

/*  new query for finding brands */

        if( $parser->isBrands() ){

            $term  = get_term_by('slug', $parser->getTaxonomiesValue(), 'brands') ;

            $catID = get_category_id('Tokyo Street Snaps');

            $this->query_vars =  array(
                                    'post_type' => 'post',
                                    'tax_query' => array( array(
                                                            'taxonomy' => 'brands',
                                                            'field'    => 'id',
                                                            'terms'    => array( $term->term_id )
                                                           ) ),
                                    'cat'       => $catID
                                );

            
			//if(  0 ) {
			
			if ( $parser->isPaged() )
            {
                    //logConsole('isPaged');
                    $this->query_vars = array_merge( $this->query_vars, array( 'page' => $parser->isPaged() ));
            }
            else
            {
                //logConsole('not isPaged');
            }
			
			//}

            add_filter( 'posts_where',   array( $this, 'p2p_posts_where' ), 10, 1);

            add_filter( 'posts_groupby', array( $this, 'group_by_for_brand' ), 10, 1);
        }


/* new query for finding music */
        if( $parser->isMusic() ){

            $term  = get_term_by('slug', $parser->getTaxonomiesValue(false), 'music') ;

            $this->query_vars =  array(

                                    'post_type' => 'post',
                                    'tax_query' => array( array(
                                                            'taxonomy' => 'music',
                                                            'field'    => 'id',
                                                            'terms'    => array( $term->term_id )
                                                           ) )
                                );

            if ( $parser->isPaged() )
            {
                //logConsole('isPaged' ,  $parser->isPaged());
                $this->query_vars = array_merge( $this->query_vars, array( 'page' => $parser->isPaged() ));
            }
            else
            {
                //logConsole('No Paged' ,  $parser->isPaged());
            }


            add_filter( 'posts_where',   array( $this, 'p2p_posts_where_music' ), 10, 1);

            add_filter( 'posts_groupby', array( $this, 'group_by_post_id' ), 10, 1);
        }

        if ( $parser->isContributor() ){

			if ( $parser->isPaged() )                 
                $this->query_vars = array_merge( $this->query_vars, array( 'page' => $parser->isPaged() ));
			
            add_filter( 'posts_where',   array( $this, 'posts_contributor_where' ), 10, 1);
		
        }



        parent::__construct( $this->query_vars );

       //var_dump( $this->request );
		
    }

   function p2p_posts_where( $sql ) {
       global $wpdb;

       $term_id = get_term_by('slug', $this->parser->getFieldValue(false), 'brands');

       $get_p2p_posts =
       "SELECT $wpdb->posts.ID FROM $wpdb->posts
           INNER JOIN $wpdb->p2p
           INNER JOIN $wpdb->p2pmeta ON ($wpdb->p2p.p2p_id = $wpdb->p2pmeta.p2p_id)
               WHERE 1=1
                   AND $wpdb->posts.post_type = 'post'
                   AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')
                   AND ($wpdb->p2p.p2p_type = 'post_to_persons'
                       AND $wpdb->posts.ID = $wpdb->p2p.p2p_from
                       AND $wpdb->p2p.p2p_to IN (SELECT $wpdb->posts.ID FROM $wpdb->posts
                                                   WHERE 1=1
                                                       AND $wpdb->posts.post_type IN ('persons')
                                                       AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')
                                               )
                       )
                   AND ( ($wpdb->p2pmeta.meta_key = 'brands' AND CAST($wpdb->p2pmeta.meta_value AS CHAR) LIKE '%\"$term_id->term_id\"%') ) LIMIT 15, 12";

       return $sql . " OR $wpdb->posts.ID IN(" . $get_p2p_posts . ") ";
   }

   function p2p_posts_where_music( $sql ) {
       global $wpdb;

       $term_id = get_term_by('slug', $this->parser->getTaxonomiesValue(false), 'music');

       $get_p2p_posts =
       "SELECT $wpdb->posts.ID FROM $wpdb->posts
           INNER JOIN $wpdb->p2p
           INNER JOIN $wpdb->p2pmeta ON ($wpdb->p2p.p2p_id = $wpdb->p2pmeta.p2p_id)
               WHERE 1=1
                   AND $wpdb->posts.post_type = 'post'
                   AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')
                   AND ($wpdb->p2p.p2p_type = 'post_to_persons'
                       AND $wpdb->posts.ID = $wpdb->p2p.p2p_from
                       AND $wpdb->p2p.p2p_to IN (SELECT $wpdb->posts.ID FROM $wpdb->posts
                                                   WHERE 1=1
                                                       AND $wpdb->posts.post_type IN ('persons')
                                                       AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')
                                               )
                       )
                   AND ( ($wpdb->p2pmeta.meta_key = 'favorite_music' AND CAST($wpdb->p2pmeta.meta_value AS CHAR) LIKE '%\"$term_id->term_id\"%') )";

       return $sql . " OR $wpdb->posts.ID IN(" . $get_p2p_posts . ") ";
   }

   function group_by_snap() {
        return ' ID ';
    }

   function group_by_for_brand() {
        return ' ID ';
    }

   function group_by_post_id(){
        return ' ID ';
    }

   function posts_contributor_where( $sql ){
	
		//echo 'this is contributors filter';
		
        global $wpdb;
        $arr = $this->getConstibutorRow( $this->parser->getFieldValue() );
        $implode  = implode( ',', $arr );
		
        return $sql . ' AND wp_posts.ID IN ( ' . $implode . ' ) ';
        
    }
 
   function getConstibutorRow( $value ){
       global $wpdb;

		$value = urldecode( $value );

       $photo = get_posts( array(
                             'fields'           => 'ids',
                             'post_type'        => 'contributors',
                             'meta_query' => array(array(
                                       'key'     => 'contributor_shortname',
                                       'value'   => $value,
                                       'compare' => '=',
                                       'type'    => 'CHAR',
                                   )),

                           ) );


       $sql = " SELECT $wpdb->posts.ID
             FROM $wpdb->posts
             INNER JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
             WHERE 1=1 AND $wpdb->posts.post_status = 'publish'
                      AND ( ($wpdb->postmeta.meta_key LIKE %s AND $wpdb->postmeta.meta_value = %s  ) )
             GROUP BY $wpdb->posts.ID ";

       $rows = $wpdb->get_results($wpdb->prepare( $sql, 'contributors_%_contributor', $photo[0]  ), ARRAY_N );

       $ids = array();

       foreach ($rows as $value) {

           $ids[] = $value[0];

       }


       return $ids;
   }

   function remove_filters() {
        if( has_filter( 'posts_where', array( $this, 'p2p_posts_where' )) )
            remove_filter( 'posts_where', array( $this, 'p2p_posts_where' ));
    }

   function add_filters(){
        add_filter( 'posts_where', array( $this, 'p2p_posts_where' ));
    }

   function get_term_id( $value, $taxonomy ){

       global $wpdb;

       $decoded1 = htmlentities( $this->parser->getFieldValue() );

       $taxonomy = 'brands';
       $field    = 't.slug';
       $value    = $decoded1;

       $term     = $wpdb->get_row( $wpdb->prepare( "SELECT t.*, tt.* FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = %s AND $field = %s LIMIT 1", $taxonomy, $value) );

       if( isset( $term->term_id ) )
           return $term->term_id;

       return false;

   }

}




class Parse_Request {

    /**
    * can be 
    */
    var $types;
    var $current_type;
    var $nessesary_fields;
    var $wp_query_args;
    var $args;


    function __construct( $args = array() ) {

        $this->init( $args );

    }
    function isPaged(){
        return isset( $this->wp_query_args['paged'] ) ? $this->wp_query_args['paged'] : false ;
    }

    function setCurrentType( $name ){

        if ( !isset( $this->types ) )
            return new WP_Error('Types', __( "Type are not defined" ));

        foreach ($this->types as $key => $value) {
            //if something be in_array $value -> set type and break loop
            if( in_array( $name, $value ) ){
                $this->current_type = $key;
                break;
            }
        }

        if ( empty( $this->current_type ) )
            return new WP_Error('Current Time', __( "Current type are not defined" ));
    }

    function getType(){
        return $this->current_type;
    }

    function init( $args ){

        $this->setDefaultTypes();
        $this->setNessesaryFields();
        $this->setNecessaryVariables( $args );

        if( $this->isTaxonomy() ){
            $type = $this->isBrands() ? 'brands' : 'music';
        } else {
            $type = isset( $args['field_name'] ) ?  $args['field_name']  : false;
        }
        $this->setCurrentType( $type );

    }

    function setDefaultTypes(){

        $this->types = array(
                    'favorite-brands'  => array( 'favorite_brands' ),
					//'favorite-brands'  => array( 'favorite-brands' ),
                    // 'music'            => array( 'music' ),
                    'photographer'     => array( 'photographer' ),
                    'hunter'           => array( 'hunter' ),
                    'author'           => array( 'author' ),
                    'person'           => array( 'age', 'gender', 'name', 'job', 'things' ),
                    'brands'           => array( 'brands' ),
                    'music'            => array( 'music' ),
                );

    }

    function setNessesaryFields(){

        $this->nessesary_fields = array(
                'find',
                'field_name',
                'field_value',
                'paged',
                'page',
                'posts_per_page',
                'brands',
                'music'
            );    
         
    }

    function getNessesary(){
        return $this->nessesary_fields;
    }

    function isFavorite(){
        return in_array( $this->current_type, array( 'favorite-brands' ) );
    }

    function isPersonData(){
        return in_array( $this->current_type, array( 'favorite-brands', 'person' ) );
    }

    function isBrands(){
        return isset($this->wp_query_args['brands']);
    }

    function isMusic(){
        return isset($this->wp_query_args['music']);
    }

    function isTaxonomy(){
        return $this->isBrands() || $this->isMusic();
    }

    function isContributor(){
        return in_array( $this->current_type, array( 'author', 'photographer', 'hunter' ) );        
    }

    function isType( $type ){
        return $this->current_type === $type;
    }

    function setNecessaryVariables( $args ){

        foreach ($args as $key => $value) {
            if( is_string( $value ) ){

                if( $key == 'field_value' ){ $args[$key] = $value ;    

                } else { $args[$key] = urldecode( $value ); }
            }

            if( !in_array( $key, $this->getNessesary() ) )
                unset( $args[ $key ] );
        }

        $this->wp_query_args = $args;

    }

    /* if true - return field_value. */
    function getFieldValue( $flag = true ){
        //if true - return field_value.
        return ( $flag ) ? $this->wp_query_args['field_value'] : $this->wp_query_args['brands'] ;
    }

    /*test this function for all fields*/
    function getFieldName( $flag = true ){
        //if true - return field_name.
        return ( $flag ) ? $this->wp_query_args['field_name'] : 'brands' ;
    }

    function getTaxonomiesValue( $flag = true ){
        //if true - return taxonomy name
        return ( $flag ) ? $this->wp_query_args['brands'] : $this->wp_query_args['music'];
    }

}