<?php
/*
Template Name: Brands A-Z
*/
?>
<?php get_header() ?>

<?php global $post; ?>


    <div id="content">

        <h2 class="contentpaneltitle">A-Z BRANDS</h2>

        <?php
        $records = get_terms('brands');

        $lastChar = '';

        $arrr = array();

        foreach ($records as $val => $key2) {
            $arr = array('name' => $key2->name, 'termId' => $key2->term_id);

            $char = mb_substr($arr['name'], 0, 1, "UTF-8");

            $arrr[$char][] = array('name' => $arr['name'], 'termId' => $key2->term_id);
        }

        foreach ($arrr as $key => $letter) {
            ?>

            <div class="letter-row">

                <h3 class="blocksubtitle"><?php echo $key; ?></h3>

                <?php $counter = 0; ?>

                <?php foreach ($letter as $keys => $brand) { ?>

                    <?php $term_link = get_term_link((int)$brand['termId'], 'brands'); ?> 

                    <?php if ($counter <= 23) : ?>

                        <?php if ((($counter+1)/8) <= 0) : ?>

                            <?php if ($counter == 0) : ?>

                            <div class="brandblock">
                                
                                <ul id="brandlist"> 
                                    <li>
                                        <a href="<?php echo $term_link; ?>"><?php echo $brand['name'] ?></a>
                                    </li>

                            <?php endif; ?>

                            <?php if ($counter == 7) : ?>

                                </ul>

                            <?php endif; ?>

                            <?php if ($counter == 8) : ?>

                                <ul id="brandlist"> 
                                    <li>
                                        <a href="<?php echo $term_link; ?>"><?php echo $brand['name'] ?></a>
                                    </li>

                            <?php endif; ?>

                            <?php if ($counter == 15) : ?>

                                </ul>

                            <?php endif; ?>


                            <?php if ($counter == 16) : ?>
                                <ul id="brandlist"> 
                                    <li>
                                        <a href="<?php echo $term_link; ?>"><?php echo $brand['name'] ?></a>
                                    </li>

                            <?php endif; ?>

                            <?php if ($counter == 23) : ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                        <?php endif; ?>

                    <?php else : ?>
                    <?php $counter ++;?> 
                    <?php endif; ?>



                <?php } ?>

            </div>
        <?php } ?>
    </div>

<?php get_sidebar() ?>

<?php get_footer() ?>





