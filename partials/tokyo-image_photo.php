
<?php //var_dump( get_url_image() ); ?>

<?php if (get_post_meta($post->ID, "image", true) != "" || has_post_thumbnail( $post->ID ) ) { 

	$url= get_url_image($post->ID);
	tf_bfi_image($url, 140, 180);

} else { ?>

    <a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo $post->post_title; ?>">
    	<img class="nopinit" src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php echo $post->post_title; ?>" />
    </a>

<?php } ?>
