<?php
if(has_post_thumbnail( $post->ID )) { 

    $url= get_url_image($post->ID);
	tf_bfi_image($url, 140, 180,'alignright' );
}

elseif (get_post_meta($post->ID, "image", true) != "") { ?>

  <?php $get_url_image  =   get_field('image',$post->ID) ;
   
	tf_bfi_image($get_url_image, 140, 180,'alignright' );

}?>

