<?php
if(has_post_thumbnail( $post->ID )) { 

    $url= get_url_image($post->ID);
	tf_bfi_image($url, 140, 180);
}

elseif (get_post_meta($post->ID, "image", true) != "") { ?>

  <?php $get_url_image  =   get_field('image',$post->ID) ;
   
	tf_bfi_image($get_url_image, 140, 180);

} else { ?>

    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
    	<img src="<?php bloginfo('template_directory'); ?>/images/nothumb_small.gif" alt="<?php the_title(); ?>" class="snippet-thumb" />
    </a>
    
<?php } ?>

