<?php

	function truncate($string, $del) {
		$len = strlen($string);
		if ($len > $del) {
			$new = substr($string, 0, $del)."...";
			return $new;
		}
		else return $string;
	}

	remove_filter('the_excerpt', 'wpautop');
	
	if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '<div class="sidepanel">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

// Changes to add Brands
add_action( 'init', 'create_my_taxonomies', 0 );
function create_my_taxonomies() {
    register_taxonomy( 'brands', 'post', array( 'hierarchical' => false, 'label' => 'Brands5555555555', 'query_var' => true, 'rewrite' => true ) );
}
add_filter('manage_posts_columns', 'add_brands_column');

function add_brands_column($post_columns) {
    $post_columns['brands'] = __('Brands');
    return $post_columns;
}

add_action('manage_posts_custom_column', 'my_brands_column');

function my_brands_column($attr) {
    global $post;
    if($attr == 'brands') {
        $brands = get_the_terms($post->ID, 'brands');
        if ( !empty( $brands ) ) {
            $out = array();
            foreach ( $brands as $c )
                $out[] = "<a href='edit.php?brands=$c->slug'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'brands', 'display')) . "</a>";
            echo join( ', ', $out );
        } else {
            _e('No Brands');
        }
    }
}

add_action('admin_head', 'js_for_brands');

// Function to add javascript to replace the text "Add new tag" under Brands post meta box to "Add new brand"
function js_for_brands() {
?>
    <script type="text/javascript">
        jQuery(document).ready( function($) {

            // Change "Separate tags with commas." to "Separate brands with commas." under brands
            jQuery('#tagsdiv-brands').find('.howto').html('Separate brands with commas.');
            // Change "Choose from the most used tags in Brands" to "Choose from the most used Brands" under brands
            jQuery('#tagsdiv-brands').find('#link-brands').html('Choose from the most used Brands');

            function update_input_tips_to_brand() {
                var brandsbox;
                brandsbox = jQuery('#tagsdiv-brands');

                // is the input box empty (i.e. showing the 'Add new tag' tip)?
                if ( brandsbox.find('input.newtag').hasClass('form-input-tip') ) {
                    brandsbox.find('input.newtag').val("Add new brand");
                }
            }

            function update_input_tips_to_brand_var_focus() {
                postL10n.addTag = 'Add new tag';
            }
            function update_input_tips_to_brand_var_blur() {
                postL10n.addTag = 'Add new brand';
            }
            jQuery('#new-tag-post_tag').focus(function() { update_input_tips_to_brand_var_focus(); });
            jQuery('#new-tag-brands').blur(function() { update_input_tips_to_brand_var_blur(); });
            update_input_tips_to_brand();
        });
    </script>
<?php
}

// Changes to have custom title on Brands page. eg. "Vivienne Westwood - Brands - TokyoFashion.com"
// Implemented because of interference by All in One SEO and the same is used as reference to create this

//add_action('template_redirect', 'replace_title_for_brands_start', 0);
function replace_title_for_brands_start() {
    global $wp_query;
    if($wp_query->query_vars['taxonomy'] != 'brands') { return; }
    ob_start(replace_title_for_brands);
}

function replace_title_for_brands($content) {
    global $wp_query;

    $title = $wp_query->queried_object->name . " - Brands - TokyoFashion.com";
    $title = trim(strip_tags($title));

    $title_tag_start = "<title>";
    $title_tag_end = "</title>";
    $len_start = strlen($title_tag_start);
    $len_end = strlen($title_tag_end);
    $title = stripcslashes(trim($title));
    $start = strpos($content, $title_tag_start);
    $end = strpos($content, $title_tag_end);

    if ($start && $end) {
        $header = substr($content, 0, $start + $len_start) . $title .  substr($content, $end);
    } else {
        $header = $content;
    }

    return $header;
}
?>

