<?php
/*
Template Name: Mail Chimp
*/
?>
<?php get_header() ?>

    <div id="content">

        <?php the_post() ?>

        <div id="post-<?php the_ID(); ?>" class="post">

            <h1 class="post-title"><?php the_title(); ?></h1>

            <div class="post-content">

                <?php the_content() ?>
                <br /><br /><br />

                <?php dynamic_sidebar('mail_chimp_tokyo'); ?>

            </div>

        </div>

    </div>

<?php get_footer() ?>