<?php 
function movingBrandTags(){
    $arr = array(2342, 11946, 15960, 16837, 8754, 11282, 7482, 11289, 23888, 11495, 9562, 11372, 23696, 18723, 51304, 56627, 60164, 59667, 61504, 72523);
    $taxonomy = "post_tag";
    $array_term_id = array();
    foreach ($arr as $post_id) {
        foreach ( wp_get_post_terms( $post_id, $taxonomy ) as $value ) {
            wp_insert_term($value->name, "tags");
        }            
    }  
}

function removeAllTags(){

    $terms = get_terms( 'tags', array('hide_empty'    => 0) );

    foreach ($terms as $key => $value) {

        wp_delete_term( $value->term_id, 'tags');  
    }

}

function get_post_by_title($page_title, $post_type ='post' , $output = OBJECT ) {
    global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));

        //if( !empty( $fields ) )


        if ( $post )
            return get_post($post, $output);


        $sql = $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name LIKE %s AND post_type= %s", '%' . $page_title . '%', $post_type);

        $post = $wpdb->get_var( $sql );

        if ( $post )
            return get_post($post, $output);

    return null;
}

function bulkMover(){

    $brandsPosts = array( 
                          2342, 11946, 15960, 16837, 8754,
                          11282, 7482, 11289, 23888, 11495,
                          9562, 11372, 23696, 18723, 51304,
                          56627, 60164, 59667, 61504, 72523
                        );


    $taxonomy = array('post_tag','brands');



    foreach ($brandsPosts as $postID) {

        
            $title = get_the_title( $postID );

            $customBrandPost = get_post_by_title( $title, 'brands' );

            var_dump( $title );
            var_dump( $customBrandPost->post_title );
            echo '<br />';


        // foreach ( wp_get_post_terms( $postID, $taxonomy ) as $value ) {
            
        //     if( $value->taxonomy == 'post_tag' ){

        //         // change taxonomy to tags
        //     }


        //     if( $value->taxonomy == 'brands' ){
        //         // change taxonomy to brand-category
        //     }            

        //     //find name by title

        

        // }

    }


}

function ACF_empty_fields_filter($var){

    if( !empty( $var ) ){

        if( is_array( $var ) ){
            return array_filter($var, "ACF_empty_fields_filter");    
        } else {
            return $var;
        }
        
    }

}

function logConsole($name, $data = NULL, $jsEval = FALSE)
{

  if (! $name) return false;

  $isevaled = false;
  $type = ($data || gettype($data)) ? 'Type: ' . gettype($data) : '';

  if ($jsEval && (is_array($data) || is_object($data)))
  {
       $data = 'eval(' . preg_replace('#[\s\r\n\t\0\x0B]+#', '', json_encode($data)) . ')';
       $isevaled = true;
  }
  else
  {
       $data = json_encode($data);
  }

  # sanitalize
  $data = $data ? $data : '';
  $search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
  $replace_array = array('"', '', '', '\\n', '\\n');
  $data = preg_replace($search_array,  $replace_array, $data);
  $data = ltrim(rtrim($data, '"'), '"');
  $data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

$js = <<<JSCODE
\n<script>
     // fallback - to deal with IE (or browsers that don't have console)
     if (! window.console) console = {};
     console.log = console.log || function(name, data){};
     // end of fallback

     console.log('$name');
     console.log('------------------------------------------');
     console.log('$type');
     console.log($data);
     console.log('\\n');
</script>
JSCODE;

          echo $js;

} 

// if( function_exists('pre_print') ) {

// function pre_print( $data ){

//   $data = objectToArray($data);
//   // logConsole('pre_print',  objectToArray($data) );
//   logConsole('pre_print', (object) $data);

// $js = <<<JSCODE
// \n<script>
//      // fallback - to deal with IE (or browsers that don't have console)
//      if (! window.console) console = {};
//      console.log = console.log || function(name, data){};
//      // end of fallback
//      console.log('------------------------------------------');
//      console.log( JSON.stringify( $data , null, '\\t') );
//      //console.log( JSON.stringify( { uno: 1, dos : 2 }, null, '\\t') );
//      //console.log($data);
//      console.log('\\n');
// </script>
// JSCODE;

//    logger( $data, $js );
  
//   }
// }




function objectToArray($d) {
    if (is_object($d)) {
      // Gets the properties of the given object
      // with get_object_vars function
      $d = get_object_vars($d);
    }
 
    if (is_array($d)) {
      /*
      * Return array converted to object
      * Using __FUNCTION__ (Magic constant)
      * for recursive call
      */
      return array_map(__FUNCTION__, $d);
    }
    else {
      // Return array
      return $d;
    }
  }

function html_print( $data ){

$selector = '$('  . $data . ').context.outerHTML';

$js = <<<JSCODE
\n<script>
     // fallback - to deal with IE (or browsers that don't have console)
     if (! window.console) console = {};
     console.log = console.log || function(name, data){};
     // end of fallback

     console.dirxml( $data );
     console.log( $selector );

</script>
JSCODE;

   logger( $data, $js );
}


function group_test( $data ){

$trace = getBacktrace();

$js = <<<JSCODE
\n<script>
     // fallback - to deal with IE (or browsers that don't have console)
     if (! window.console) console = {};
     console.log = console.log || function(name, data){};
     // end of fallback
     groupCollapsed( "Developing '%s'", $trace );
     console.log( $data );
     console.groupEnd();

</script>
JSCODE;

  logger( $data, $js );
  
}

function logger($data, $js, $jsEval = FALSE, $name = FALSE ){

  // if (! $name) return false;

  $isevaled = false;
  $type = ($data || gettype($data)) ? 'Type: ' . gettype($data) : '';

  if ($jsEval && (is_array($data) || is_object($data)))
  {
       $data = 'eval(' . preg_replace('#[\s\r\n\t\0\x0B]+#', '', json_encode($data)) . ')';
       $isevaled = true;
  }
  else
  {
       $data = json_encode($data);
  }

  # sanitalize
  $data = $data ? $data : '';
  $search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
  $replace_array = array('"', '', '', '\\n', '\\n');
  $data = preg_replace($search_array,  $replace_array, $data);
  $data = ltrim(rtrim($data, '"'), '"');
  $data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

  echo $js;

}



function printAllConnectedData( $post_id ){
     // $post_id = get_the_ID();
     $acf = get_fields( $post_id );

     $connectedPersons = get_posts( array(
                                          // 'connected_type'   => array('post_to_p'),
                                          'connected_type'   => array('post_to_persons'),
                                          'connected_items'  => $post_id,
                                          'nopaging'         => true,
                                          // 'connected_directions' => 'to'
                                   ) );

//     $connectedClothes = get_posts( array(
//                                          // 'connected_type'   => array('post_to_c'),
//                                          'connected_type'   => array('post_to_clothes'),
//                                          'connected_items'  => $post_id,
//                                          'nopaging'         => true,
//                                          // 'connected_directions' => 'to'
//                                   ) );
//
//
//     echo '------------------------------------<br />';
//     echo '<pre>';
//     var_dump( $acf );
//     echo '-----------------------------persons<br />';
//
//
//     foreach ($connectedPersons as $key => $value) {
//          echo '<br />';
//          var_dump( $value->post_title );
//
//     }
//     echo '-----------------------------clothes<br />';

     // foreach ($connectedClothes as $key => $value2) {
          
     //      var_dump( $value2->post_title );
     //      echo ' **** <br />';
     //      var_dump( p2p_get_meta( $value2->p2p_id, 'brands', true ) );

     // }
     // echo '-----------------------------------<br />';

     
}


/**
* print all connected meta for post_to_persons
*/
function printAllConnectedPersonMetadata( $post_id ){
     // $post_id = get_the_ID();
     // $acf = get_fields( $post_id );

     global $post; 
     $personMeta = array( 'date'   ,
                          'age'    ,
                          'sex'    ,
                          'job'    ,
                          'things' ,
                          'brands' ,
                          'music'  , 
                         );


     $connectedPersons = new WP_Query( array(
                                          'connected_type'   => array('post_to_persons'),
                                          'connected_items'  => $post_id,
                                          'nopaging'         => true,
                                   ) );




    // $connectedClothes = get_posts( array(
    //                                       'connected_type'   => array('post_to_clothes'),
    //                                       'connected_items'  => $post_id,
    //                                       'nopaging'         => true,
    //                                ) );



    // var_dump( $connectedClothes );

    while( $connectedPersons->have_posts() ){ $connectedPersons->the_post();
          echo '<br />';
          the_title();
          echo  '<br/>'; 

          $p2p_id = $post->p2p_id;

          foreach ($personMeta as  $value) {
              
                $meta = p2p_get_meta( $p2p_id, $value, true );

                if( $meta ){

                  if( !is_array( $meta ) ){

                    echo  '<br/>' . $value . ' &nbsp;&nbsp; ' . $meta . '<br/>'; 

                  } else {

                    echo  '<br/>' . $value . ' &nbsp;&nbsp; ';

                    var_dump( $value );

                    foreach ($meta as $key => $item) {

                      echo  '<br/> &nbsp;&nbsp;&nbsp; ' . $key . ' --- ' . $item . '<br/>';   

                    }
                    
                  }

                }

            }  

     }

     wp_reset_postdata();

    echo '<br /-----------------------------------<br />';

//    foreach ($connectedClothes as $key => $value2) {
//          echo ' <br /**** <br />';
//          var_dump( $value2->post_title );
//          echo ' <br /> meta  <br />';
//          var_dump( p2p_get_meta( $value2->p2p_id, 'brands', true ) );
//          echo '<br /-----------------------------------<br />';
//     }
     
     wp_reset_postdata();

     
}

/*
** <?php get_module('catalogue_query'); ?>
*/
/*
function get_module( $name = null ) {
    do_action( 'get_module', $name );
 
    $templates = array();
    if ( isset($name) )
        $templates[] = "modules/module-{$name}.php";
 
    $templates[] = "modules/module.php";
    // Backward compatible code will be removed in a future release
    if ('' == locate_template($templates, true))
        load_template();
}
 
add_action('get_template_part', 'get_module');*/


function testPostToPersonCreator(){

//    p2p_type( 'post_to_persons' )->connect( 76786, 76142, 
//      array(
//        'meta' => array( 'date' => current_time('mysql'), ),
//             'age'  => 18,
//               'sex'    => 'male',
//                  'job'    => 'MC`Donalds',
//                  'things' => 'some things',
//                  'brands' => array( 6778 ) ,
//                  'music'  => array( 4386 ) , 
//                  'social_links' => array( 'vk' => 'vk.com/athedon', 'fb' => 'fb.com/athedon', 'tw' => 'tw.com/athedon'),
  
//        ) );


//  p2p_type( 'post_to_persons' )->connect( 76786, 76120, 
// array(
//  'meta' => array( 'date' => current_time('mysql'), ),
//   'age'  => 18,
//       'sex'    => 'female',
//      'job'    => 'MC`Donalds',
//      'things' => 'some thin4gs',
//      'brands' => array(  5603, 6778 ) ,
//      'music'  => array( 1006 ) , 
//      'social_links' => array( 'vk' => 'vk.com/1athedon', 'fb' => 'fb.com/1athedon', 'tw' => 'tw.com/1athedon'),

//  ) );

//  p2p_type( 'post_to_persons' )->connect( 76786, 76129, 
// array(
//  'meta' => array( 'date' => current_time('mysql'), ),
//   'age'  => 18,
//       'sex'    => 'male',
//      'job'    => 'MC`Donald2s',
//      'things' => 'some things2',
//      'brands' => array( 7377 ) ,
//      'music'  => array( 4386 ) , 
//      'social_links' => array( 'vk' => 'vk.com/2athedon', 'fb' => 'fb.com/2athedon', 'tw' => 'tw.com/2athedon'),

//  ) );




//  p2p_type( 'post_to_persons' )->connect( 81179, 76129, 
// array(
//  'meta' => array( 'date' => current_time('mysql'), ),
//   'age'  => 18,
//       'sex'    => 'male',
//      'job'    => 'MC`Donald2s',
//      'things' => 'some things2',
//      'brands' => array( 7377 ) ,
//      'music'  => array( 4386 ) , 
//      'social_links' => array( 'vk' => 'vk.com/2athedon', 'fb' => 'fb.com/2athedon', 'tw' => 'tw.com/2athedon'),

//  ) );

//   p2p_type( 'post_to_persons' )->connect( 81179, 76120, 
// array(
//  'meta' => array( 'date' => current_time('mysql'), ),
//   'age'  => 18,
//       'sex'    => 'male',
//      'job'    => 'MC`Donald2s',
//      'things' => 'some things2',
//      'brands' => array( 7377 ) ,
//      'music'  => array( 4386 ) , 
//      'social_links' => array( 'vk' => 'vk.com/2athedon', 'fb' => 'fb.com/2athedon', 'tw' => 'tw.com/2athedon'),

//  ) );

 //--------------------------------


  p2p_type( 'post_to_persons' )->connect( 74489, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'male',
     'job'    => 'MC`Donald2s',
     'things' => 'some things2',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon', 'fb' => 'fb.com/2athedon', 'tw' => 'tw.com/2athedon'),

 ) );

  p2p_type( 'post_to_persons' )->connect( 74447, 76129, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );


  p2p_type( 'post_to_persons' )->connect( 74275, 76129, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );


  p2p_type( 'post_to_persons' )->connect( 74153, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );

    p2p_type( 'post_to_persons' )->connect( 73873, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );


     p2p_type( 'post_to_persons' )->connect( 73829, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );

         p2p_type( 'post_to_persons' )->connect( 73626, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );


        p2p_type( 'post_to_persons' )->connect( 72958, 76120, 
array(
 'meta' => array( 'date' => current_time('mysql'), ),
  'age'  => 18,
      'sex'    => 'female',
     'job'    => 'Mainbank',
     'things' => 'any',
     'brands' => array( 7377 ) ,
     'music'  => array( 4386 ) , 
     'social_links' => array( 'vk' => 'vk.com/2athedon4', 'fb' => 'fb.com/2athedosn', 'tw' => 'tw.com/2athedon'),

 ) );


// 73873

// 74489

// 74447

// 74275

// 74153

// 81179

// 76142

// 76120

// 76129

// 74409

}
//
//function testClothesToPersonCreator(){
//
//
//
//   p2p_type( 'post_to_clothes' )->connect( 76786, 76506,
//     array(
//         'meta'   => array( 'date' => current_time('mysql'), ),
//         'brands' => array( 6778, 7377, 5603 ),
//         'name'   => 'boots',
//
//       ) );
//
//
// p2p_type( 'post_to_clothes' )->connect( 76786, 76126,
//     array(
//         'meta'   => array( 'date' => current_time('mysql'), ),
//         'brands' => array( 7377, 5603 ),
//         'name'   => 'military-boots',
//
//       ) );
//
//
//    p2p_type( 'post_to_clothes' )->connect( 76786, 75689,
//     array(
//         'meta'   => array( 'date' => current_time('mysql'), ),
//         'brands' => array( 5603 ),
//         'name'   => 'boots'
//       ) );
//
//
//
// p2p_type( 'post_to_clothes' )->connect( 81179, 76126,
//     array(
//         'meta'   => array( 'date' => current_time('mysql'), ),
//         'brands' => array( 7377, 5603 ),
//         'name'   => 'shoes'
//       ) );
//
//
//    p2p_type( 'post_to_clothes' )->connect( 81179, 75689,
//     array(
//         'meta'   => array( 'date' => current_time('mysql'), ),
//         'brands' => array( 5603 ),
//         'name'   => 'boots'
//       ) );
//
//
//
//
//}


/** 
 * Getting backtrace 
 * 
 * @param int $ignore ignore calls 
 * 
 * @return string 
 */ 
// function getBacktrace($ignore = 2) 
// { 
//     $trace = ''; 
//     foreach (debug_backtrace() as $k => $v) { 
//         if ($k < $ignore) { 
//             continue; 
//         } 

//         array_walk($v['args'], walker_2(&$item, $key) ); 

//         $trace .= '#' . ($k - $ignore) . ' ' . $v['file'] . '(' . $v['line'] . '): ' . (isset($v['class']) ? $v['class'] . '->' : '') . $v['function'] . '(' . implode(', ', $v['args']) . ')' . "\n"; 
//     } 

//     return $trace; 
// } 

// function walker_2($item, $key){
//   $item = var_export($item, true); 
// }


function wp_tag_cloud_custom( $args = '' ) {
	$defaults = array(
		'smallest' => 8, 'largest' => 22, 'unit' => 'pt', 'number' => 45,
		'format' => 'flat', 'separator' => "\n", 'orderby' => 'name', 'order' => 'ASC',
		'exclude' => '', 'include' => '', 'link' => 'view', 'taxonomy' => 'post_tag', 'echo' => true
	);
	$args = wp_parse_args( $args, $defaults );

	$tags = get_terms( $args['taxonomy'], array_merge( $args, array( 'orderby' => 'count', 'order' => 'DESC' ) ) ); // Always query top tags

	if ( empty( $tags ) || is_wp_error( $tags ) )
		return;
        
       //echo "<pre>"; print_r($tags); echo"</pre>"; 
        // get tags from new type of posts
        $additional_tags = get_additional_tags($args['taxonomy']);
       //echo "<pre>"; print_r($additional_tags); echo"</pre>";
        if ($additional_tags) {
            foreach ($tags as $key => $tag) {
                if (array_key_exists($tag->term_id, $additional_tags)) {
                    $tags[$key]->count += $additional_tags[$tag->term_id]->count;
                    unset($additional_tags[$tag->term_id]);
                }
            }
            array_filter($additional_tags);
            if ($additional_tags)
                $tags = array_merge($tags, $additional_tags);
 
            usort($tags, "tags_count_cmp");
            //echo "<pre>"; print_r($tags); echo"</pre>";
            
            if (count($tags) > $args['number']) {
                $tags = array_slice ($tags, 0, $args['number']);
            }
            
            //echo "<pre>"; print_r($tags); echo"</pre>";
        }
        
        
	foreach ( $tags as $key => $tag ) {
		if ( 'edit' == $args['link'] )
			$link = get_edit_tag_link( $tag->term_id, $tag->taxonomy );
		else
			$link = get_term_link( intval($tag->term_id), $tag->taxonomy );
		if ( is_wp_error( $link ) )
			return false;

		$tags[ $key ]->link = $link;
		$tags[ $key ]->id = $tag->term_id;
	}

        
	$return = wp_generate_tag_cloud( $tags, $args ); // Here's where those top tags get sorted according to $args

	$return = apply_filters( 'wp_tag_cloud', $return, $args );

	if ( 'array' == $args['format'] || empty($args['echo']) )
		return $return;

	echo $return;
}

function get_additional_tags($taxonomy) {
    if ($taxonomy == 'music' )
        $metaKey = 'favorite_music';
    else
        $metaKey = $taxonomy;
    
    $additionalTags = array();
    $args = array(
        'post_type' => 'post',
        'fields'    => 'ids',
    );

    
    //$firstQuery = new WP_Query($args);
    $firstQuery = get_posts($args);
    
    $i = 0;
    foreach($firstQuery as $fq){
        $connected = new WP_Query( array(
            'connected_type'  => 'post_to_persons',
            'connected_items' => get_post( $fq ),
            'nopaging'        => true,
            ) );
        
        while ($connected->have_posts()) : $connected->the_post();
            global $post;
            
            $taxonomyTerms = p2p_get_meta( $post->p2p_id, $metaKey, true ) ;
            
            
            $terms_ids   = array_filter(array_unique(array_values_recursive( (array)$taxonomyTerms )));
            
           
           
            //print_r($terms_ids);
            
            foreach ($terms_ids as $term_id) {
                if (array_key_exists($term_id, $additionalTags)) {
                    $additionalTags[$term_id]->count++;
                }
                else {
                    $additionalTags[$term_id] = get_term_by( 'id', $term_id, $taxonomy );    
                    $additionalTags[$term_id]->count = 1;
                }
            }
            
        endwhile;
        wp_reset_postdata();
        $i++;
    }
    return $additionalTags;
}

function tags_count_cmp($a, $b) {
    if ($a->count == $b->count) {
        return 0;
    }
    return ($a->count < $b->count) ? 1 : -1;
}