<?php
/***********************************************************************************************
                                        REGISTRATION POST TYPE
************************************************************************************************/
//Brand Articles
$brand_articles_labels = array (
    'name'               => _x( 'Brand Articles',                   'Brand Articles' ),
    'singular_name'      => _x( 'Brand Article',                    'Brand Articles' ),
    'menu_name'          => _x( 'Brand Articles',                   'Brand Articles' ),
    'add_new'            => _x( 'Add Brand Article',                'Brand Articles' ),
    'add_new_item'       => __( 'Add New Brand Article',            'Brand Articles' ),
    'edit'               => __( 'Edit',                             'Brand Articles' ),
    'edit_item'          => __( 'Edit Brand Article',               'Brand Articles' ),
    'new_item'           => __( 'New Brand Article',                'Brand Articles' ),
    'view'               => __( 'View Brand Article',               'Brand Articles' ),
    'view_item'          => __( 'View Brand Article',               'Brand Articles' ),
    'search_items'       => __( 'Search Brand Article',             'Brand Articles' ),
    'not_found'          => __( 'No Brand Articles Found',          'Brand Articles' ),
    'not_found_in_trash' => __( 'No Brand Articles Found in Trash', 'Brand Articles' ),
    'parent'             => __( 'Parent Brand Article',             'Brand Articles' ),
);

$brand_articles_supports = array(
    'title'          ,
    'editor'         ,
    'excerpt'        ,
    'trackbacks'     ,
    'custom-fields'  ,
    'comments'       ,
    'revisions'      ,
    'thumbnail'      ,
    'author'         ,
    'page-attributes',
);

$brand_articles_args = array( 
    'label'               => 'Brand Articles'        ,
    'description'         => ''                      ,
    'public'              => true                    ,
    'show_ui'             => true                    ,
    'show_in_menu'        => true                    ,
    'capability_type'     => 'post'                  ,
    'hierarchical'        => false                   ,
    'rewrite'             => array('slug' => '')     ,
    'query_var'           => true                    ,
    'has_archive'         => true                    ,
    'exclude_from_search' => false                   ,
    'supports'            => $brand_articles_supports,
    'labels'              => $brand_articles_labels  ,
);

register_post_type( 'brand_articles', $brand_articles_args );

//
////Clothes
//$clothes_labels = array (
//    'name'               => _x( 'Clothes',                   'Clothes' ),
//    'singular_name'      => _x( 'Clothes',                   'Clothes' ),
//    'menu_name'          => _x( 'Clothes',                   'Clothes' ),
//    'add_new'            => _x( 'Add Clothes',               'Clothes' ),
//    'add_new_item'       => __( 'Add New Clothes',           'Clothes' ),
//    'edit'               => __( 'Edit',                      'Clothes' ),
//    'edit_item'          => __( 'Edit Clothes',              'Clothes' ),
//    'new_item'           => __( 'New Clothes',               'Clothes' ),
//    'view'               => __( 'View Clothes',              'Clothes' ),
//    'view_item'          => __( 'View Clothes',              'Clothes' ),
//    'search_items'       => __( 'Search Clothes',            'Clothes' ),
//    'not_found'          => __( 'No Clothes Found',          'Clothes' ),
//    'not_found_in_trash' => __( 'No Clothes Found in Trash', 'Clothes' ),
//    'parent'             => __( 'Parent Clothes',            'Clothes' ),
//
//);
//
//$clothes_supports = array(
//    'title'          ,
//    'editor'         ,
//    'excerpt'        ,
//    'trackbacks'     ,
//    'custom-fields'  ,
//    'comments'       ,
//    'revisions'      ,
//    'thumbnail'      ,
//    'author'         ,
//    'page-attributes',
//);
//
//$clothes_args = array(
//    'label'               => 'Clothes'          ,
//    'description'         => ''                 ,
//    'public'              => true               ,
//    'show_ui'             => true               ,
//    'show_in_menu'        => true               ,
//    'capability_type'     => 'post'             ,
//    'hierarchical'        => false              ,
//    'rewrite'             => array('slug' => ''),
//    'query_var'           => true               ,
//    'has_archive'         => true               ,
//    'exclude_from_search' => false              ,
//    'supports'            => $clothes_supports  ,
//    'labels'              => $clothes_labels    ,
//);
//
//register_post_type( 'clothes',  $clothes_args );


//Persons
$persons_labels = array (
    'name'               => _x( 'Persons',                   'Persons' ),
    'singular_name'      => _x( 'Persons',                   'Persons' ),
    'menu_name'          => _x( 'Persons',                   'Persons' ),
    'add_new'            => _x( 'Add Persons',               'Persons' ),
    'add_new_item'       => __( 'Add New Persons',           'Persons' ),
    'edit'               => __( 'Edit',                      'Persons' ),
    'edit_item'          => __( 'Edit Persons',              'Persons' ),
    'new_item'           => __( 'New Persons',               'Persons' ),
    'view'               => __( 'View Persons',              'Persons' ),
    'view_item'          => __( 'View Persons',              'Persons' ),
    'search_items'       => __( 'Search Persons',            'Persons' ),
    'not_found'          => __( 'No Persons Found',          'Persons' ),
    'not_found_in_trash' => __( 'No Persons Found in Trash', 'Persons' ),
    'parent'             => __( 'Parent Persons',            'Persons' ),

);

$persons_supports = array(
    'title'          ,
    'editor'         ,
    'excerpt'        ,
    'trackbacks'     ,
    'custom-fields'  ,
    'comments'       ,
    'revisions'      ,
    'thumbnail'      ,
    'author'         ,
    'page-attributes',
);

$persons_args = array(    
    'label'               => 'Persons'          ,
    'description'         => ''                 ,
    'public'              => true               ,
    'show_ui'             => true               ,
    'show_in_menu'        => true               ,
    'capability_type'     => 'post'             ,
    'hierarchical'        => false              ,
    'rewrite'             => array('slug' => ''),
    'query_var'           => true               ,
    'has_archive'         => true               ,
    'exclude_from_search' => false              ,
    'supports'            => $persons_supports  ,
    'labels'              => $persons_labels    ,
    'taxonomies' => array('brands', 'music')    ,
);

register_post_type( 'persons',  $persons_args );


//Contributors
$contributors_labels = array (
    'name'               => _x( 'Contributors',                   'Contributors' ),
    'singular_name'      => _x( 'Contributors',                   'Contributors' ),
    'menu_name'          => _x( 'Contributors',                   'Contributors' ),
    'add_new'            => _x( 'Add Contributors',               'Contributors' ),
    'add_new_item'       => __( 'Add New Contributors',           'Contributors' ),
    'edit'               => __( 'Edit',                           'Contributors' ),
    'edit_item'          => __( 'Edit Contributors',              'Contributors' ),
    'new_item'           => __( 'New Contributors',               'Contributors' ),
    'view'               => __( 'View Contributors',              'Contributors' ),
    'view_item'          => __( 'View Contributors',              'Contributors' ),
    'search_items'       => __( 'Search Contributors',            'Contributors' ),
    'not_found'          => __( 'No Contributors Found',          'Contributors' ),
    'not_found_in_trash' => __( 'No Contributors Found in Trash', 'Contributors' ),
    'parent'             => __( 'Parent Contributors',            'Contributors' ),

);

$contributors_supports = array(
    'title'          ,
    'editor'         ,
    'excerpt'        ,
    'trackbacks'     ,
    'custom-fields'  ,
    'comments'       ,
    'revisions'      ,
    'thumbnail'      ,
    'author'         ,
    'page-attributes',
);

$contributors_args = array(   
    'label'               => 'Contributors'        ,
    'description'         => ''                    ,
    'public'              => true                  ,
    'show_ui'             => true                  ,
    'show_in_menu'        => true                  ,
    'capability_type'     => 'post'                ,
    'hierarchical'        => false                 ,
    'rewrite'             => array('slug' => '')   ,
    'query_var'           => true                  ,
    'exclude_from_search' => false                 ,
    'supports'            => $contributors_supports,
    'labels'              => $contributors_labels  ,
);

register_post_type( 'contributors', $contributors_args );


/***********************************************************************************************
						RELATIONSHIP TAXONOMY AND POST TYPE
************************************************************************************************/

add_action('init', 'relationship_taxonomy_and_post_type');
function relationship_taxonomy_and_post_type() {
    register_taxonomy_for_object_type( 'music', 'brand_articles' );
    register_taxonomy_for_object_type( 'music', 'persons' );
    register_taxonomy_for_object_type( 'brands', 'brand_articles' );
    register_taxonomy_for_object_type( 'brands', 'persons' );
    register_taxonomy_for_object_type( 'post_tag', 'brand_articles' );
    register_taxonomy_for_object_type( 'category', 'brand_articles' );
//    register_taxonomy_for_object_type( 'brands', 'clothes' );
}


/***********************************************************************************************
								POST TYPE THEME SUPPORT
************************************************************************************************/

add_theme_support( 'post-thumbnails', array( 'contributors' ) );

/***********************************************************************************************
                                POST TYPE CUSTOM COLUMS
************************************************************************************************/
//Clothes
//add_filter('manage_clothes_posts_columns', 'set_clothes_head', 10);
//function set_clothes_head( $column_names ) {
//    unset($column_names['music']);
//    return $column_names;
//}


//Contributors
add_filter('manage_contributors_posts_columns', 'set_contributors_head', 10);  
function set_contributors_head() {
    return array(
            'title'               => 'Title',
            'short_name'          => 'Short Name',
            'contributor-type'    => 'Type',
            'blood-type'          => 'Blood Type',
            'contributor-profile' => 'Profile',
            'contact-links'       => 'Contact And Links',
            'featured-image'      => 'Image'
        );
}

add_action('manage_contributors_posts_custom_column', 'set_contributors_cintent', 10, 2);
function set_contributors_cintent( $column_names, $post_id ) {
    if( $column_names == 'short_name' ) {
        if( get_field('contributor_shortname', $post_id) )
            echo get_field('contributor_shortname', $post_id);    
        else
            echo "Please choose a contributor's short name";
    }

    if( $column_names == 'contributor-type' ) {
        if( get_field('contributor_type', $post_id) )
            echo get_field('contributor_type', $post_id);    
        else
            echo "Please choose a contributor's type";
    }

    if( $column_names == 'blood-type' ) {
        if( get_field('blood_type', $post_id) )
            echo get_field('blood_type', $post_id);    
        else
            echo "Please choose a contributor's blood type";
    }

    if( $column_names == 'contributor-profile' ) {
        if( get_field('profile', $post_id) )
            echo get_field('profile', $post_id);    
        else
            echo "Please fill in the profile about the contributor";    
    }

    if( $column_names == 'contact-links' ) {
        $contact_links = array();
        while( has_sub_field('contact_and_links', $post_id) ):
            $contact_links[]  = array( 
                        'title' => get_sub_field('cal_title', $post_id),
                        'value' => get_sub_field('cal_value', $post_id),
                        'main'  => get_sub_field('cal_main',  $post_id)
                    );
        endwhile;
        
        if( !empty( $contact_links ) ) {
            foreach ($contact_links as $single_contact_link) {
                echo $single_contact_link['title'] . ' : ' . '<a href="' . $single_contact_link['value'] . '">' . $single_contact_link['main'] . '</a>' . '<br>';
            }    
        } else {
            echo "Please choose a contributor's contact and links";
        }
    }

    if( $column_names == 'featured-image' ) { 
        if( has_post_thumbnail( $post_id ) )
            echo get_the_post_thumbnail( $post_id, 'thumbnail' );
        else
            echo "Please choose a contributor's image";
    }
}
?>