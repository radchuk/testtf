<?php
/*
 * Ajax action for brands
 */
add_action('wp_ajax_reload_taxonomy', 'reloaded_taxonomy');
function reloaded_taxonomy() {
    global $wpdb;
    $slug =  $_GET['term'] . '%'; 
    //not in the middle of term
    // $slug =  '%' . $_GET['term'] . '%';
    $brands_fields = array( 'brands' ); 

    if( in_array( $_GET['name'], $brands_fields ) )
        $taxonomy = 'brands';
    else    
        $taxonomy = 'music';

    $query = "SELECT wp_term_taxonomy.term_id as id, wp_terms.name as text
    FROM   wp_terms
      JOIN wp_term_taxonomy USING (term_id)
    WHERE  wp_term_taxonomy.taxonomy = '%s'
       AND wp_terms.name LIKE '%s'";

    $sql_results = $wpdb->get_results( $wpdb->prepare($query, array($taxonomy, $slug)), ARRAY_A );

    if (! is_array($sql_results) ) echo json_encode( array() );
    
    // $sql_results = $wpdb->get_results( $wpdb->prepare($query, array($taxonomy, $slug)) );

    // echo $sql_results; die();
    // var_dump( $sql_results );
    // logConsole('text', $sql_results);

    // $brands = array(); 
    // foreach ($sql_results as $result) {
    //  $term_object = get_term_by( 'term_id', $result->term_id, $taxonomy);
    //  $brands[] = array('id' => $term_object->term_id, 'text' => $term_object->name);
    // }
    echo json_encode($sql_results);
    
    die();
}

/*
 * Function for cunstom insert post
 */
function tf_custom_insert_post( $inner_options, $wp_error = false ) {
    global $wpdb, $user_ID;

    $defaults = array(
                    'post_status'           => 'draft', 
                    'post_type'             => 'post', 
                    'post_author'           => $user_ID,
                    'ping_status'           => get_option('default_ping_status'), 
                    'post_parent'           => 0,
                    'menu_order'            => 0, 
                    'to_ping'               => '', 
                    'pinged'                => '', 
                    'post_password'         => '',
                    'guid'                  => '', 
                    'post_content_filtered' => '', 
                    'post_excerpt'          => '', 
                    'import_id'             => 0,
                    'post_content'          => '', 
                    'post_title'            => ''
                    );


    $inner_options = wp_parse_args($inner_options, $defaults);
    extract($inner_options, EXTR_SKIP);

    if ( empty($post_type) )
        $post_type = 'post';

    if ( empty($post_status) )
        $post_status = 'draft';

    if ( empty($post_author) )
        $post_author = $user_ID;

    $post_id = 0;

    if ( empty($post_name) ) {
        if ( !in_array( $post_status, array( 'draft', 'pending', 'auto-draft' ) ) )
            $post_name = sanitize_title($post_title);
        else
            $post_name = '';
    } 

    if ( empty($post_date) || '0000-00-00 00:00:00' == $post_date )
        $post_date = current_time('mysql');
        $mm = substr( $post_date, 5, 2 );
        $jj = substr( $post_date, 8, 2 );
        $aa = substr( $post_date, 0, 4 );
        $valid_date = wp_checkdate( $mm, $jj, $aa, $post_date );
        if ( !$valid_date ) {
            if ( $wp_error )
                return new WP_Error( 'invalid_date', __( 'Whoops, the provided date is invalid.' ) );
            else
                return 0;
        }

    if ( empty($post_date_gmt) || '0000-00-00 00:00:00' == $post_date_gmt ) {
        if ( !in_array( $post_status, array( 'draft', 'pending', 'auto-draft' ) ) )
            $post_date_gmt = get_gmt_from_date($post_date);
        else
            $post_date_gmt = '0000-00-00 00:00:00';
    }

    if ( 'publish' == $post_status ) {
        $now = gmdate('Y-m-d H:i:59');
        if ( mysql2date('U', $post_date_gmt, false) > mysql2date('U', $now, false) )
            $post_status = 'future';
    } elseif( 'future' == $post_status ) {
        $now = gmdate('Y-m-d H:i:59');
        if ( mysql2date('U', $post_date_gmt, false) <= mysql2date('U', $now, false) )
            $post_status = 'publish';
    }

    if ( !isset($post_password) || 'private' == $post_status )
        $post_password = '';

    $post_name = wp_unique_post_slug($post_name, $post_id, $post_status, $post_type, $post_parent);

    $data = compact( array( 'post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_content_filtered', 'post_title', 'post_excerpt', 'post_status', 'post_type', 'comment_status', 'ping_status', 'post_password', 'post_name', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt', 'post_parent', 'menu_order', 'guid' ) );

    if ( false === $wpdb->insert( $wpdb->posts, $data ) ) {
        if ( $wp_error )
            return new WP_Error('db_insert_error', __('Could not insert post into the database'), $wpdb->last_error);
        else
            return 0;
    }

    $post_id = (int) $wpdb->insert_id;

    $where = array( 'ID' => $post_id );
    if ( empty($data['post_name']) && !in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft' ) ) ) {
        $data['post_name'] = sanitize_title($data['post_title'], $post_id);
        $wpdb->update( $wpdb->posts, array( 'post_name' => $data['post_name'] ), $where );
    }

    $current_guid = get_post_field( 'guid', $post_id );
    if ( '' == $current_guid )
        $wpdb->update( $wpdb->posts, array( 'guid' => get_permalink( $post_id ) ), $where );
   // die($post_id);
    return $post_id;
    

}

/*
 * Action for snap group
 * TODO: rewrite this function later
 */

function my_acf_save_post(  $post_id ){
    
      if ( get_post_type( $post_id ) == 'acf'  ) return;
      if ( get_post_type( $post_id ) != 'post' ) return;

    $snap_group = get_field( 'persons', $post_id );
       
    if( empty( $snap_group ) )
        return;

    $default_keys = array('name', 'photos', 'gender', 'job', 'age', 'facebook', 'twitter', 'instagram', 'tumblr', 'blog', 'brands', 'favorite_brands', 'favorite_music');
    $brands_keys = array('brands_outer', 'brands_tops', 'brands_bottoms', 'brands_bag', 'brands_shoes', 'brands_accessories' );
    $acf_ids = array();
    $acf_meta = array();

    foreach ($snap_group as $layout) {

            if( !empty($layout['name'] ) ) {
            $post_title = $layout['name'] . ' - ' . '(' . current_time('mysql') . ')';
            $new_persons_id = tf_custom_insert_post(array( 'post_type' => 'persons', 'post_status' => 'publish', 'post_title' =>  $post_title )); //create new post
             
            $acf_ids[] = $new_persons_id;
        }


        
        $data = array();
        $brands = array();
        foreach ($layout as $key => $value) {

            if( in_array($key, $brands_keys) && (!empty($value) && $value != 'Invalid URL')) {
                $brands[$key] = $value;
            }
            if( !in_array($key, $brands_keys) && (!empty($value) && $value != 'Invalid URL')) {
                $data[$key] = $value;    
            }
        }
        if(count($brands)){
        $data['brands'] = $brands;
        }

        if(count($data['favorite_brands'])){
            $data['brands']['favorite_brands'] = $data['favorite_brands'];    
        }
            foreach ($data['brands'] as $brand ){
                foreach($brand as $brand_term){
                    $person_brands [] = $brand_term;
                }
        }
        if(count($person_brands) ){
                $person_brands = array_map('intval', $person_brands);
                $person_brands = array_unique( $person_brands );
                wp_set_object_terms( $new_persons_id, $person_brands, 'brands');
        }
        
        if(count($data['favorite_music'])){
                foreach ($data['favorite_music'] as $favorite_music_term ){
                    $music_term [] = $favorite_music_term;
                }
        }
        if(count($music_term) ){
                $music_term = array_map('intval', $music_term);
                $music_term = array_unique( $music_term );
                wp_set_object_terms( $new_persons_id, $music_term, 'music');
        }
        $acf_meta[$new_persons_id] = array_merge(array('object' => $new_persons_id), $data); 

    }

    //get persons p2p_meta for disconnect & connect
    $p2p_meta = tf_custom_get_p2p_meta( $post_id, 'post_to_persons', $default_keys );
    //---------------------------------------------

    //persons connect or disconnect

    //diff between person's acf_meta & p2p_meta 
    $disconnect_diff = tf_custom_array_diff_assoc_recursive( $acf_meta, $p2p_meta['layouts'] );

    //disconnect persons
    tf_custom_need_to_disconnect( $disconnect_diff, $post_id, 'post_to_persons', $acf_ids, $p2p_meta['connected_ids'] );
    
    //diff between person's acf_meta & p2p_meta
    $connect_diff = tf_custom_array_diff_assoc_recursive( $acf_meta, $p2p_meta['layouts'] );
    
    //connect persons
    tf_custom_need_to_connect( $connect_diff, $post_id, 'post_to_persons', $acf_meta );





}
add_action( 'acf/save_post', 'my_acf_save_post', 10, 1 );

/*
 * Prepare array with p2p_meta for function tf_custom_acf_generate_layouts();
 */
// function tf_custom_acf_prepare_layouts( $p2p_meta ) { 
//     $output_layouts = array();
//     foreach ( $p2p_meta as $person_data ) {
//         $data = array();
//         foreach ($person_data as $key => $value) {
//             if( $key != 'brands' ) {
//                 $data[$key] = $value;
//             } elseif( $key == 'brands' ) {
//                 foreach ($value as $brand_key => $brand_value) {
//                     $data[$brand_key] = $brand_value;
//                 }
//             }
//         }
//         $output_layouts[] = $data;
//     }
//     return $output_layouts;
// }

/*
 * Create repeater lauouts
 */
// function tf_custom_acf_create_layouts( $new_layouts, $post_id ) {
//     $field_key = "field_51a921e84df5f";
//     $repeater_value = get_field($field_key);
//     foreach ($repeater_value as $old_layout_key => $old_layout_value) {
//         if( $repeater_value )
//             unset($repeater_value[$old_layout_key]);
//     }

//     foreach ($new_layouts as $layout) {
//         $repeater_value[] = $layout;
//     }

//     update_field( $field_key, $repeater_value, $post_id ); 
// }

function tf_custom_get_p2p_meta_preview_mode( $post_id, $connection_type, $default_acf_keys ) {

    $query = new WP_Query( array(
                        'connected_type'   => $connection_type ,
                        'connected_items'  => $post_id,
                        'connected_query' => array( 
                                'post_status' =>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash','pitch') 
                                ),

                        'nopaging'         => true,
                        'post_type'        => 'persons'
                        ,'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash') 

                    ) );

    $connected_ids = array();
    $layouts = array();

    global $post;
    if ( $query->have_posts() ){

         while( $query->have_posts() ){ $query->the_post();
        
                $p2p_id          = $post->p2p_id;
                $connected_id    = $post->ID;
                $connected_ids[] = $connected_id;

                $layout = array();
                foreach ($default_acf_keys as $acf_key)
                    $layout[$acf_key] = p2p_get_meta( $p2p_id, $acf_key, true );

                $layouts[$connected_id] = $layout;

        }

    }
    wp_reset_postdata();
    $data = array(
                'connected_ids'  => $connected_ids,
                'layouts'        => $layouts
        );

    return $data;

}


/*
 * Get metadata from p2p
 */
function tf_custom_get_p2p_meta( $post_id, $connection_type, $default_acf_keys ) {
    $connected_objects = get_posts( array(
                            'connected_type'   => array( $connection_type ),
                            'connected_items'  => $post_id,
                            'nopaging'         => true,
                            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash') 
                            // 'post_status'      => 'any',

                        ) );

    $connected_ids = array();
    $layouts = array();

    foreach ( $connected_objects as $object ) {
        $p2p_id = $object->p2p_id;
        $connected_id = $object->ID;
        $connected_ids[] = $connected_id;

        $layout = array();

        foreach ($default_acf_keys as $acf_key)
            $layout[$acf_key] = p2p_get_meta( $p2p_id, $acf_key, true );

        $layouts[$connected_id] = $layout;
    }

    $data = array(
                'connected_ids'  => $connected_ids,
                'layouts'        => $layouts
        );

    return $data;
}

/*
 * Create connection
 */
function tf_custom_need_to_connect( $diff, $post_id, $connection_type, $acf_meta ) {
    if( !empty( $diff ) ) {
        $connect_ids = array();
        foreach ($diff as $diff_key => $diff_value) {
            $connect_ids[] = $diff_key;
        }

        foreach ($connect_ids as $connect_id) {
            $p2p_id = p2p_type( $connection_type )->get_p2p_id( $post_id, (int) $connect_id );      
            if( !$p2p_id ){
                p2p_type( $connection_type )->connect( $post_id, (int) $connect_id, $acf_meta[$connect_id] );
            }
        }
    }
}

/*
 * Create disconnection
 */
function tf_custom_need_to_disconnect( $diff, $post_id, $connection_type, $acf_ids, $p2p_ids ) {
    if( !empty( $diff ) ) {
        $disconnect_ids = array();
        foreach ($diff as $diff_key => $diff_value) {
            $disconnect_ids[] = $diff_key;
        }
        if( !empty( $disconnect_ids ) ) {
            foreach ( $disconnect_ids as $disconnect_id ) {
                $p2p_id = p2p_type( $connection_type )->get_p2p_id( $post_id, (int) $disconnect_id );
                if( $p2p_id ){
                    wp_delete_post( $disconnect_id ); //delete post
                    p2p_type( $connection_type )->disconnect(  $post_id, (int) $disconnect_id );        
                }
            }    
        }
    }

    $disconnect_ids_further_validation = array_diff( $p2p_ids, $acf_ids );
    if( !empty( $disconnect_ids_further_validation ) ) {
        foreach ( $disconnect_ids_further_validation as $disconnect_id_further_validation ) {
            $p2p_id = p2p_type( $connection_type )->get_p2p_id( $post_id, (int) $disconnect_id_further_validation );
            if( $p2p_id ){
                wp_delete_post( $disconnect_id_further_validation ); //delete post
                p2p_type( $connection_type )->disconnect(  $post_id, (int) $disconnect_id_further_validation );        
            }
        }    
    }
}

/*
 * After removal of the post, the function removes posts with which it has are connected
 */
add_action('p2p_delete_connections', 'tf_custom_delete_related_persons');
function tf_custom_delete_related_persons($p2p_id) {
    foreach ($p2p_id as $id) {
        $connection = p2p_get_connection( $id );
        wp_delete_post( $connection->p2p_to ); //delete post
    }
}

/*
 * Recursive diff two arrays
 */
function tf_custom_array_diff_assoc_recursive( $array1, $array2 ) {
    $difference = array();
    foreach($array1 as $key => $value) {
        if( is_array($value) ) {
            if( !isset($array2[$key]) || !is_array($array2[$key]) ) {
                $difference[$key] = $value;
            } else {
                $new_diff = tf_custom_array_diff_assoc_recursive($value, $array2[$key]);
                if( !empty($new_diff) )
                    $difference[$key] = $new_diff;
            }
        } else if( !array_key_exists($key,$array2) || ($array2[$key] !== $value || ( (int) $array2[$key] !== (int) $value ) ) ) {
            $difference[$key] = $value;
        }
    }
    return $difference;
}
