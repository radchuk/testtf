<?php 
/**
 * Post Type Connection
 * @author Bill Erickson
 * @link http://www.billerickson.net/code/posts-2-posts-create-connection
 *
 * @uses Posts2Posts
 * @link https://github.com/scribu/wp-posts-to-posts/wiki
 */

add_action( 'p2p_init', 'be_post_type_connections' );
function be_post_type_connections() {
    // Make Sure plugin is active
    if ( !function_exists( 'p2p_register_connection_type' ) )
        return;

	//stored connected snap informations        
    p2p_register_connection_type( array(
        'name'               => 'post_to_persons',   // unique name
        'from'               => 'post',             // post type slug
        'to'                 => 'persons',       // post type slug
        'prevent_duplicates' => true,
        'admin_box'          => false
    ) );
}

