<?php
add_filter('init', 'add_custom_rewrite_rules');
function add_custom_rewrite_rules() {

    
    add_rewrite_rule(
        '([^/]+)/([^/]+)/([^/]+)/?$',
        'index.php?find=$matches[1]&field_name=$matches[2]&field_value=$matches[3]',
        'bottom'
    );
    add_rewrite_rule(
        '([^/]+)/([^/]+)/([^/]+)/page/([0-9]+)?$',
        'index.php?find=$matches[1]&field_name=$matches[2]&field_value=$matches[3]&paged=$matches[4]',
        'bottom'
    );


    add_rewrite_rule(
        'find/([^/]+)/([^/]+)/?$',
        'index.php?find=$matches[1]&field_name=$matches[2]&field_value=$matches[3]',
        'bottom'
    );
    add_rewrite_rule(
        'find/([^/]+)/([^/]+)/page/([0-9]+)?$',
        'index.php?find=$matches[1]&field_name=$matches[2]&field_value=$matches[3]&paged=$matches[4]',
        'bottom'
    );
}


add_filter( 'query_vars', 'snap_add_queryvars' );
function snap_add_queryvars( $query_vars ) {
    $query_vars[] = 'find';
    $query_vars[] = 'field_name';
    $query_vars[] = 'field_value';
    $query_vars[] = 'item_field_name';
    $query_vars[] = 'item_field_value';
    $query_vars[] = 'brand_field_name';
    $query_vars[] = 'brand_field_value';

    return $query_vars;
}


add_filter('template_include', 'tokyo_rewrite_template', 1, 1);
function tokyo_rewrite_template($template) {
    global $wp_query;

    if (isset($wp_query->query_vars['find']))
    {
        return dirname(__FILE__) . '/../templates/persons-view-template.php';
    }
    if (isset($wp_query->query_vars['brands']))
    {
        return dirname(__FILE__) . '/../templates/taxonomy_custom_brands.php';
    }

    return $template;
}

/**
 * Change Posts Per filtered snaps
 * 
 * @author Arthur Tkachenko
 * @link netweightit.com
 * @param object $query data
 *
 */