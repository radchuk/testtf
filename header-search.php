<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="generator" content="WordPress <?php bloginfo('version') ?>" /><!-- Please leave for stats -->
<title><?php bloginfo('name'); ?>- <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?version=2" type="text/css" media="screen" />

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Comments RSS Feed" href="<?php bloginfo('comments_rss2_url') ?>"  />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.accordion.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-init.js"></script>

<style type="text/css">
#friend-logo { background:url(<?php echo get_bloginfo('template_url');?>/images/gallery-nucleus-logo.gif) no-repeat scroll 50% 50%; float:right; margin:10px 200px 10px 0; width:140px; height:140px; }
</style>
<?php wp_head() ?>

</head>

<body>
<div id="header">

	<div class="wrapper">

		<h1 id="logo"><a href="<?php echo get_option('home'); ?>" title="<?php bloginfo('name'); ?>"><span><?php bloginfo('name'); ?></span></a></h1>
		<h2 id="friend-logo"></h2>
		
	</div>
		
</div><!--  #header -->

<div class="wrapper">
	
	<div id="container">
	
