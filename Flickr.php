<?php

class Flickr
{
	private $feed_url;
	private $photos = array();
	
	public function __construct($feed_url)
	{
		$this->feed_url = $feed_url;
	}
	
	public function get_photos($quantity=1000)
	{
		
		try {
			$data = @new SimpleXMLElement($this->feed_url, 0, TRUE);						
			
			
			$idx = 0;
			foreach($data->channel->item as $photo) 
			{		
				$idx++;
				if ($idx>$quantity) break;
				
				$photo_item = array();

				$photo_item['title']   = (string)$photo->title;
				$photo_item['link']    = (string)$photo->link;
				$photo_item['pubDate'] = (string)$photo->pubDate;

				// this is a way to get data with colons (:)
				$media = $photo->children("http://search.yahoo.com/mrss/");
				$photo_item['description'] = (string)$media->description;			

				$content_attr = $media->content->attributes();			
				$photo_item['photo_url'] = (string)$content_attr->url;

				$this->photos[] = $photo_item;
				
			}
		
		} catch (Exception $e) {error_log($e->getMessage()); }
		
		return $this->photos;
	}
	
}
?>
