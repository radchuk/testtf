jQuery(function($) {

    var socialDropDown = {

        selectorTitle: '.snippet-title.js-share',
        selectorTitleHref: '.snippet-title a',
        selectorSocialTitle: '#nav',

        init: function(){
            this.showPopup();  
            this.drop();
        },
        showPopup: function(){
            var self = this
            // $(self.selectorSocialTitle).hide();
            $(self.selectorTitle).mouseenter(function(){
                // $(this).addClass('social');
                var pisitionSocialTitle = $(this).children('a').position();
                var blockPosition = $('.snippet').position();
                var blockWidth = $('.snippet').width();
                $(self.selectorSocialTitle).css({
                    position:   'absolute',
                    top:        pisitionSocialTitle.top,
                    left:       blockPosition.left + blockWidth - 50
                });
                $('.dropdown').show();
                $('#custom-tweet-button a').prop('href','https://twitter.com/share?' + $(this).children('a').data('twitter') + '&via=TokioFashion');
                $('#custom-fb-button a').prop('href','https://www.facebook.com/sharer/sharer.php?' + $(this).children('a').data('fb'));
                // console.log(blockPosition);
                
                $(self.selectorSocialTitle).show();
            });
            // $(self.selectorTitle).mouseleave(function(){
            //     $(this).removeClass('social');
            //     // $(self.selectorSocialTitle).hide();
            // })
        },
        drop: function(){
           $('.dropdown-toggle').dropdown();
        }
    }

socialDropDown.init();   
});