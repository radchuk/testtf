$.smartbanner({
    title: null, // What the title of the app should be in the banner (defaults to <title>)
    author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
    price: 'FREE', // Price of the app
    appStoreLanguage: 'us', // Language code for App Store
    inAppStore: 'On the App Store', // Text of price for iOS
    inGooglePlay: 'In Google Play', // Text of price for Android
    icon: null, // The URL of the icon (defaults to <link>)
    iconGloss: null, // Force gloss effect for iOS even for precomposed (true or false)
    button: 'VIEW', // Text on the install button
    scale: 'auto', // Scale based on viewport size (set to 1 to disable)
    speedIn: 300, // Show animation speed of the banner
    speedOut: 400, // Close animation speed of the banner
    daysHidden: 15, // Duration to hide the banner after being closed (0 = always show banner)
    daysReminder: 90, // Duration to hide the banner after "VIEW" is clicked (0 = always show banner)
    force: null // Choose 'ios' or 'android'. Don't do a browser check, just always show this banner
})