<div class="contentpanel">
	<a href="http://tokyofashion.com/articles/" class="morelink">Browse All Articles &raquo;</a>
	<h2 class="contentpaneltitle">Tokyo Fashion By Category</h2>
	<div id="mycarousel-nav">
		<a href="#" id="mycarousel-prev">&laquo; Previous</a>
		<a href="#" id="mycarousel-next">Next &raquo;</a>
	</div>
	<ul id="mycarousel" class="jcarousel-skin">
	<?php
		$homepageCatIDs = explode (',', $homepageCatIDs);
		foreach ($homepageCatIDs as $catID) {
			$catPosts = new WP_Query();
			$catPosts->query('showposts=1&cat=' . $catID);					
	?>
		<?php if ( $catPosts->have_posts() ) : $catPosts->the_post() ?>
		<li>
			<h3><?php echo get_cat_name($catID); ?></h3>


            <?php    /*******************************************************************************************************************/ ?>
            <?php   get_template_part( '/partials/tokyo', 'image_small2' ); ?>
            <?php   /*******************************************************************************************************************/ ?>



            <h4 class="snippet-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			<p class="snippet-meta">Posted on <?php the_time('F j, Y'); ?></p>
			<p class="snippet-intro"><?php echo truncate($post->post_excerpt, 100); ?></p>
		</li>
		<?php endif; ?>
	<?php } ?>
	</ul>
</div>